# TODO

- UPDATE these todos
- FIX reference mapBack
- FIX populate tracker after create
- Make author reference work.
- Remove t() from errors.

Later
- Add option to hijack node edit form
- Add option for a propose-change link

Much-Later
- Check violations in Webform4ContentHandler::validateForm
- Add sth like UserConfirmItemMapper (instead of user magic)
- Add needsModeration=pre/post/none field & moderation logic: 
  - handler.field_no_access=premoderation/postmoderation/grantaccess
  - handler.user_needs_confirmation=wait-for-confirmation/publish-anonymous
  - userConfirmLogic
  - preModerateAction (if moderating user has access, map, save, and update needsModeration)
  - postModerateAction (set isPostModerated=true)
- Consider instead: Use workflow, transitions with fields
  - start as draft
  - last is published (not listed in view)
  - transition can have field trigger-on=full-access/users-confirmed/is-postmoderated
  - states can have field?...

Upstream
- Move prepopulate to prepareForm
