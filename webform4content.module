<?php

declare(strict_types=1);

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\state_machine\Form\StateTransitionConfirmForm;
use Drupal\webform4content\InlineEntityForm\Helpers\SootheWebformUnsavedWarning;
use Drupal\webform4content\InlineEntityForm\WebformSubmissionInlineForm;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Plugin\WebformHandler\W4cHandler;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

/**
 * Implements hook_entity_field_storage_info().
 *
 * @noinspection PhpUnused*/
function webform4content_entity_field_storage_info(EntityTypeInterface $entity_type): ?array {
  return WebformSubmissionWrapper::baseFieldDefinitionsByEntityTypeId()[$entity_type->id()] ?? NULL;
}

/**
 * Implements hook_entity_bundle_field_info().
 *
 * DISABLED, using base fields for now, to get views integration.
 * @see https://www.drupal.org/project/drupal/issues/2898635
 *
 * Note that the lack of bundle field override config may cause issues.
 * Modules like content_translation or field_permission wont work.
 * @see https://www.drupal.org/node/2346347
 */
function webform4content_entity_bundle_field_info_DISABLED(EntityTypeInterface $entity_type, string $bundle, array $base_field_definitions) {
  if (
    $entity_type->id() === 'webform_submission'
    && W4cHandler::all($bundle)
  ) {
    return WebformSubmissionWrapper::baseFieldDefinitionsByEntityTypeId()[$entity_type->id()] ?? NULL;
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function webform4content_entity_base_field_info(EntityTypeInterface $entity_type) {
  return WebformSubmissionWrapper::baseFieldDefinitionsByEntityTypeId()[$entity_type->id()] ?? NULL;
}

/**
 * Implements hook_install().
 *
 * Because the field storage definition knows its provider (us), we need not and
 * in fact must not delete the fields on hook_uninstall.
 * @see \Drupal\Core\Extension\ModuleInstaller::uninstall
 */
function webform4content_install($is_syncing): void {
  if (!$is_syncing) {
    foreach (WebformSubmissionWrapper::baseFieldDefinitionsByEntityTypeId() as $entityTypeId => $fieldDefinitions) {
      foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
        \Drupal::entityDefinitionUpdateManager()
          ->installFieldStorageDefinition($fieldName, $entityTypeId, 'webform4content', $fieldDefinition);
      }
    }
  }
}

/**
 * Implements hook_entity_type_alter().
 *
 * Enable confirmation screen for webform submission state changes.
 */
function webform4content_entity_type_alter(array &$entity_types) {
  if ($entityType = $entity_types['webform_submission'] ?? NULL) {
    assert($entityType instanceof EntityTypeInterface);
    $entityType->setLinkTemplate('state-transition-form', '/webform_submission_transition_confirm/{webform_submission}/{field_name}/{transition_id}');
    // Also set form class.
    // @todo Fix upstream at state_machine to come after all other modules.
    // @see \state_machine_entity_type_alter
    $entityType->setFormClass('state-transition-confirm', StateTransitionConfirmForm::class);
  }
}

/**
* Implements hook_entity_view_display_alter().
*/
function webform4content_entity_view_display_alter(EntityViewDisplayInterface $display, array $context): void {
  WebformSubmissionWrapper::staticHookEntityViewDisplayAlter($display, $context);
}

/**
 * Implements hook_entity_type_build().
 */
function webform4content_entity_type_build(array &$entity_types) {
  $entity_types['webform_submission']
    ?->setHandlerClass('inline_form', WebformSubmissionInlineForm::class);
}

/**
 * Implements hook_ajax_render_alter().
 */
function webform4content_ajax_render_alter(array &$commands) {
  SootheWebformUnsavedWarning::hookAjaxRenderAlter($commands);
}
