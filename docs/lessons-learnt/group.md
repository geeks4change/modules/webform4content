# Lessons learnt: Group

## Group Data Model
- GroupType: ConfigEntity, bundle of Group
- Group: ContentEntity
- GroupContentType: ConfigEntity, bundle of GroupContent
- GroupEnabler plugin
- GroupContent: ContentEntity

### GroupType
- ConfigEntity, bundle of Group (ContentEntity)
- Has settings for
  - create_membership: If group creator is auto-added to group as member.
  - creator_roles: What roles to get.
  - creator_wizard: If to use form wizard to edit fields to group creator membership.
- Also on GroupType create there a setting: "Automatically configure an administrative role -- This will create an 'Admin' role by default which will have all currently defined permissions."
  - Note that it does NOT grant all future roles like core admin role.

### GroupContent
- Serves mainly as anchor to relate GroupContent and GroupMembership to.
- GroupContent has a label field.
- GroupContent has a uid field, which triggers auto-creation of a user role on creation, if configured so in the group type (Group::postSave).

### GroupContentType
- ConfigEntity, bundle of GroupContent (ContentEntity)
- Name is {groupTypeId}-{ContentEnablerPluginId}
  - where ContentEnablerPluginId will be shortened if needed.
  - example: *my_group_type-group_membership* - pluginId is *group_membership*.
  - example: *my_group_type-group_node-my_node_type* - pluginId part is *group_node-my_node_type*, a *Derivative* of the group_node plugin template, for the node bundle *my_node_type*.

### GroupContent
- References Group (gid) and Entity (entity_id).
- GroupContent has a label field, which is not required and ignored in favor of related entity label.
- GroupContent has a uid field, with no known functionality.

### ContentEnablerPluginId
- Beware: While GroupContentType ID is separeted by '-', pluginId is separeted by ':'.
- ContentEnablerPluginTypes, provided by modules, are e.g. *GroupNode* or *GroupCommerceOrder*. I.e. every EntityType needs its own.
- The PluginTemplates are often "derived" to concrete ContentEnablerPlugin by bundle
- ContentEnablerPluginType may also bring in different logic.
- An example of such different logic ist GroupMembership, which is different from a (afaik hypothetical) GroupUser plugin that makes users group content.

### GroupRoles
- per GroupType
- Complex Insider / Outsider logic (will change in 2.x)
- Name: {GroupTypeId}-{RoleId}
  - where RoleId will be shortened if needed.

### ID shortening for GroupContentType and GroupRoles
- GroupContentType and GroupRoles have naming schemes that, if they exceed some character limit, are shortened by replacing the long part with a hash.
  - \Drupal\group\Plugin\GroupContentEnablerBase::getContentTypeConfigId
  - \Drupal\group\GroupRoleSynchronizer::getGroupRoleId

## Group 2.x+
- Naming like *GroupEnabler* and *GroupContent* will change in Group v2/v3.
- Group roles will change significantly.
