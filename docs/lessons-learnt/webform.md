# Lessons learnt: Webform

## Many ways to hide elements:
- (no-form-access) Remove access for create / update access (Element > Access)
- (no-access) Display element (Element > Access)
  - Help says: *If unchecked, the element is never displayed. The element will only be visible within the form builder and hidden from all other displays including submission details, results, and download.*
- (private) Private fields (Element > Advanced > Administration):
  - Help says: *Private elements are shown only to users with results access.*
- (hidden) Element type *Hidden*
  - Note: Can be (intendedly or maliciously) changed via JS.

## Hidden fields and conditions
How do (which kind of) hidden fields play with conditions?
- There **is** a server-side condition evaluator (WebformSubmissionConditionsValidator).
- I guess it is built have no FLUC (flash of unstyled content) though.
- Note: There is a bug in it for checkboxes with value '0'. Sigh.

## Conditions also for handlers
- Handlers can be enabled depending on element conditions.

## Is our user-create-or-update form safe?
#### What is wanted:
- Anonymous user enters user name and email (and possibly other fields) for a new user to create.
- Authenticated user has nothing to enter and is assigned.
- Permitted admin user can select any user.

#### How it works:
- A select_user_switch element gets populated with a nonempty user ID OR empty, depending on current user.
- A select_user_switch selects, which part of the form is displayed.
- The select_user_switch is read-only (see below).
- The select_user_switch determines what is processed on the server.

In the end, we added a create-or-select widget (which is impossible in webform, as it can not pass entities, )

#### Details:
- The select_user_switch element must be available on the client (hidden).
- It is prepopulated by the server.
- It is **important**, that a element of type *Hidden* is used, not no-access trickery (breaks conditions), no visibility conditions (they are only client-side, and element is submitted then).

#### Why this is safe against JS trickery:
- Even when clientside hidden field is changed, the server takes the unchanged value and does the right thing. So the worst is validation error serverside.
- User autocomplete allows user name enumeration, but our form does not bring this in.
