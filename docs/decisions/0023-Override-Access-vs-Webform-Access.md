# ADR 0016: Mapping Access
Status: Decided, NOT implemented
Related: ADR-0016, ADR-0022

## Context and Problem Statement
How is override access determined?

## Decision Outcome
- Webform access is always respected (to not have json:api bomb us).
- Further restrictions (think: No updates, or admin-only) are possible via provider conditions.
- We need option to restrict node access for the same reason.
- Having conditions which form is displayed for overrides still makes sense.
- Provider logic must block webform form when not wanted.

## Reasoning
- When json:api is enabled, anyone can create a webform via that.
- In other words: Never rely on form access.
- Same for nodes.

## How to implement
- Unify webform and form access
  - [ ] Move tracker preparation magic to hookWebformLoad.
  - [ ] Restrict WebformSubmission create access like defined.
  - [ ] Then simply respect WebformSubmission create access in override
  - [ ] (Later) Allow to restrict node create / update access.
  - [ ] (Later) Add original node form to selection page
  - [ ] Document
