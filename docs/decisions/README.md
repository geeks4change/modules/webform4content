# Webform4Content
## Algorithm
- [Algorithm](algorithm.md)

## Glossary
- A webform submission is mapped to one or more *target entities*, the ID of which is stored in the webform submission via a *tracker element*.
- The bi-directional mapping is defined via *entity mappers*, which have options and consist of *item mappers*.
- Iterm mappers map one or more *elements* to a single *target entity field*.

## Decisions
- [ADR-0001](0001-mapper--bidirectional-mapping.md) Bidirectional mapping
- [ADR-0002](0002-mapper--item-mapping-keyed-by-field.md) Item mapping is keyed by field
- [ADR-0003](0003-mapper--use-code-to-configure.md) Use code to configure
- [ADR-0004](0004-mapper--add-email-confirmer.md) Add email confirmer
- [ADR-0005](0005-workflow--email-confirmation-in-workflow.md) Email confirmation is part of workflow
- [ADR-0006](0006-workflow--possible-workflows.md) Decide possible workflows
- [ADR-0010](0010-emails--configure-emails.md) Configure "webform submission accepted" and "email confirmation" email via separate webform handler
- [ADR-0012](0012-ui--hack-state-change-formatter-above-webform-submission-view.md) Workflow state change formatter hacked above webform submission view
- [ADR-0007](0007-workflow--keep-draft-state.md) Keep draft state in workflow
- [ADR-0016](0016-Mapping-Access.md) Add explicit mapping access and auto-accept, and include entity and field access in that
- [ADR-0017](0017-Override-and-Accept-Conditions.md) Add explicit conditions for override and auto-accept
- [ADR-0018](0018-Validation-Overhaul.md) Overhaul validation
- [ADR-0019](0019-Make-References-work-again.md) Make references work again
- [ADR-0020](0020-Make-element-access-work.md) Make element access work
- [ADR-0021](0021-Rename-to-Fetch-and-Store.md) Rename "Mapping" and "BackMapping" to "Store" and "Fetch"
- [ADR-0022](0022-Entity-and-Field-Access-again.md) Settle entity and field access
- [ADR-0023](0023-Override-Access-vs-Webform-Access.md) Trust entity access, not form access
- [ADR-0024](0024-group-mapping.md) Add group mapping
- [ADR-0025](0025-prefer-hooks.md) Prefer hooks for group mapping
- [ADR-0026](0026-hook-must-store.md) Add hookMustStore

## Next-steps backlog
- [ADR-0008](0008-workflow--use-universal-workflow.md) Fold all pre/post/no-moderation workflows into one?
- [ADR-0015](0015-mapper--update-target-entity.md) Option to hijack target entity edit

## Someday-maybe backlog and tech debt
- [ADR-0013](0013-ui--display-current-state-in-state-change-formatter.md) Display current state in state change formatter
- [ADR-0009](0009-workflow--filter-ui-transitions-by-guard.md) Filter UI transitions by guard, not in field formatter
- [ADR-0011](0011-ui--confirm-state-changes.md) Require confirmation for state changes
- Fix indexing and other email confirmer issues from [ADR-0004](0004-mapper--add-email-confirmer.md)
- Consider using symfony expression list to configure, see [ADR-0003](0003-mapper--use-code-to-configure.md)
- [ADR-0014](0014-datamodel--reference-contributing-submissions.md) Reference all contributing webform submissions in ERR field of target entity.
