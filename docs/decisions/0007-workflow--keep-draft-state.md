# Keep draft state and setter code for now
Status: implemented

## Context and Problem Statement

The draft state is an odd citizen in the workflow. Drop it and use NULL?

## Considered Options

- 1: Keep draft state, keep setting it in code
- 2: Keep draft state, add it as default
- 3: Drop draft state, use NULL

## Decision Outcome

Option 1: Keep draft state and setting code.

### Reasoning

- Don't go 3, because NULL is even odder and has no transition control.
- If we go 2 and use BaseFieldDefinition::setDefaultValue, this may simplify our code,
  but also may populate all fields of all webformSubmissions of non-relevant bundles.
- So stay at 1 and bear with it.
