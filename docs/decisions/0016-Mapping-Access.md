# ADR 0016: Mapping Access
Status: Decided
Related: ADR-0022, ADR_0023

## Context and Problem Statement
- Access to Mapping is not trivial:
  - Who should be allowed to map?
  - Under what conditions is auto-mapping allowed?
- Current case: Even if Anon is permitted to create a node, they are not allowed to set its UID / author.

## Decision Outcome
- A new mapping access grants all entity and field access needed to map.
- The maping access is configurable by mapping plugin author and may be delegated to plugin configurator.

## Reasoning
## Former plan
The former idea was to simply respect entity and field access.
Which means:
- This is conceptually simple and leads to no surprises: The mapper can only do what entity forms can do anyway.
- To check if mapping may take place (and thus 'accept' botton is available), access must be checked without doing the real thing.
- I.e., Bofore doing the real mapping including cascaded save, the mapper does a not-for-real mapping, without the save,
- and records the missing entity and field permissions.
- It turns out that checking access in this not-for-real mapping is conceptually flawed:
  - References are not valid when the referenced entities have not been saved. (Which might be mitigated.)
  - How to set the uid / author of a created node?
    - Setting this need either "administer content" permissions (which we don't want to give),
    - Or it is auto-set if we temporarily switch to that user
      - The latter can be done, but only if the user is saved and then re-deleted
      - And really saving might lead to all sorts of other actions triggered.
    - Of if we add exceptions (e.g. by field) to the check-access rule.
      - Which means that access might still fail in the for-real step,
      - without possibility to know before, and again the need to clean up.
- In effect this means, there is no such thing as a simple not-for-real check.

# Alternative: Mapping access
- In which case there is some access control for the mapping itself,
  which then does mapping and saving without further access checks.
- The access may be configurable per mapping plugin:
  - A permission, which means a webform configurator may NOT set it.
  - A permitted-roles setting, which means a webform configurator MAY set it.
  - Maybe even a group permission or sth else.
- The current not-for-real check can stay in the code, and may be cleaned up some day.
- As of auto-accept
  - This allows webform submitter (usually anonymous) to have the mapping done after
  - email confirmation without extra admin moderation.
  - This can also be hard-coded by the plugin or delegated to the webform configurator.
