# Use bidirectional mapping
Status: implemented

## Context and Problem Statement

Use bidirectional mapping to overcome limitations that other mappers have.

## Decision Drivers

- Want to edit already mapped data.
- Want to leverage target entity validation.

## Decision Outcome

- See [Algorithm](../algorithm.md)
