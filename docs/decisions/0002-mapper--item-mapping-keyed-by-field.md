# Item mapping is keyed by field
Status: implemented

## Context and Problem Statement

What is the data structure for item mappers?

## Decision Drivers

- On one hand, be as generic as possible, allow mapping N elements onto N fields
- But unlimited flexibility bloats up the code.


## Decision Outcome

- Item mappers are keyed by name of the target entity field.
- Item mappers can be attached to arbitrarily many webform submission elements (think multiple-to-yaml).

### Consequences

- Having multiple item mappers attached to one field or one element are not validated currently (don't do it).
- Backmapping dependent on more than one field is not possible (and will never be without major architecture change).
