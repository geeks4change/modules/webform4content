# ADR-0017 Override & Accept Conditions
Status: Decided

## Context and Problem Statement
We need to implement some conditions:
- 1) The overrride condition (if the override applies)
- 2) The override access condition (if the applicable override has access)
- 3) The auto-accept condition

Do we need them all in arbitrary granularity? Or can we simplify?

## Decision Outcome
Implement 1) override and 3) auto-accept conditions, sprere 2).

## Reasoning
- The 3) Auto-accept may be arbitrary. Examples:
  - Auto-accept only for some admin status by permission
  - Auto-accept if post does not contain nasty words (custom)
- The 1) override condition may be quite arbitrary
  - It may be broader than the node create access: so everyone can propose a post
  - It may be disjunct: Everyone who may not create a node directly may propose
  - Or e.g.: Only people with the noob status get a wizard instead of the form
  - Or e.g. everyone may create exactly one node via the wizard
- The 2) Override access condition does not seem to be necessary: Everyone who has the override may also access it
  - A broader access than override condition clearly has no result and makes no sense
  - A narrower condition does not make sense either. To see this, let's look at the situation when the override is selected, but has no access. Then
    - Either node access is not given; in which situation the override should not be selected in the first place to get the same result, no access
    - Or node access is given. In which case node access should be restricted accordingly, and we're in the first case.
  - So all in all, this condition is not necessary. And if use cases arise, we can still implement it.
