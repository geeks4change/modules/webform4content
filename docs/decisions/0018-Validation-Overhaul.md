# ADR-0018: Validation Overhaul
Status: Done

## Context and Problem Statement
While working on a recursion WSOD (endless flip of accepted state due to entity validation violations), we found some dragons in the validate-and-save logic.

## Decision Outcome

## Reasoning
Dragons found:
- Multiple instances of dubious and dead code.

Ideas:
- Factor together accept-logic
- Factor out validation mapping
- Separate violation-based validation and access check
- Integrate backmapping (entity=>webform), read/write access check, and form access
- Unify access and violations into EntityBag
- Reconsider Switch (name and pluginification)
- Disable, then drop irrelevant functionality:
  - Individual-save of entities
  - possibly webformSubmission fields and logic
- Also fix AutoAccept to optionally decline on not accept
- Use field label for violations
- Consider dropping tracker element prepopulate via query parameters
- Give switch element a better name
- Add api marker to methods.

How form workflow should work:
- Prepare entities
- Backmap to webform (including access)
- Display webform, have it submitted
- Validate webform: Map to EntityBag, backmap violations
- Submit webform: Handle workflow
- Handle emailsConfirmed workflow
- Handle accept / decline workflow

Todo:
- [/] Integrate backmapping (entity=>webform), read/write access check, and form access
- [/] Unify access and violations into EntityBag
- [/] Consider dropping tracker element prepopulate via query parameters
- [X] Drop Individual-save of entities
- [X] Factor out validation mapping
- [X] Separate violation-based validation and access check
- [X] Use field label for violations
- [X] Add api marker to methods.
- [X] Reconsider Switch (name and pluginification)
- [X] Simplify emailsToConfirm logic
- [X] Factor together accept-logic
- [/] Also fix AutoAccept to optionally decline on not accept
- [X] Drop outdated webformSubmission fields and logic

Possibly breaking changes:
- Access control for elements
