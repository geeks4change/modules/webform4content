# What workflows do we even want to support?
Status: accepted (no implementation)

## Context and Problem Statement

We started with a single workflow. With the implementation of update-mappings, we have to re-decide.

## Considered Options

- 1 "pre-moderation with email confirmation": email-confirm, accept, map
- 2 "post-moderation with email confirmation": email-confirm, map, review
- 3 "no-moderation with email confirmation": email-confirm, map

## Decision Outcome

Support all of them, and no others.

### Reasoning

- All anonymous submissions must be email-confirmed.
- Authenticated submissions can be handled with the same workflow, they are auto-confirmed (if user uses that email address!).
- Even if any new submission needs to be pre-moderated, updating it usually does not need moderation (optionally post-moderation).
- There are definitely use cases for post-moderation.
- Note: While pre-moderation needs an accept / reject choice, post-moderation only needs a reviewed choice.
