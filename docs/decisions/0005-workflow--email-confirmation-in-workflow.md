# Email confirmation is part of workflow
Status: implemented

## Context and Problem Statement

How do we combine email confirmation and editorial workflow?

## Considered Options

- 1) Emails-confirmation and workflow are two separate properties of the submission
- 2) Email confirmation is represented by a transition of a common workflow.

## Decision Outcome

Option 2: Email confirmation is part of the workflow.
Also, it is always the first, non-optional step if emails are involved.
Decided simply because it was the simplest to start with, and we only had one workflow in sight.
This may change later.

## Pros and Cons of the Options

### Email confirmation and workflow are separate

- Possibly good, because email confirmation and moderation can happen in parallel (but, does this even make sense?).
- Possibly good, because moderation is free to work only on confirmed items or pre-accept submissions.

### Email confirmation is part of workflow

- Good and bad, because the email confirmation has exactly one place in the workflow to happen.
