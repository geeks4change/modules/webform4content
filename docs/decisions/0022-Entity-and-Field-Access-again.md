# ADR 0016: Mapping Access
Status: Decided
Related: ADR-0016, ADR_0023

## Context and Problem Statement
Access is still not settled. Mapping field access to form access is flawed.

## Decision Outcome
- Access is governed all-or-nothing. Who can access the webform, can create or update the entities.
- If sitebuilder wants to restrict access, they can restrict element access in the webform builder.

## Reasoning
- Respecting user write access does not make sense in the mapping paradigm.
- Which grants one to submit some data, another (or the same, with auto-accept) to have that data mapped by moderating, with possibly both not having entity or field access.
- Adding an own access system is not necessary, because webform element access.

### What about read access?
- Problems with read access were possible when we had query-based prepopulation of trackers, and tracker-based loading of entities.
- Both is gone now, for good.
- So fields can only be accessed when an entity is loaded by a provider. Which is either:
  - Entity update forms. Fine, which is exactly what we want.
    - Should people be able to update fields they can not view? This makes no sense, then they can view by hitting update button.
    - Should people be able to update a field they have view access, but do not get the field content in the form? This makes no sense, they could always copy-paste.
    - Should people be able tu update a field they do NOT have view access? Yes this may be an uncommon but possible use case.
    - => Field view access must be respected.
  - An entity from a reference of an existing entity
    - All the above applies.
  - An entity from a reference of a prepopulated referenced entity (like currentUser owner)
    - All the above applies.
