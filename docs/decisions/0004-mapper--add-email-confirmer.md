# Add email confirmation via email_confirmer.module
Status: implemented

## Context and Problem Statement

How to handle anonymous submissions?


## Considered Options

- 1) Use manual editorial check.
- 2) Create users for all submissions and rely on that confirmation logic
- 3) Have all submissions' emails confirmed

## Decision Outcome

- Option 3) Have all emails confirmed, via email_confirmer.module
- Use a EmailConfirmationItemMapper, so it's configurable which email(s) must be confirmed
- These are possibly multiple emails, and confirmation happens when all of them are confirmed.
  - Not sure, if there's even a use case for that, but the logic was not harder.

### Consequences

- The email_confirmer module is stale and has some issues. Consider a internal fork.
  - (This is urgent once we have some 100 confirmations;) [Add Index on email column \[#3095588\] | Drupal.org](https://www.drupal.org/project/email_confirmer/issues/3095588)
  - (We may hit that with translations:) [The call to mail() incorrectly passes a language object instead of a string \[#3261352\] | Drupal.org](https://www.drupal.org/project/email_confirmer/issues/3261352)
  - (The getStatus method is broken:) [Confirmed emails are set to expired \[#3079429\] | Drupal.org](https://www.drupal.org/project/email_confirmer/issues/3079429)
