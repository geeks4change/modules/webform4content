# Filter UI transitions by guard, not in field formatter
Status: accepted

## Context and Problem Statement

- Any StateMachine state field is transition-validated on the TypedDate level (see StateItem::getConstraints / StateConstraintValidator::validate / StateItem::isValid )
- Otoh, only some transitions are allowed to be used by the user via UI
- Current solution: Configure allowed transitions in the StateMachineTools formatter
- This feels odd.

## Considered Options

- 1: Leave it as is.
- 2: Use a guard to filter allowed states, and a global permission token that allows non-ui transitions.

## Decision Outcome

Option 2: Filter UI transitions by guard, not in field formatter (once we have time or are pressed to).

### Notes

- For the token, RAII via WeakRef might be nice.
- Where to check for allowed transitions
  - 1: => Define a guard and muscle it there
    - For defining a guard, see GuardInterface, GuardsPass
  - 2: In workflow group, override workflow_class (no guard)
- How to define UI-allowed transitions?
  - 1: Muscle it in the code
  - 2: => Parse additional transition properties in overridden workflow class constructor
