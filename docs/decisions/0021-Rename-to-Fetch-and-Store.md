# ADR-0021: Rename to fetch and store
## Context and Problem Statement
Even after long time, map and backMap is not intuitive to my brain.

## Decision outcome
Use Fetch (ex backMap) and Store (ex map).

## Reasoning
Naming is difficult. This is better.
