# Require confirmation for state changes
Status: accepted

## Context and Problem Statement

As accepting a submission is irreversible, a confirmation form makes a lot of sense.

## Considered Options

- 1: Leave it as is.
- 2: Make all state changes reversible instead.
- 3: Implement confirmation form.

## Decision Outcome

Option 3: Implement confirmation form

### Notes

## Pros and Cons of the Options

### Option 2: Make all state changes reversible

- Possible in principle, but undo for accept means delete the target entity. But this must be blocked anyway once that has changed.

### Option 3: Implement confirmation form

- The formatter has already settings for this, but that needs additional love.
- It relies on a dedicated form mode of the underlying entity.
