# Display current state in state change formatter
Status: accepted

## Context and Problem Statement

Having the current state beneath the state change buttons would be neat.

## Decision Outcome

We did this in c4c, steal it.
