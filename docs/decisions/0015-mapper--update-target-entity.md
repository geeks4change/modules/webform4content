# Option to hijack target entity edit
Status: accepted

## Context and Problem Statement

- Creating a target entity (plus user, group, and relations) works fine, updating it is prepared.
- Now we want **permitted** users (and only them) to seamlessly update the target entity via webform.
- What are the configurable options and how is it configured?

## Decision Outcome

- Add mapper option to hijack target entity edit.
  - It allows user permitted to edit the entity to edit content via webform (and forbids all others.

## Reasoning

- A typical use case:
  - Anon user suggests content, and confirms email.
  - Moderator approves, content, user, group are created then.
  - User logs in and editing that content redirects to the webform.
  - This webform submission updates content without moderation step.
- Useful e.g. for the json field, where we administer data model, form and display via webform.
- Variants:
  - Auth user suggests content => No confirmation and user creation necessary.
  - Updated content has other workflow (pre /post- / not-moderated).
    - Maybe users without edit permission may suggest changes, and maybe moderation is done by a different group than newly created content.
  - Created content has other workflow (pre /post- / not-moderated).
    - Maybe logged in users or users with some permission / group permission have a less restrictive workflow.

Agile approach
- Tackle the main use case now: Hijack target entity edit for permitted user, with configurable workflow.
- Configure as option of entity mapper, sth like `->handle(Edit::create()->setNoModeration()`.
