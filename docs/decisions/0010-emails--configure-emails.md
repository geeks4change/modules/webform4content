# Configure emails via separate webform handler
Status: implemented

## Context and Problem Statement

We need to configure the email templates:
- "webform submission accepted"
- "email confirmation"

## Considered Options

- 1: Extend mapping handler
- 2: Add separate handlers
-
## Decision Outcome

Option 2: Add separate handlers.

### Reasoning

Both emails are optional, depending on workflow. Also it's simpler to implement.
