# Use code to configure mappers
Status: implemented

## Context and Problem Statement

A first version used yaml in the webform handler form to configure the mapper.
The mapping (serialization, validation) of the actual mappers to the yaml turned out as a huge bloat.

## Considered Options

- 1) A generic webform mapping handler configured in the UI
  - 1a) PHP in the UI is not an option due to security
  - 1b) Yml, bloats the code
  - 1c) Symfony expressions and the builder pattern
- 2) A base class, extended by a concrete handler in a custom module.


## Decision Outcome

Option 2: Use handler class in custom module to configure the mapper.

### Consequences

- Coding needs skills (but yaml needed no less)
- Mapper configuration is always deployed code, never contentish.

## Backlog

- If we need configuration by sitebuilder more urgently, implament additionally option 1c.
