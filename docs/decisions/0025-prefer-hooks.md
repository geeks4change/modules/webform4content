# ADR-0025 Prefer hooks
Status: Implemented
## Context and Problem Statement
Group creation and mapping is cumbersome, also update not necessary.

## Decision Outcome
Add (webform to entities) store hooks that change the store process, or add functionality.
So the mapping paradigm is used for what it does best, but for one-time group-and-relation creation use php.
