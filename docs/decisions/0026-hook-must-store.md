# ADR-0026 hookMustStore
Status: Implemented
## Context and Problem Statement
Restricting entity storing to creation (not update) is a cumbersome and brittle process,
involving a play together of php and switch element.
Then a reference element is loaded and stored unchanged.
Or, without switch, field items are loaded and stored unchanged.

## Decision Outcome
Add a hookMustStore():bool that enables or disables storing of any entity.

## Reasoning
The described process is brittle, also explicitly disabling storing is better auditable than hiding fields
(which must happen additionally).
No, better: Make the hook disable storing **and** disable the fields.
