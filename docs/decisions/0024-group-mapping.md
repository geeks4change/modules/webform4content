# ADR-0024 Group Mapping
Status: Implemented

## Context and Problem Statement
Creating or even updating group relations blows up some basic assumptions.

## Decision Outcome
Leverage EntityGroupField to do the PostSave magic.
Add a new store / save API and ReferenceItemMapper::inhibitSaving

## Reasoning
### The problem
In bnn use case, we have these relations:
- GroupContent
  - Group
  - Node
    - User
- GroupMembership
  - Group
  - User

### Challenges
- 1) Group and User are related twice. So the reference tree is not a tree anymore.
  - which may break assumptions when iterating fetch / store
- 2) The node to update is *not* a root entity.
  - Which breaks the assumption that only root entities can be prepared.
  - Also fetch / store does not know how to follow a reverse entity.

### Options for 1
- 1a) Try if it simply works (which may well be the case, because fetch ans store should be idempotent).
- 1b) (If not) Implement some fetch / store list and skip if already done.

Use 1b in the new API.

### Options for 2
- 2a) What if we allow Node as non-root entity to be populated?
- May work fine on create.
- But on update, how is GroupContent fetched?
- Populating GroupContent fron Node is not uniquely possible. (It is only unique if the GroupType is known.)
- GroupMembership is even harder, as it is N:N.
- 2b) Leveraging EntityGroupField might be an option.
  - But recursive saving then tries to save before the node, which throws.
  - 2b1) Add option to defer saving for such references.

Use 2b1.

### Gotcha: Inconsistent inhibited saving
- Inhibited saving is now a property of the reference item mapper.
- Which may break (depending on the tree structure) when GroupContent is referenced by a inhibited-saving reference (EntityGroupField) **and** a regular reference.
  - Solution: DO not do that.
