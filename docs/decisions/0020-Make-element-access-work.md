# ADR-0020 Make element access work
## Context and Problem Statement
Element access introduced in [ADR-0018](0018-Validation-Overhaul.md) does not work, because some target entities must be granted access, even if current user does not have it. Element access was then disabled.

## Decision outcome
Add methods to EntityMapper: ::grantEntityAccess, ::grantFieldAccess.

## Reasoning
