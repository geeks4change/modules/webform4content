# {short title of solved problem and solution}
Status: proposed

## Context and Problem Statement

How do we model the workflows defined in [ADR-0006](0006-possible-workflows.md)?

## Considered Options

- 1: Single StateMachine workflow field with multiple workflows
- 2: Single StateMachine workflow field with fixed workflow but configurable transitions
- 2: Single StateMachine workflow field with universal workflow
- 4: Separate fields or other

## Decision Outcome

Option 3: Add universal workflow. Add option to configure workflow (later: also access, entity-create-route).

### Details

Workflow:
- Sumbitted
- (forNoModeration) Confirmed/Mapped/NoMpderation
- (forPostModeration) Confirmed/Mapped/NeedsPostModeration
  - Reviewed
- (forPreModeration) Confirmed/NeedsPreModeration
  - Accepted/Mapped
  - Rejected

### Reasoning

- Option 2 is likely to be simplest, maybe 1. Decide while coding.
- Add pre / post / no-moderation as option of the mapping.

## Pros and Cons of the Options

### 1: Single workflow field with multiple workflows

- Provide all 3 different workflows.
- Fix workflow per handler OR add a current-workflow field.
- Find a way to figure out in code if and when email confirmation must happen.
- Find a way to figure out in code if and when mapping must happen.
- Provide views for pre- and post-moderation.

### 2/3: Single StateMachine workflow field with fixed workflow...

- Provide 1 universal workflow
- Find a way to figure out in code which transition to take.
- Provide views for pre- and post-moderation.

### 3: Separate fields or workflows

- Have different fields for confirmed, accepted, reviewed.
- All teh workflow goes into code.

