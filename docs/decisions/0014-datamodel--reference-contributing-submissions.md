# Reference all contributing webform submissions in ERR field of target entity
Status: accepted

## Context and Problem Statement

Sometimes it would be nice to reference all contributing webforms (although i forgot the exact use case).

## Considered Options

- 1: Reference the entity revision from the webform submission.
- 2: Reference the webform submission from the entity.
  - 2a: A single valued webform submission reference on the target entity references the webform submission that changed the entity.
  - 2b: A multi valued webform submission reference on the target entity references all webform submissions that changed the entity.

## Decision Outcome

Option 1: Once we need it, use 2b.

## Reasoning

- There is no entity revision reference webform element.
