# Workflow state change formatter hacked above webform submission view
Status: implemented

## Context and Problem Statement

We need a form to change workflow state. Where to display it? How to implement?

## Considered Options

- 1: Hack it together

## Decision Outcome

Option 1: Hack state change formatter above webform submission view

## Reasoning

Where to trigger the state change?
- The submission list allows to display the state change formatter via views, but usually it does not have enough information to decide.
- So we need it on the webform submission view.

How to implement
- Webform submission does not have configurable, instead hardcoded view display.
- See WebformSubmissionViewController::view, WebformSubmissionViewBuilder::buildComponents, EntityViewDisplay::collectRenderDisplays
- It still uses internally a display object.
- Alter that in  WebformSubmissionWrapper::staticHookEntityViewDisplayAlter.

