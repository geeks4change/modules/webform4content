# ADR-0019: Make references work again
Status: Done

## Context and Problem Statement
Proper handling of references was lost in the battle or never thoroughly implemented.
It is not spotted in tests, because they contain too many tracker elements.
Also, some references only make sense on create, but not on update.

## Decision outcome
- Start a handler creation readme

Overhaul trackers
- Make Tracker optional (::setTracker, ::setName)
- Move tracker and bundle access to EntityMapper
- Throw when tracker is needed but not there
- Later: Consider prepopulating to entity instead of tracker
- Find a way to unify tracker and ref

Overhaul Stitch / select
- Add wrappers: Switch, select, onlyOnCreate

...and an awful lot more that evolved from all this.

## Reasoning
- Think of a node with an author and a media reference. On mapping webform to entities, the node must be populated by a tracker element, but the others must not.
- For media, another edit may have changed the referenced media item, so the tracker item is possibly wrong.
- For author, the same is true.
- Also, the author property usually is set on create only, and is not to be updated.
  - Q: How to model that? (Maybe sth like "reference on creation only".)
- For both cases, there is a need for sth like a "select existing or create new "
  - Q: How to model that?
- Any chance of unifying entity population by tracker and by reference?
  - Maybe no need for tracker anyway.
- Also, the bundle-to-create is introspected from tracker element bundle selection, but this breaks down when a select element uses view selection.
- Also, the current mandatory tracker limits entities to one bundle only, which is wrong for referenced entities.
- Q: Require tracker to always be there or not?
  - Even if tracker gets optional, requiring it may be a good intermediate step.

#### Research on tracker:
- Dropping tracker would break:
  - AutoAcceptCondition (needs tracker to extract entity from webform)
  - RouteOverride (needs tracker to access allowed bundles)
- So in effect, tracker is needed to store current (main) entity for these.

#### Refactoring note:
- Creating an entity has long worked with $webformSubmission data alone, and is nested deeply in delegation calls. This makes refactoring towards sth like a reference tracker difficult.

#### On references:
- It seems best to wrap EntityMapper
- Before that, clean up api and extract interface
- Controller::addEntityMapper needs a small change to accept a wrapper later
- Decide first if TrackerInterface is absorbed into EntityMapperInterface

#### On wrapping EntityMapper
- EntityMapper has the switch functionality built in, so it's not easy to extract an interface, and create a switch wrapper.

#### On UserMapper - wha does it do?
- Set currentUser => ElementWrapper (checked that webform does not have this)
- Random PW => Drop.
- Welcome Email => Drop, webform email is fine.
- Load user from email in prepareForm => ???
  - This is unsafe in ::prepareForm
  - It is needed in StoreData though, as email must be unique for users
  - As email is confirmed then, that is safe
- Note: email validation is in EmailConfirmation::ignoreViolation

#### FetchEntity overhaul
- EntityMapper::fetchEntity/createEntity is used both in FetchData and StoreData.
- Is this correct?
- As of createEntity, this is fine.
  - When there's no entity, creation mappers need an entity to fetch/store.
  - And selection mappers are fine with an unsaved entity.
- As of fetchEntity though...
  - Currently, it reads ID from tracker. We do not want this anymore. Tracker will be a storage for posterity, and an optional one, and not an internal storage anymore.
  - So try reading from formState in the constructors.
  - No, formState is not available in some calls, like in preSave, so move storage to webformSubmission.

#### EntityProviders - in fact it should be only one
- EntityProviders started to collect routeOverrides, so multiple are possible.
- Naked webform for creation is possible, too, but maybe we don't need to support it.
- However, only one is active at any time.
- If entityFetch is delegated to provider, it must be unique.
- So, factor together providers.

#### SelectMapper needs more code move
- Current code gets ItemMappers and works.
- All ItemMapper work should live in EntityMapper though.
  - Fetch and Store
  - Mapping violations to elements
  - Mapping field access to elements

## Remaining Todos
- [X] Obsolete ::getItemMappersXXX methods
- [X] Create EntityMapperWrapper: DefaultToCurrentUser
- [X] Drop element access mapping
- [X] Drop EntityMapper::getItemMappers
- [X] Create EntityMapper: SelectMapper
- [X] Create ReferenceProvider
- [X] Fix TestHandler
- [X] Fix nodeAddPage
- [X] Drop $entityMapper->prepareForm
- [X] Move tracker and provider from EntityMapper to Controller
- [X] Make tracker required again
- [X] Fix MappedEntityCondition
- [X] Re-create switch
  - [X] Create Switch: ConditionalStore(WebformSubmissionCondition, EntityMapper, EntityMapper)
  - [X] Create WebformSubmissionCondition: ElementValue, Not, Any
- [X] Document EntityMapper
- [X] Document EntityMapperController
- [X] Document Provider
- [X] Document Tracker
- [X] Fix entity creation for all use cases: webform, routeOverride, confirm, moderate
- [X] Consider implementing reference as ItemMapper wrapping EntityMapper
- [/] Fix field access
  - [X] Drop write access
- Unify webform and form access
  - [/] (Later) Move tracker preparation magic to hookWebformLoad.
  - [/] (Later) Restrict WebformSubmission create access like defined.
  - [/] (Later) Then simply respect WebformSubmission create access in override
  - [/] (Later) Allow to restrict node create / update access.
  - [/] (Later) Add original node form to selection page
  - [/] Document
- Allow non-saved WebformSubmissions
- Cleanup
  - [X] Convert listeners to magic
  - [X] (abandoned) Drop our prepopulation logic
  - [X] Consider using WebformSubmissionWrapper everywhere
  - [X] Rename s/bundle/bundleName/
  - [X] Rename s/elementName/elementKey/

## To update
- All handlers, extensively
- Email confirmation link token changed to confirmation:response-url

## Handler creation docs
- EntityMappers are saved in order, so order matters.
- Tracker element is optional, and only saves created or updated entities to document what happened.
- Tracker element is not prepopulated on update anymore, so legacy webform logic may have to be updated.
  - Instead, use a (maybe hidden) select wrapper and element.
  - If needed, leverage Switch wrapper around EntityMapper.
  - If needed, leverage DefaultToCurrentUser wrapper around Select entity mapper.

## API changes
```php
    $nodeMapper = new EntityMapper('node');
    $nodeMapper->setTracker(new ElementTracker('node'));
    $node2Mapper = new EntityMapper('node');
    $node2Mapper->setTracker(new ElementTracker('node2'));
    $controller->add($nodeMapper);
    $controller->add($nodeMapper, 'node2');
```
