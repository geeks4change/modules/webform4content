# Handler creation

# Handler
- EntityMappers are saved in order, so order matters.
- Tracker element is required for root mappers, it es needed to save the entity to update between webform submission creation and moderation.
- Tracker element is optional for reference mappers, and only saves created or updated entities to document what happened.

## Access (Security!)
- The module is designed to provide public proxy proposal or edit to public entities, which may or may not need additional email confirmation and moderation step.
- Proxy access means, accepted WebformSubmissions update the configured content without further access checks. Neither submitter nor moderator need additional privileges.
- If part of the content are to be protected, restricting access to webform elements should work fine.
- When using it for other, confidential content, think twice.

## Access in conjunction with JSON:Api (Security!)
- When JSON:Api or similar is enabled, entities can be created and updated via the API, without any form. So never rely on form access alone.
- Even when WebformSubmission elements need a special [Module](https://www.drupal.org/project/json_api_webform_submissions) to be manipulated, think twice.
- Also, overriding a node form with a RouteOverride that returns AccessDenied does not protect the for. Think twice.
