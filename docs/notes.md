# Notes
The heap of historical notes which may or may not be true anymore.

-----------------


# Algorithm
Bidirectional mapping is performed in *Webform4ContentHandlerBase* like this:
- Webform4ContentHandlerBase::prepareForm
  - Load-or-create webform, prepopulate it, and map back any target entities referenced by tracker elements.
- Webform4ContentHandlerBase::alterForm
  - If webform is already submitted, prevent re-use by disabling it, and add link to new webform with prepopulated tracker elements.
- Webform4ContentHandlerBase::validateForm
  - Map webform submission data to entities, apply typed data validation on them, and map back errors, but only for visible elements.
- Webform4ContentHandlerBase::preSave
  - Call WebformSubmissionWrapper::preSave
    - Adjust workflow state: draft / submitted
  - If workflow state changes to *accepted*, perform the mapping and iv valid, save without access checks.
- Webform4ContentHandlerBase::postSave
  - Call WebformSubmissionWrapper::postSave
    - Request email confirmations for added emails.
    - Adjust workflow state: submitted / confirmed
- WebformSubmissionWrapper::staticHookEmailConfirmer
  - If confirmed and no email left to confirm, apply workflow state transition 'confirm_all_emails'
- WebformSubmissionWrapper::staticHookEntityViewDisplayAlter
  - Display state change formatter above applicable webform submission display.
- Webform4ContentEmailHandlerForEmailConfirmation::staticHookMailAlter
  - Add our configured email text, overriding the globally configured one.
## Special elements
- Tracker is the element that stores the entity id, and triggers loading of values on re-edit or prepopulate
- Switch decides if an entity is saved at all. On re-edit it is prepopulated if the entity exists.

-----------------


## Test case
- Fill in webform
  - Form entries will be validated against mapped targets
  - ...except "Email is taken"
  - If user is logged in, user data is pre-filled and disabled, no confirmation needed.
- Submit webform (create_user=yes)
  - Webform is saved, state "submitted"
  - Comfirmation mail will be sent
  - (Or if nothing to confirm, state is "emails_confirmed")
- Use link from confirmation mail
  - On submitting "cancel", webform goes to state "email_cancelled"
  - On submitting "confirm", webform goes to state "emails_confirmed"
- View submission list
  - Shows submissions, filterable by "confirmed"
- Go to "view" of confirmed submission
  - Shows submission data, plus "Accept" / "Reject" action buttons
- On "Reject", state goes to "rejected"
- On "Accept", state gote to "accepted,
  - and all content is mapped,
  - if user with that email does not exist yet, it is created, populated with fields, and made owner
  - if user with that mail does exist, it is made owner, but no updates (here: username) are made
- On edit of submitted webform, webform is disabled and link to tracker-prepopulated new one provided (but see todo)
- On edit of tracker-prepopulated webform, fields are pre-filled with content values (but only if access)
- On submit, the process is repeated, existing content updated

-----------------

# What it does
(Some early notes.)

## Functionality

- Webform fields can be (back-)mapped to node fields, known issues:
  - date range fields (webform has no date range field type)
  - .. ?
- Webform fields can be (back-)mapped into a node form json field to allow for flexible content structure defined in webform
  - How do we determine which fields are mapped to json, which to node and which shall not be mapped
- Updating content
  - json fields: only via (new) webform submission
  - node fields: node form only OR webform only? configurable?
- Editors can accept or reject submissions
- On accepting a submission the mapper optionally creates:
  - a group from webform fields
  - a user from webform fields
  - group content
  - group membership
- On accepting a submission the mapper optionally sends:
  - email confirm of submitted email address
  - email confirmed notification to editors
  - user account email notification (once editor accepts submission), optionally includes link to created content
- The mapper respects permissions

### TBD

- The node revision history links to the submissions that created/changed the node
