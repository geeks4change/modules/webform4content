<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

final class Email {

  protected string $subject;
  protected string $body;

  public function __construct(string $subject, string $body) {
    $this->subject = $subject;
    $this->body = $body;
  }

  public function getSubject(): string {
    return $this->subject;
  }

  public function getBody(): string {
    return $this->body;
  }

}
