<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

trait AssertionTrait {

  /**
   * @return array|false|int|string|null
   */
  public function getCurrentPath() {
    return parse_url($this->getSession()->getCurrentUrl(), PHP_URL_PATH);
  }

  protected function assertFormFieldsAreEqual(array $expectedValues) {
    $values = [];
    foreach ($expectedValues as $field => $value) {
      $node = $this->assertSession()->fieldExists($field);
      $values[$field] = strval($node->getValue());
    }
    $this->assertSame($expectedValues, $values);
  }

  protected function assertHiddenFieldsAreEqual(array $expectedValues) {
    $values = [];
    foreach ($expectedValues as $field => $value) {
      $node = $this->assertSession()->hiddenFieldExists($field);
      $values[$field] = strval($node->getValue());
    }
    $this->assertSame($expectedValues, $values);
  }

}
