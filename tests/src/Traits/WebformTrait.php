<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation;

trait WebformTrait {

  protected function submitWebform(string $path, array $data): void {
    $this->drupalGet($path);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($data, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);
  }

  protected function getLastWebformSubmissionIdFromList(): string {
    $this->drupalget('/admin/structure/webform/manage/webform4content_test/results/submissions', ['query' => ['state' => 'All']]);
    $this->assertSession()->statusCodeEquals(200);
    $viewLink = $this->getSession()
      ->getPage()
      ->find('css', 'form#views-form-webform4content-test-embed-1 td.views-field-view-webform-submission a');
    $viewUri = $viewLink->getAttribute('href');
    $this->assertStringContainsString('/admin/structure/webform/manage/webform4content_test/submission/', $viewUri);
    $parts = explode('/', $viewUri);
    $webformSubmissionId = end($parts);
    $this->assertIsNumeric($webformSubmissionId);
    return $webformSubmissionId;
  }

  protected function triggerLastWebformSubmissionStateChangeInUI(?bool $shallAccept): void {
    $webformSubmission = $this->getLastWebformSubmission();
    $this->drupalGet($webformSubmission->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    // Test that no state change button is there **before** confirmation.
    if (!isset($shallAccept)) {
      $this->assertSession()
        ->elementNotExists('css', "form.state-machine-transition-form input:not([type=hidden])");
    }
    else {
      // Test that there's only accept and reject links.
      $this->assertSession()
        ->elementExists('css', "form.state-machine-transition-form #edit-actions a#edit-accept");
      $this->assertSession()
        ->elementExists('css', "form.state-machine-transition-form #edit-actions a#edit-reject");
      $this->assertSession()
        ->elementsCount('css', "form.state-machine-transition-form #edit-actions a", 2);

      // Click the link to get the confirmation page.
      $this->getSession()->getPage()->clickLink($shallAccept ? 'edit-accept' : 'edit-reject');
      $this->assertSession()->statusCodeEquals(200);

      // Click the confirm link for the confirm or reject action.
      $this->getSession()->getPage()->pressButton('edit-submit');
      $this->assertSession()->statusCodeEquals(200);
    }
  }

  protected function assertLastWebformSubmissionStateIs(string $stateValue): void {
    $webformSubmission = $this->getLastWebformSubmission();
    $this->assertNotEmpty($webformSubmission, 'Got WebformSubmission');
    $this->assertSame($stateValue, $webformSubmission->get(WebformSubmissionWrapper::FIELD_STATE)
      ->getString());
  }

  protected function loginAsWebformSubmissionAdmin(): void {
    $submissionAdmin = $this->drupalCreateUser([
      'administer webform submission',
      'view any webform submission',
      'edit any webform submission',
    ]);
    $this->drupalLogin($submissionAdmin);
  }

  /**
   * @return void
   */
  protected function assertLastWebformSubmissionIsInUIList(): void {
    $webformSubmissionIdFromUi = $this->getLastWebformSubmissionIdFromList();
    $this->assertSame($webformSubmissionIdFromUi, $this->getLastWebformSubmission()
      ->id());
  }

  public function setWebformCreateAccess(bool $forAll): void {
    $this->config('webform.webform.webform4content_test')
      ->set('access.create.roles', $forAll ? ['anonymous', 'authenticated'] : [])
      ->save();
  }

}
