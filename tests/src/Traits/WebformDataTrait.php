<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

trait WebformDataTrait {

  public function getWebformSubmissionTestData(int $serial, bool $withUserData): array {
    $data[1] = [
      'title' => 'MyTitle',
      'body' => 'MyBody',
      'number' => '13',
      'bool' => '',
      'jtext' => 'j text',
      'jnumber' => '42',
      'jbool' => '1',
      'timestamp[date]' => '1999-01-01',
      'timestamp[time]' => '13:37:01',
      'date[date]' => '1999-01-02',
      'date[time]' => '13:37:02',
      'date_range_start[date]' => '1999-01-03',
      'date_range_start[time]' => '13:37:03',
      'date_range_end[date]' => '1999-01-04',
      'date_range_end[time]' => '13:37:04',
    ];
    $data[2] = [
      'title' => 'MyTitleTwo',
      'body' => 'MyBody 2',
      'number' => '17',
      'bool' => '1',
      'jtext' => 'j text two',
      'jnumber' => '43',
      'jbool' => '',
      'timestamp[date]' => '1999-02-01',
      'timestamp[time]' => '13:38:01',
      'date[date]' => '1999-02-02',
      'date[time]' => '13:38:02',
      'date_range_start[date]' => '1999-02-03',
      'date_range_start[time]' => '13:38:03',
      'date_range_end[date]' => '1999-02-04',
      'date_range_end[time]' => '13:38:04',
    ];
    $userData = [
      'create_user' => '1',
      'email' => 'test1@example.com',
      'username' => 'MasterOfDesaster',
    ];
    return $withUserData ? $userData + $data[$serial] : $data[$serial];
  }

}
