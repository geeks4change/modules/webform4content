<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

trait MailSafetyTrait {

  /**
   * Copied from @see \Drupal\mail_safety\Controller\MailSafetyController::load
   */
  protected function getLastEmail(): ?Email {
    $connection = \Drupal::database();
    $query = $connection->select('mail_safety_dashboard', 'msd');
    $query->fields('msd', ['mail']);
    $query->orderBy('sent', 'DESC');
    $query->range(0, 1);
    $result = $query->execute()->fetchField();
    if ($result) {
      $emailArray = unserialize($result);
      return new Email($emailArray['subject'], implode("\n", $emailArray['body']));
    }
    else {
      return NULL;
    }
  }

}
