<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

use Drupal\confirmation\Entity\ConfirmationInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation;

trait EntityTrait {

  protected function assertEntityFields(FieldableEntityInterface $entity, array $values): void {
    // Pass multiple arrays to array_map does not work here, as it returns integer keys.
    $fieldValues = [];
    foreach ($values as $fieldName => $value) {
      $fieldItemList = $entity->get($fieldName);
      $fieldValues[$fieldName] = is_string($value)
        // ::getString does not do that, but concat e.g. value and format
        // of formatted text.
        ? $fieldItemList->first()?->get($this->getMainPropertyName($fieldItemList))->getValue()
        : $fieldItemList->getValue();
    }
    $message = sprintf('%s fields', $entity->getEntityType()->getLabel());
    $this->assertSame($values, $fieldValues, $message);
  }

  private function getMainPropertyName(FieldItemListInterface $fieldItemList): string {
    $itemClass = $fieldItemList->getItemDefinition()->getClass();
    assert($itemClass instanceof FieldItemInterface);
    return $itemClass::mainPropertyName();
  }

  protected function reLoadEntity(EntityInterface &$entity): void {
    // MUST use ::loadUnchanged, as entity update on SUT does not invalidate static cache.
    $entity = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
  }

  private function getLastEntityOfType(string $entityTypeId, string $bundleName = NULL): ?EntityInterface {
    $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
    // When using the web UI, everything is fine, because caches are fresh on
    // every new request, which takes a lot of time and resources of course.
    // On the test system though, the memory cache is never cleared, even when
    // we do changes via the UI. Which means, we have stale entities.
    // Prevent this.
    $storage->resetCache();

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entityType */
    $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);
    $idKey = $entityType->getKey('id');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->sort($idKey, 'DESC')
      ->range(0, 1);
    if ($bundleName) {
      $bundleKey = $entityType->getKey('bundle');
      $query->condition($bundleKey, $bundleName);
    }
    $ids = $query->execute();
    // EntityStorageBase does not invalidate memory cache on other webheads,
    // so use ::loadUnchanged().
    return $ids ? $storage->load(reset($ids)) : NULL;
  }

  protected function getLastWebformSubmission(): ?WebformSubmissionInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('webform_submission');
  }

  protected function getLastNode(): ?NodeInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('node');
  }

  protected function getLastUser(): ?UserInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('user');
  }

  protected function getLastEmailConfirmation(): ?WebformSubmissionConfirmation {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('confirmation');
  }

  protected function confirmConfirmation(ConfirmationInterface $confirmation, bool $state = TRUE) {
    $confirmation->setStateConfirmed();
    $confirmation->save();
  }

  protected function getLastGroup(): ?GroupInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('group');
  }

  protected function getLastGroupContent(string $type): ?GroupContent {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getLastEntityOfType('group_content', $type);
  }

}
