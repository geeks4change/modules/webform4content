<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

trait SetupJsonNodeTrait {

  protected NodeInterface $node;

  public function setupNode(): void {
    $this->node = Node::create([
      'type' => 'webform4content_test_json',
      'title' => 'Test',
      'field_webform4content_test_json' => json_encode([
        'text' => ['Hello'],
        'checkbox' => [1],
      ]),
    ]);
    $this->node->save();
  }

}
