<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

trait ConfirmationTrait {

  protected function getLastConfirmationLink(?Email $email): string {
    $this->assertNotEmpty($email);
    preg_match('~https?://[^/]+(/confirmation/\S+)~u', $email->getBody(), $matches);
    $this->assertTrue(isset($matches[0]), sprintf("No confirmation link found in body: %s", $email->getBody()));
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $confirmationLink = $matches[0];
    return $confirmationLink;
  }

  protected function confirmEmail(bool $confirm): void {
    $confirmationLink = $this->getLastConfirmationLink($this->getLastEmail());
    $this->drupalGet($confirmationLink);
    $this->submitForm([
      'state' => $confirm ? 'confirm' : 'disconfirm',
    ], t('Submit'));
    // This does not work sometimes, but we check the status later anyway.
    // $confirmationMessage = $confirm ? 'Email confirmation confirmed.' : 'Email confirmation cancelled';
    // $this->assertSession()->elementTextContains('css', 'div[data-drupal-messages]', $confirmationMessage);
  }

}
