<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Traits;

use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

trait BrowserTestSetupTrait {

  protected function setUp(): void {
    parent::setUp();
    // Enable mail logging.
    $this->config('mail_safety.settings')->setData([
      'enabled' => TRUE,
      'send_mail_to_dashboard' => TRUE,
    ])->save();

    // Allow everyone to confirm emails.
    Role::load(RoleInterface::ANONYMOUS_ID)
      ->grantPermission('access email confirmation')
      ->save();
    Role::load(RoleInterface::AUTHENTICATED_ID)
      ->grantPermission('access email confirmation')
      ->save();
  }

}
