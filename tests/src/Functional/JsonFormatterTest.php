<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\SetupJsonNodeTrait;

/**
 * @group json_field_tools
 */
final class JsonFormatterTest extends BrowserTestBase {

  use SetupJsonNodeTrait;

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'webform4content_test_json',
  ];

  protected function setUp(): void {
    parent::setUp();
    $this->setupNode();
  }

  public function testFormatter() {
    $this->drupalGet($this->node->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Text Hello');
    $this->assertSession()->pageTextContains('Checkbox Yes');
  }

}
