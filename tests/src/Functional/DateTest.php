<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\AssertionTrait;
use Drupal\Tests\webform4content\Traits\BrowserTestSetupTrait;
use Drupal\Tests\webform4content\Traits\ConfirmationTrait;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\MailSafetyTrait;
use Drupal\Tests\webform4content\Traits\WebformDataTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;

/**
 * @group webform4content
 */
final class DateTest extends BrowserTestBase {

  use BrowserTestSetupTrait;
  use AssertionTrait;
  use EntityTrait;
  use MailSafetyTrait;
  use WebformTrait;
  use ConfirmationTrait;
  use WebformDataTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  protected RendererInterface $renderer;

  public function testDate() {
    $this->renderer = \Drupal::service('renderer');

    $permissions = ['create webform4content_test content'];
    $user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($user);

    // Add node with dataset #1 via webform.
    $this->drupalGet('/node/add/webform4content_test', ['query' => ['webform' => 'webform4content_test']]);
    $this->assertSession()->statusCodeEquals(200);
    $data1 = $this->getWebformSubmissionTestData(1, FALSE);
    $this->submitForm($data1, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertLastWebformSubmissionStateIs('accepted');

    // Verify date set correctly.
    $node = $this->getLastNode();
    $this->assertNotEmpty($node);

    // Someday, do it nicer:
    //   - create a date format
    //   - use that date format in default display
    //   - remove the config here
    //   - also, dump date object while mapping
    $renderArray = $node->get('field_w4c_timestamp')->view([
      'label' => 'hidden',
      'type' => 'timestamp',
      'settings' => [
        'date_format' => 'custom',
        'custom_date_format' => \DateTime::ATOM,
        'timezone' => '',
      ],
    ]);
    $rendered = trim(strip_tags(strval($this->renderer->renderPlain($renderArray))));
    $this->assertSame('1999-01-01T13:37:01+11:00', $rendered);
  }

}
