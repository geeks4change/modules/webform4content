<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\AssertionTrait;
use Drupal\Tests\webform4content\Traits\BrowserTestSetupTrait;
use Drupal\Tests\webform4content\Traits\ConfirmationTrait;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\MailSafetyTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;

/**
 * @group webform4content
 */
final class ComputedTextTest extends BrowserTestBase {

  use BrowserTestSetupTrait;
  use AssertionTrait;
  use EntityTrait;
  use MailSafetyTrait;
  use WebformTrait;
  use ConfirmationTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  /**
   * @covers \Drupal\webform4content_test\Plugin\WebformHandler\Webform4ContentComputedTestHandler
   */
  public function testComputedText() {
    // Create user with just enough node permissions.
    $user = $this->drupalCreateUser([
      'create webform4content_computed content',
      'edit any webform4content_computed content',
      'use text format webform4content_html',
    ]);
    $this->drupalLogin($user);

    // Add node with dataset #1 via webform.
    $this->drupalGet('/node/add/webform4content_computed', ['query' => ['webform' => 'webform4content_computed']]);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'text1' => 'Foo',
      'text2' => 'Bar',
    ], t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertLastWebformSubmissionStateIs('accepted');

    // Edit node, verify data set #1.
    $node = $this->getLastNode();
    $this->assertNotEmpty($node);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertFormFieldsAreEqual([
      'formatted[value]' => "<h1>Text1</h1>\nFoo\n<h1>Text2</h1>\nBar",
    ]);
    // Without ckeditor5
    // $this->assertSession()->elementTextEquals('css', '#edit-formatted-format div[data-drupal-format-id] h4', 'webform4content_html');
    $this->assertHiddenFieldsAreEqual(['formatted[format]' => 'webform4content_html',]);

    // Re-submit form.
    $this->submitForm([], t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertLastWebformSubmissionStateIs('accepted');
  }

}
