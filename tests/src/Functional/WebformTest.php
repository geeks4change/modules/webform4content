<?php

declare(strict_types=1);

namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\AssertionTrait;
use Drupal\Tests\webform4content\Traits\BrowserTestSetupTrait;
use Drupal\Tests\webform4content\Traits\ConfirmationTrait;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\MailSafetyTrait;
use Drupal\Tests\webform4content\Traits\WebformDataTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;

/**
 * @group webform4content
 */
final class WebformTest extends BrowserTestBase {

  use BrowserTestSetupTrait;
  use AssertionTrait;
  use EntityTrait;
  use MailSafetyTrait;
  use WebformTrait;
  use ConfirmationTrait;
  use WebformDataTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  /**
   * @dataProvider provideTestWebformData
   */
  public function testWebform(
    bool $emailConfirmationStateToSet,
    string $webformSubmissionStateAfterEmailConfirm,
    ?bool $moderationShallAccept,
    string $webformSubmissionStateAfterModeration
  ) {
    // Why does the alias /form/webform4content-test not work here?
    $this->submitWebform('/webform/webform4content_test', $this->getWebformSubmissionTestData(1, TRUE));
    $this->assertLastWebformSubmissionStateIs('submitted');

    $emailConfirmation = $this->getLastEmailConfirmation();
    $this->assertSame(NULL, $emailConfirmation->getState());

    $this->confirmEmail($emailConfirmationStateToSet);
    $this->reLoadEntity($emailConfirmation);
    $this->assertSame($emailConfirmationStateToSet, $emailConfirmation->getState());
    $this->assertLastWebformSubmissionStateIs($webformSubmissionStateAfterEmailConfirm);

    $this->loginAsWebformSubmissionAdmin();
    $this->assertLastWebformSubmissionIsInUIList();

    $this->triggerLastWebformSubmissionStateChangeInUI($moderationShallAccept);
    if (isset($moderationShallAccept)) {
      $this->assertLastWebformSubmissionStateIs($webformSubmissionStateAfterModeration);
    }

    if ($emailConfirmationStateToSet && $moderationShallAccept) {
      $node = $this->getLastNode();
      $this->assertNotEmpty($node);
      $user = $this->getLastUser();
      $this->assertNotEmpty($user);
      // @todo Re-activate after fix via (Field|Element)DefaultToCurrentUser.
      // $this->assertSame($user->id(), $node->getOwnerId(), 'Owner is adding user.');
    }
  }

  public function provideTestWebformData() {
    return [
      'confirmAndAccept' => [TRUE, 'emails_confirmed', TRUE, 'accepted'],
      'confirmAndReject' => [TRUE, 'emails_confirmed', FALSE, 'rejected'],
      'noConfirm' =>        [FALSE, 'email_cancelled', NULL, ''],
    ];
  }

}
