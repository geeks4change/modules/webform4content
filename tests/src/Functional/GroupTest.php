<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;
use Drupal\webform4content_test_group\Plugin\WebformHandler\Webform4ContentGroupTestHandler;

/**
 * @group webform4content
 */
final class GroupTest extends BrowserTestBase {

  use WebformTrait;
  use EntityTrait;

  protected static $modules = ['webform4content_test_group'];

  protected $defaultTheme = 'stark';

  public function provideTestGroup() {
    $baseData = [
      'node_title' => 'NodeTitle',
      'group_title' => 'GroupTitle',
    ];
    $userCreateData = [
      'username' => 'UserName',
      'email' => 'free-assange@example.com',
    ];

    foreach ([
      'autocreate' => TRUE,
      'selfcreate' => FALSE,
    ] as $createType => $groupMembershipAutoCreate) {
      $data["anon-$createType"] = [
        NULL,
        $baseData + $userCreateData,
        FALSE,
        $groupMembershipAutoCreate,
      ];
      $data["auth-$createType"] = [
        [],
        $baseData,
        FALSE,
        $groupMembershipAutoCreate,
      ];
      $data["admin-$createType"] = [
        [Webform4ContentGroupTestHandler::SELECT_USER_PERMISSION],
        $baseData,
        TRUE,
        $groupMembershipAutoCreate,
      ];
    }
    return $data;
  }

  /**
   * @dataProvider provideTestGroup
   *
   * @param array|null $userPermissions
   *   Run test as user with these permissions, or anon if null.
   * @param array $formData
   *   Use provided data to post to the form.
   * @param bool $selectDifferentUser
   *   Select a different user with this form element.
   * @param bool $groupMembershipAutoCreate
   *   Whether group membership auto create should be used.
   */
  public function testGroup(?array $userPermissions, array $formData, bool $selectDifferentUser, bool $groupMembershipAutoCreate) {
    $this->config('group.type.w4ct')
      ->set('creator_membership', $groupMembershipAutoCreate)
      ->save();

    $userSelectionElement = 'select_user';

    // Prepare acting user.
    if (isset($userPermissions)) {
      $user = $this->drupalCreateUser($userPermissions);
      $this->drupalLogin($user);
    }

    // Submit.
    $this->drupalGet('/node/add/w4ct_group', ['query' => ['webform' => 'w4ct_group']]);
    if ($selectDifferentUser) {
      // Reference different user.
      $differentUser = $this->drupalCreateUser();
      $differentUserAutocompleteValue = sprintf('%s (%s)', $differentUser->getAccountName(), $differentUser->id());
      $formData[$userSelectionElement] = $differentUserAutocompleteValue;
    }
    else {
      // Test that there is no selection element.
      $this->assertSession()->fieldNotExists($userSelectionElement);
    }
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($formData, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    // Verify webformSubmission
    $webformSubmission = $this->getLastWebformSubmission();
    $this->assertNotEmpty($webformSubmission, 'Got webformSubmission');

    // Anon submission.
    if (!isset($userPermissions)) {
      // Verify workflowState.
      $this->assertLastWebformSubmissionStateIs('submitted');
      // Verify no node created.
      $node = $this->getLastNode();
      $this->assertEmpty($node);
      // Confirm anon.
      $confirmation = $this->getLastEmailConfirmation();
      $this->assertNotEmpty($confirmation);
      $this->confirmConfirmation($confirmation);
    }
    // Verify workflowState.
    $this->assertLastWebformSubmissionStateIs('accepted');

    // Verify user.
    if (!isset($userPermissions)) {
      $referencedUser = $this->getLastUser();
      $this->assertNotEmpty($referencedUser, 'Got user created');
    }
    elseif ($selectDifferentUser) {
      $referencedUser = $differentUser;
    }
    else {
      /** @noinspection PhpUndefinedVariableInspection */
      $referencedUser = $user;
    }
    $this->assertNotEmpty($referencedUser);

    // Verify node.
    $node = $this->getLastNode();
    $this->assertNotEmpty($node, 'Got node');
    $this->assertEntityFields($node, [
      'title' => $formData['node_title'],
      'uid' => $referencedUser->id(),
    ]);

    // Verify group.
    $group = $this->getLastGroup();
    $this->assertNotEmpty($group, 'Got group');
    $this->assertEntityFields($group, [
      'label' => $formData['group_title'],
    ]);

    // Verify groupContent.
    $groupContent = $this->getLastGroupContent('w4ct-group_node-w4ct_group');
    $this->assertNotEmpty($groupContent, 'Got group content');
    $this->assertEntityFields($groupContent, [
      'gid' => $group->id(),
      'entity_id' => $node->id(),
    ]);

    // Additionally verify EntityGroupField.
    $this->assertEntityFields($node, [
      'entitygroupfield' => [['target_id' => $groupContent->id()]],
    ]);

    // Verify groupMembership.
    $groupMembership = $this->getLastGroupContent('w4ct-group_membership');
    $this->assertNotEmpty($groupMembership, 'Got group membership');
    $this->assertEntityFields($groupMembership, [
      'gid' => $group->id(),
      'entity_id' => $referencedUser->id(),
    ]);
  }

}
