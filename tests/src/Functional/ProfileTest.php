<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\AssertionTrait;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;

/**
 * @group webform4content
 */
final class ProfileTest extends BrowserTestBase {

  use AssertionTrait;
  use WebformTrait;
  use EntityTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  /**
   * @dataProvider provideProfilesData
   */
  public function testProfiles(string $webformId, array $data) {
    // Create user with just enough node permissions.
    $user = $this->drupalCreateUser([
      'create webform4content_profile content',
      'edit any webform4content_profile content',
    ]);
    $this->drupalLogin($user);

    // Add profile
    $this->drupalGet('/node/add/webform4content_profile', ['query' => ['webform' => $webformId]]);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($data, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertLastWebformSubmissionStateIs('accepted');

    // Edit profile
    $node = $this->getLastNode();
    $this->assertNotEmpty($node);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertFormFieldsAreEqual($data);

    // Re-submit form.
    $this->submitForm([], t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertLastWebformSubmissionStateIs('accepted');
  }

  public function provideProfilesData() {
    return [
      ['webform4content_profile1', ['email' => 'foo@example.com',]],
      ['webform4content_profile2', ['number' => '99',]],
    ];
  }

}
