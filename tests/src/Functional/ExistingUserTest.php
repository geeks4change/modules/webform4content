<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @group webform4content
 */
final class ExistingUserTest extends BrowserTestBase {

  use WebformTrait;
  use EntityTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  /**
   * Existing username triggers a validation error.
   *
   * This is OK, because usernames are public and checking or even enumerating
   * them is not a security issue.
   */
  public function testExistingUserName() {
    $this->prepareTakenUser();
    $this->prepareAutoAccept();
    $this->submitOurForm(['username' => 'taken']);
    $this->assertSession()->pageTextContains('User name: The username taken is already taken.');
  }

  /**
   * Existing email trigers loading the user with that email.
   *
   * In the default setup, the email field of a user is considered unique, i.e.
   * for any email address there is at most one user.
   * Checking or even enumerating email addresses of existing users must be
   * considered a GDPR violation though.
   * So, validation error for existing email is not an option.
   *
   * If an existing user's email is mapped, the mapper loads that user, and any
   * entered data is mapped to that user.
   * Assuming the email field is configured correctly as EmailConfirmItemMapper,
   * this is safe, because mapping only happens after it confirmation via
   * that email has happened, or mapping is inplicitly confirmed, because the
   * user is already logged in.
   */
  public function testExistingUserEmail() {
    $takenUser = $this->prepareTakenUser();
    $this->prepareAutoAccept();
    $this->submitOurForm(['email' => 'taken@example.com']);

    // Verify workflowState.
    $this->assertLastWebformSubmissionStateIs('submitted');
    // Confirm.
    $confirmation = $this->getLastEmailConfirmation();
    $this->assertNotEmpty($confirmation);
    $this->confirmConfirmation($confirmation);
    // Verify workflowState.
    $this->assertLastWebformSubmissionStateIs('accepted');

    $node = $this->getLastNode();
    $this->assertNotEmpty($node, 'Got node');

    // Existing user with entered email address is used.
    $nodeOwner = $node->getOwner();
    $this->assertSame($takenUser->id(), $nodeOwner->id(), 'TakenUser is node owner.');
    // Tha user's name is updated.
    $this->assertEntityFields($nodeOwner, [
      'name' => 'free',
      'mail' => 'taken@example.com',
    ]);
  }

  public function prepareTakenUser(): User {
    $takenUser = User::create([
      'name' => 'taken',
      'mail' => 'taken@example.com',
    ]);
    $takenUser->save();
    return $takenUser;
  }

  public function prepareAutoAccept(): void {
    // Give anon auto-accept permissions.
    $anonRole = Role::load(Role::ANONYMOUS_ID)
      ->grantPermission('create webform4content_test content');
    $anonRole->save();
  }

  public function submitOurForm(array $data): void {
    $commonData = [
      'title' => 'NodeTitle',
      'create_user' => '1',
      'email' => 'free@example.com',
      'username' => 'free',
    ];
    $this->drupalGet('/node/add/webform4content_test', ['query' => ['webform' => 'webform4content_test']]);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($data + $commonData, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
