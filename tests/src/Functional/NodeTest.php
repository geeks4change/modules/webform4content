<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform4content\Traits\AssertionTrait;
use Drupal\Tests\webform4content\Traits\BrowserTestSetupTrait;
use Drupal\Tests\webform4content\Traits\ConfirmationTrait;
use Drupal\Tests\webform4content\Traits\EntityTrait;
use Drupal\Tests\webform4content\Traits\MailSafetyTrait;
use Drupal\Tests\webform4content\Traits\WebformDataTrait;
use Drupal\Tests\webform4content\Traits\WebformTrait;

/**
 * @group webform4content
 */
final class NodeTest extends BrowserTestBase {

  use BrowserTestSetupTrait;
  use AssertionTrait;
  use EntityTrait;
  use MailSafetyTrait;
  use WebformTrait;
  use ConfirmationTrait;
  use WebformDataTrait;

  protected static $modules = ['webform4content_test'];

  protected $defaultTheme = 'stark';

  /**
   * @dataProvider provideTestNodeData
   */
  public function testNode(bool $withAutoAcceptOnCreate, bool $withAutoAcceptOnUpdate) {
    // No webform access, so this also tests if redirect to node works.
    $this->setWebformCreateAccess(FALSE);

    // Create user with just enough node permissions.
    $permissions = array_merge(
      ($withAutoAcceptOnCreate ? ['create webform4content_test content'] : []),
      ($withAutoAcceptOnUpdate ? ['edit any webform4content_test content'] : [])
    );
    $user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($user);

    // Add node with dataset #1 via webform.
    $this->drupalGet('/node/add/webform4content_test', ['query' => ['webform' => 'webform4content_test']]);
    $this->assertSession()->statusCodeEquals(200);

    $data1toSet = $this->getWebformSubmissionTestData(1, FALSE);
    $data1toTest = $this->getWebformSubmissionTestData(1, FALSE);
    $this->submitForm($data1toSet, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    // Verify redirect to node url.
    if ($withAutoAcceptOnCreate) {
      $node = $this->getLastNode();
      $this->assertNotEmpty($node);
      $this->assertSame($node->toUrl()->toString(), $this->getCurrentPath());
    }

    // Accept if needed.
    if (!$withAutoAcceptOnCreate) {
      $this->assertLastWebformSubmissionStateIs('emails_confirmed');
      $this->loginAsWebformSubmissionAdmin();
      $this->assertLastWebformSubmissionIsInUIList();
      $this->triggerLastWebformSubmissionStateChangeInUI(TRUE);
    }
    $this->assertLastWebformSubmissionStateIs('accepted');

    // Edit node, verify data set #1.
    $this->drupalLogin($user);
    $node = $this->getLastNode();
    $this->assertNotEmpty($node);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'form.node-form');
    $this->assertFormFieldsAreEqual($data1toTest);

    // Submit data set #2
    $data2toSet = $this->getWebformSubmissionTestData(2, FALSE);
    $data2toTest = $this->getWebformSubmissionTestData(2, FALSE);
    $this->submitForm($data2toSet, t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    // Verify redirect to node url.
    $node = $this->getLastNode();
    $this->assertSame($node->toUrl()->toString(), $this->getCurrentPath());

    // Accept if needed.
    if (!$withAutoAcceptOnUpdate) {
      // Test that webform still has #1 data.
      $this->drupalGet($node->toUrl('edit-form'));
      $this->assertSession()->statusCodeEquals(200);
      $this->assertSession()->elementNotExists('css', 'form.node-form');
      $this->assertFormFieldsAreEqual($data1toTest);

      // Trigger accept state transition.
      $this->assertLastWebformSubmissionStateIs('emails_confirmed');
      $this->loginAsWebformSubmissionAdmin();
      $this->assertLastWebformSubmissionIsInUIList();
      $this->triggerLastWebformSubmissionStateChangeInUI(TRUE);
    }
    $this->assertLastWebformSubmissionStateIs('accepted');

    // Verify that webform now has #2 data.
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'form.node-form');
    $this->assertFormFieldsAreEqual($data2toTest);

  }

  public function provideTestNodeData() {
    return [
      // $withAutoAcceptOnCreate, $withAutoAcceptOnUpdate
      [FALSE, FALSE],
      [FALSE, TRUE],
      [TRUE, FALSE],
      [TRUE, TRUE],
    ];
  }

}
