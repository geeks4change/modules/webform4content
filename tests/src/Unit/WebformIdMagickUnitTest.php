<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Unit;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Mapping\Workflow\StateGuard\Webform4ContentStateGuard;

/**
 * @group webform4content
 */
final class WebformIdMagickUnitTest extends UnitTestCase {

  public function testGet() {
    $this->initContainer();

    $webformIdExpected = 'someWebformIdToGet';
    $webform = $this->createMock(WebformInterface::class);
    $webformSubmission = $this->createMock(WebformSubmissionInterface::class);

    $webformSubmission->expects($this->once())
      ->method('getData')->will($this->returnValue([]));
    $webformSubmission->expects($this->once())
      ->method('getWebform')->will($this->returnValue($webform));
    $webform->expects($this->once())
      ->method('id')->will($this->returnValue($webformIdExpected));

    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    $webformId = $webformSubmissionWrapper->getElementItems(WebformSubmissionWrapper::WEBFORM_ID_KEY);
    $this->assertSame([$webformIdExpected], $webformId);
  }

  public function testSet() {
    $this->initContainer();

    $webformIdExpected = 'someWebformIdToSet';
    $webformSubmission = $this->createMock(WebformSubmissionInterface::class);

    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    $webformSubmissionWrapper->initFetchData();
    $this->assertSame(NULL, $webformSubmissionWrapper->getMaybeFetchedWebformId());
    $webformSubmissionWrapper->setElementItems(WebformSubmissionWrapper::WEBFORM_ID_KEY, [$webformIdExpected]);
    $this->assertSame($webformIdExpected, $webformSubmissionWrapper->getMaybeFetchedWebformId());
  }

  public function initContainer(): void {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->any())
      ->method('get')
      ->will($this->returnValueMap([
        ['entity_type.manager', ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE, $this->createMock(EntityTypeManagerInterface::class)],
        ['webform4content.state_guard', ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE, new Webform4ContentStateGuard()],
      ]));
    \Drupal::setContainer($container);
  }

}
