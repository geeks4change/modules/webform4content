<?php

declare(strict_types=1);

namespace Drupal\Tests\webform4content\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\webform4content\Mapping\Utility\RoutePatternMatcher;
use ReflectionClass;

/**
 * @group webform4content
 */
final class RoutePatternTest extends UnitTestCase {

  /**
   * @dataProvider provideMatchRouteData
   */
  public function testMatchRoute(bool $result, string $routeNaneWithParameters, string $route, ?array $parameters = NULL) {
    $this->assertSame($result, RoutePatternMatcher::matchRoute($routeNaneWithParameters, $route, $parameters));
  }

  public function provideMatchRouteData() {
    return [
      'routeYo' => [TRUE, 'my.route?para=val', 'my.route'],
      'routeNo' => [FALSE, 'other.route?para=val', 'my.route'],
    ];
  }

  /**
   * @dataProvider provideMatchRouteMatchData
   */
  public function testMatchRouteMatch(bool $result, string $routeNaneWithParameters, string $route, ?array $parameters = NULL) {
    $this->assertSame($result, RoutePatternMatcher::matchRouteMatch($routeNaneWithParameters, $route, $parameters));
  }

  public function provideMatchRouteMatchData() {
    return [
      'routeParaYo' => [TRUE, 'my.route?para=val', 'my.route', ['para' => 'val', 'other' => 'stuff']],
      'routeParaNo' => [FALSE, 'my.route?para=val', 'my.route', ['para' => 'other', 'other' => 'stuff']],
      'routeParaNoMatchEmpty' => [FALSE, 'my.route?para=val', 'my.route', []],
      'routeParaOrderForth' => [TRUE, 'my.route?p1=1&p2=2', 'my.route', ['p1' => 1, 'p2' => '2']],
      'routeParaOrderBack' => [TRUE, 'my.route?p1=1&p2=2', 'my.route', ['p2' => '2', 'p1' => 1]],
    ];
  }

  /**
   * @dataProvider provideParseData
   */
  public function testParse(string $routeNameWithParameters, string $routeName = NULL, array $parameters = NULL) {
    $parse = $this->getPrivateStaticMethod(RoutePatternMatcher::class, 'parse');
    // Avoid deprecation errors.
    $routeNameReturned = $parametersReturned = NULL;
    $parse->invokeArgs(NULL, [$routeNameWithParameters, &$routeNameReturned, &$parametersReturned]);
    $this->assertSame($routeName, $routeNameReturned);
    $this->assertSame($parameters, $parametersReturned);
  }

  public function provideParseData() {
    return [
      'noPara' => ['foo', 'foo', []],
      'hasPara' => ['foo?bar=baz&boom=bang', 'foo', ['bar' => 'baz', 'boom' => 'bang']],
    ];
  }

  protected function getPrivateStaticMethod(string $class, string $method): \ReflectionMethod {
    $class = new ReflectionClass($class);
    $method = $class->getMethod($method);
    $method->setAccessible(true);
    return $method;
  }

}
