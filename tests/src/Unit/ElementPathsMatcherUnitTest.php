<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher\ElementPaths;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher\WithoutPathPatterns;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher\ElementPathsFilterInterface;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher\OnlyPathPatterns;

/**
 * @group webform4content
 */
final class ElementPathsMatcherUnitTest extends UnitTestCase {

  public function testFromElementPaths() {
    $paths = ['foo', 'bar.baa', 'baz.boo.brr'];
    $expectedKeys = ['foo', 'baa', 'brr'];
    $instance = ElementPaths::fromPaths(...$paths);
    $this->assertSame($paths, $instance->paths(), 'Paths match');
    $this->assertSame($expectedKeys, $instance->keys(), 'Keys match');
  }

  /**
   * @dataProvider provideElementPathsMatch
   */
  public function testElementPathsMatch(array $paths, ElementPathsFilterInterface $filter, array $pathsExpected) {
    $pathsResult = ElementPaths::fromPaths(...$paths)
      ->filter($filter)
      ->paths();
    $this->assertSame($pathsExpected, $pathsResult);
  }

  public function provideElementPathsMatch() {
    $paths = ['root1', 'wrap.it', 'wrap.wrap.it', 'wrap.another', 'wrap.even.deeper1', 'wrap.even.deeper2', 'root2'];
    return [
      // Finds simple elements in root.
      [$paths, OnlyPathPatterns::create('root1'), ['root1']],
      [$paths, OnlyPathPatterns::create('root2'), ['root2']],
      // Finds simple elements nested.
      [$paths, OnlyPathPatterns::create('it'), ['wrap.it', 'wrap.wrap.it']],
      [$paths, OnlyPathPatterns::create('deeper1'), ['wrap.even.deeper1']],
      // Finds paths but not nested.
      [$paths, OnlyPathPatterns::create('wrap.it'), ['wrap.it']],
      // Finds name wildcard.
      [$paths, OnlyPathPatterns::create('wrap.i*'), ['wrap.it']],
      [$paths, OnlyPathPatterns::create('wrap.*t'), ['wrap.it']],
      [$paths, OnlyPathPatterns::create('wrap.*'), ['wrap.it', 'wrap.another']],
      // Finds path wildcard.
      [$paths, OnlyPathPatterns::create('wrap.**'), ['wrap.it', 'wrap.wrap.it', 'wrap.another', 'wrap.even.deeper1', 'wrap.even.deeper2']],

      // Also a little exclude.
      [$paths, WithoutPathPatterns::create('wrap.**'), ['root1', 'root2']],
    ];
  }

}
