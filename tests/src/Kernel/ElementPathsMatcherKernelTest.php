<?php

declare(strict_types=1);
namespace Drupal\Tests\webform4content\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher\ElementPaths;

final class ElementPathsMatcherKernelTest extends KernelTestBase {

  public static $modules = ['webform4content_test_webforms', 'webform', 'user'];

  protected function setUp() {
    parent::setUp();
    $this->installSchema('webform', 'webform');
    $this->installEntitySchema('webform');
    $this->installConfig('webform4content_test_webforms');
  }

  /**
   * @dataProvider provideElementPathsFromWebform
   */
  public function testElementPathsFromWebform(string $webformId, array $expectedPaths) {
    $instance = ElementPaths::fromWebform(Webform::load($webformId));
    $this->assertSame($expectedPaths, $instance->paths());
  }

  public function provideElementPathsFromWebform() {
    return [
      ['webform4content_test',
        [
          'title',
          'body',
          'number',
          'bool',
          'jtext',
          'jnumber',
          'jbool',
          'timestamp',
          'date',
          'date_range_start',
          'date_range_end',
          'user_data_wrapper',
          'user_data_wrapper.create_user',
          'user_data_wrapper.select_user',
          'user_data_wrapper.user_wrapper',
          'user_data_wrapper.user_wrapper.email',
          'user_data_wrapper.user_wrapper.username',
          'webform4content_test',
          'account']
        ],
    ];
  }

}
