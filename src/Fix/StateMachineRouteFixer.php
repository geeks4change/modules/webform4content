<?php

declare(strict_types=1);
namespace Drupal\webform4content\Fix;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

final class StateMachineRouteFixer extends RouteSubscriberBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    // Go after state machine alterer.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -100];
    return $events;
  }

  /**
   * Allow field name with numbers too.
   *
   * @see \Drupal\state_machine\Routing\RouteSubscriber::alterRoutes
   *
   * @todo Remove this once https://www.drupal.org/project/state_machine/issues/3369900
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (!$entity_type->hasLinkTemplate('state-transition-form')) {
        continue;
      }
      $route = $collection->get("entity.$entity_type_id.state_transition_form");
      $route
        ->setRequirement('transition_id', '[a-z0-9_]+')
        ->setRequirement('field_name', '[a-z0-9_]+')
      ;
    }
  }

}
