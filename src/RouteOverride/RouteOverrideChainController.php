<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\dcache\DCacheInterface;
use Drupal\route_override\Interfaces\RouteOverrideControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

final class RouteOverrideChainController implements RouteOverrideControllerInterface {

  protected DCacheInterface $dCache;

  /**
   * @param \Drupal\dcache\DCacheInterface $dCache
   */
  public function __construct(DCacheInterface $dCache) {
    $this->dCache = $dCache;
  }

  /**
   * @return array<\Drupal\route_override\Interfaces\RouteOverrideControllerInterface>
   */
  protected function getControllers(CacheableDependencyInterface &$cacheability = NULL): array {
    $generator = new RouteOverrideListDCacheGenerator();
    // @todo Someday, Add a DCache2 API with cache generation via contexts.
    $cacheability = (new CacheableMetadata())
      ->addCacheTags($generator->getCacheTags());
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $result = $this->dCache->lookupOrGenerate($generator);
    return $result;
  }

  public function appliesToRoute(Route $route): CacheableBool {
    $applies = FALSE;
    foreach ($this->getControllers($cacheability) as $key => $controller) {
      if ($controller->appliesToRoute($route)->value()) {
        // No problem if more than one applies, and any is enough.
        $applies = TRUE;
        break;
      }
    }
    // @todo Someday, Merge cacheabilities (it broke on first try).
    return CacheableBool::create($applies, $cacheability);
  }

  public function appliesToRouteMatch(RouteMatchInterface $routeMatch, Request $request): CacheableBool {
    $appliesToKeys = [];
    foreach ($this->getControllers($cacheability) as $key => $controller) {
      $appliesToRouteMatch = $controller->appliesToRouteMatch($routeMatch, $request);
      if ($appliesToRouteMatch->value()) {
        $appliesToKeys[] = $key;
        break;
      }
    }
    if (count($appliesToKeys) > 1) {
      throw new \LogicException(sprintf("Conflict in route overrides: %s", implode(', ', $appliesToKeys)));
    }
    return CacheableBool::create(!empty($appliesToKeys), $cacheability);
  }

  public function access(RouteMatchInterface $routeMatch, AccountInterface $account, Request $request): AccessResultInterface {
    // @fixme We can not rely on $request.
    //   @see \Drupal\Core\Access\AccessManager::checkNamedRoute
    foreach ($this->getControllers($cacheability) as $key => $controller) {
      if ($controller->appliesToRouteMatch($routeMatch, $request)->value()) {
        $access = $controller->access($routeMatch, $account, $request);
        return $access;
      }
    }
    throw new \LogicException("Call to ::access even though no controller ::appliesToRouteMatch.");
  }

  public function build(RouteMatchInterface $routeMatch, Request $request) {
    foreach ($this->getControllers($cacheability) as $key => $controller) {
      if ($controller->appliesToRouteMatch($routeMatch, $request)->value()) {
        return $controller->build($routeMatch, $request);
      }
    }
    throw new \LogicException("Call to ::build even though no controller ::appliesToRouteMatch.");
  }

}
