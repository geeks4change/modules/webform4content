<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\route_override\RouteOverride;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformEntityStorageInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Utility\FormStateWrapper;
use Drupal\webform4content\Mapping\Utility\RoutePatternMatcher;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Plugin\WebformHandler\W4cHandler;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

trait RouteOverrideControllerTrait {

  protected string $routeMatchPattern;

  protected string $entityName;

  protected string $bundleName;

  protected string $webformId;

  protected string $handlerId;

  protected CacheableDependencyInterface $cacheability;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected FormBuilderInterface $formBuilder;

  protected function getEntityName(): string {
    return $this->entityName;
  }

  protected function getEntityTypeId(): string {
    return $this->getEntityMapper()->getEntityTypeId();
  }

  protected function getW4cHandler(): Webform4ContentHandlerBase {
    return W4cHandler::forHandlerId($this->webformId, $this->handlerId)
      ?? throw new \UnexpectedValueException();
  }

  protected function getEntityMapperController(): EntityMapperController {
    return $this->getW4cHandler()->getEntityMapperController();
  }

  protected function getWebformStorage(): WebformEntityStorageInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->entityTypeManager->getStorage('webform');
  }

  /**
   * Get webform, public for fallback controller.
   *
   * @see \Drupal\webform4content\RouteOverride\RouteOverrideCreateFallbackController::build
   */
  public function getWebform(): WebformInterface {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $this->getWebformStorage()->load($this->webformId);
  }

  /**
   * Get route match pattern, public for fallback generation.
   *
   * @see \Drupal\webform4content\RouteOverride\RouteOverrideListDCacheGenerator::createControllersByRouteMatchPattern
   */
  public function getRouteMatchPattern(): string {
    return $this->routeMatchPattern;
  }

  protected function getEntityMapper(): EntityMapperInterface {
    return $this->getEntityMapperController()->getEntityMapper($this->getEntityName());
  }

  public function appliesToRoute(Route $route): CacheableBool {
    $routeName = RouteOverride::getRouteName($route);
    $match = RoutePatternMatcher::matchRoute($this->routeMatchPattern, $routeName);
    return CacheableBool::create(
      $match,
      $this->cacheability
    );
  }

  public function access(RouteMatchInterface $routeMatch, AccountInterface $account, Request $request): AccessResultInterface {
    // If ::appliesToRouteMatch, grant access, even if no webform access.
    return CacheableBool::create(TRUE, new CacheableMetadata())->toAllowedOrNeutral();
  }

  abstract protected function doesConditionApply(RouteMatchInterface $routeMatch): CacheableBool;

  public function appliesToRouteMatch(RouteMatchInterface $routeMatch, Request $request): CacheableBool {
    $routePatternMatches = CacheableBool::create(
      RoutePatternMatcher::matchRouteMatch(
        $this->routeMatchPattern,
        RouteOverride::getRouteName($routeMatch->getRouteObject()),
        $routeMatch->getRawParameters()->all()
      ),
      $this->cacheability
    );
    // Only applies if the route pattern matches
    //   (we do not require webform access, because we want the override to
    //   work even if user has no access to webform.)
    $applies = $routePatternMatches;
    // Extract next condition lazily, they depend on route matching.
    // Otherwise, throws sth like "Can not find entity in route".
    if ($applies->value()) {
      $doesConditionApply = $this->doesConditionApply($routeMatch);
      $hasApplicableBundleAndMaybeWebformId = $this->hasApplicableBundleAndMaybeWebformId($routeMatch, $request);
      $applies = CacheableBool::and(
        $applies,
        // - if the condition applies.
        $doesConditionApply,
        // - if webform applies
        $hasApplicableBundleAndMaybeWebformId,
      );
    }
    return $applies;
  }

  abstract protected function hasApplicableBundleAndMaybeWebformId(RouteMatchInterface $routeMatch, Request $request): CacheableBool;

  abstract protected function getEntityToMap(RouteMatchInterface $routeMatch): ?ContentEntityInterface;

  public function build(RouteMatchInterface $routeMatch, Request $request) {
    $formObject = $this->entityTypeManager->getFormObject('webform_submission', 'edit');

    // Build new webform submission, prepopulate trackerElement.
    $webformSubmission = WebformSubmission::create([
      'webform_id' => $this->getWebform()->id(),
    ]);
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);

    $formObject->setEntity($webformSubmission);
    // @see \Drupal\Core\Controller\FormController::getContentResult
    $formState = new FormState();
    // No additional arguments.
    // @see \Drupal\webform\WebformSubmissionForm::buildForm
    $formState->addBuildInfo('args', []);

    $entity = $this->getEntityToMap($routeMatch);
    $this->getEntityMapperController()
      ->setPreparedEntity($this->getEntityName(), $entity, $webformSubmissionWrapper);

    // We can't add ::redirectAfterSave here, so set a flag.
    // @see \Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase::alterForm
    FormStateWrapper::create($formState)
      ->addSubmitHandlerToApplyInFormAlter([$this, 'redirectAfterSave']);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $form = $this->formBuilder->buildForm($formObject, $formState);
    return $form;
  }

  /**
   * Form submit handler to redirect after save.
   *
   * This is needed because the redirect URL needs to be set after the redirect
   * url is set in ::submit handler, and after the entity URL is determined in
   * the ::save handler.
   *
   * @see \Drupal\Core\Entity\EntityForm::actions
   */
  public function redirectAfterSave(array &$form, FormStateInterface $form_state) {
    $formObject = $form_state->getFormObject();
    /** @var \Drupal\webform\WebformSubmissionInterface $webformSubmission */
    $webformSubmission = $formObject->getEntity();
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    // @todo Consider s/getPreparedEntity/getTrackedEntity/
    $entity = $this->getEntityMapperController()->getPreparedEntity($this->getEntityName(), $webformSubmissionWrapper);
    if ($entity && $entity->id() && $entity->access('view')) {
      $redirectUrl = $entity->toUrl();
      $form_state->setRedirectUrl($redirectUrl);
    }
    else {
      // If auto-accept is off, there is no entity to redirect.
      // Or maybe user can create but not view.
      // Anything is better than the webform page, which may also have no access.
      $redirectUrl = Url::fromUserInput('/');
      $form_state->setRedirectUrl($redirectUrl);
    }
  }

}
