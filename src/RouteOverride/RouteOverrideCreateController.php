<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\route_override\Interfaces\RouteOverrideControllerInterface;
use Drupal\webform4content\Mapping\Condition\EntityCondition\EntityConditionInterface;
use Drupal\webform4content\Mapping\EntityMapperController;
use Symfony\Component\HttpFoundation\Request;

final class RouteOverrideCreateController implements RouteOverrideControllerInterface {

  use RouteOverrideControllerTrait;
  use DependencySerializationTrait;

  protected EntityConditionInterface $condition;

  public function __construct(
    string $webformId,
    string $handlerId,
    EntityConditionInterface $condition,
    string $routeMatchPattern,
    string $entityName,
    string $bundleName,
    CacheableDependencyInterface $cacheability,
    EntityTypeManagerInterface $entityTypeManager,
    FormBuilderInterface $formBuilder
  ) {
    $this->webformId = $webformId;
    $this->handlerId = $handlerId;
    $this->condition = $condition;
    $this->routeMatchPattern = $routeMatchPattern;
    $this->entityName = $entityName;
    $this->bundleName = $bundleName;
    $this->cacheability = $cacheability;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  public static function create(
    string $webformId,
    string $handlerId,
    EntityConditionInterface $condition,
    string $routeMatchPattern,
    string $entityName,
    string $bundle,
    CacheableDependencyInterface $cacheability,
  ): self {
    return new self(
      $webformId,
      $handlerId,
      $condition,
      $routeMatchPattern,
      $entityName,
      $bundle,
      $cacheability,
      \Drupal::service('entity_type.manager'),
      \Drupal::service('form_builder')
    );
  }

  public function doesConditionApply(RouteMatchInterface $routeMatch): CacheableBool {
    return $this->condition->appliesToEntityBundle($this->getEntityTypeId(), $this->bundleName);
  }

  protected function getEntityToMap(RouteMatchInterface $routeMatch): ?ContentEntityInterface {
    return NULL;
  }

  protected function hasApplicableBundleAndMaybeWebformId(RouteMatchInterface $routeMatch, Request $request): CacheableBool {
    $queryParameter = $request->query->get('webform');
    $webformId = $this->getWebform()->id();
    $matches = $queryParameter === $webformId;
    return CacheableBool::create(
      $matches,
      (new CacheableMetadata())->addCacheContexts(['url.query_args:webform'])
    );
  }

}
