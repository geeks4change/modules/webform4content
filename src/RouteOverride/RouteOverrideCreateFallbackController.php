<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableRedirectResponse;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\route_override\Interfaces\RouteOverrideControllerInterface;
use Drupal\route_override\RouteOverride;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;
use Drupal\webform4content\Mapping\Utility\RoutePatternMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

final class RouteOverrideCreateFallbackController implements RouteOverrideControllerInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  protected string $routeMatchPattern;

  /**
   * CreateControllers, all with the same RouteMatchPattern.
   *
   * @see \Drupal\webform4content\RouteOverride\RouteOverrideListDCacheGenerator::generateFallbackCreateOverrides
   *
   * @var list<\Drupal\webform4content\RouteOverride\RouteOverrideCreateController>
   */
  protected array $controllers;

  public function __construct(string $routeMatchPattern, array $controllers) {
    $this->routeMatchPattern = $routeMatchPattern;
    $this->controllers = $controllers;
  }

  public function appliesToRoute(Route $route): CacheableBool {
    $routeName = RouteOverride::getRouteName($route);
    $match = RoutePatternMatcher::matchRoute($this->routeMatchPattern, $routeName);
    return CacheableBool::create(
      $match,
      $this->getCacheability()
    );
  }

  public function appliesToRouteMatch(RouteMatchInterface $routeMatch, Request $request): CacheableBool {
    $routeName = RouteOverride::getRouteName($routeMatch->getRouteObject());
    $match = RoutePatternMatcher::matchRouteMatch($this->routeMatchPattern, $routeName, $routeMatch->getRawParameters()->all());
    return CacheableBool::create(
      $match,
      $this->getCacheability()
    );
  }

  public function access(RouteMatchInterface $routeMatch, AccountInterface $account, Request $request): AccessResultInterface {
    return CacheableBool::create(
      TRUE,
      $this->getCacheability()
    )->toAllowedOrNeutral();
  }

  /**
   * @return list<\Drupal\webform4content\RouteOverride\RouteOverrideCreateController>
   */
  protected function getControllersWithAccess(RouteMatchInterface $routeMatch, AccountInterface $account, Request $request): array {
    return array_filter(
      $this->controllers,
      fn(RouteOverrideCreateController $controller) => $controller->access($routeMatch, $account, $request)->isAllowed()
    );
  }

  public function build(RouteMatchInterface $routeMatch, Request $request) {
    $controllersWithAccess = $this->getControllersWithAccess($routeMatch, \Drupal::currentUser(), $request);
    $items = [];
    foreach ($controllersWithAccess as $controller) {
      $webform = $controller->getWebform();
      $label = $webform->label();
      $url = Url::fromRouteMatch($routeMatch)
        ->setOption('query', ['webform' => $webform->id()]);
      $items[] = Link::fromTextAndUrl($label, $url);
    }
    $build['list'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Choose type to create'),
      '#list_type' => 'ul',
      '#items' => $items,
      '#attributes' => ['class' => 'w4c_fallback_list'],
      '#empty' => $this->t('No type to create available.')
    ];
    $unique = count($items) === 1;
    if ($unique) {
      $urlToRedirect = $items[0]->getUrl();
      // ::toString() will throw as of leaded cacheability.
      $urlAsString = $urlToRedirect->toString(TRUE)->getGeneratedUrl();
      return (new CacheableRedirectResponse($urlAsString))
        ->addCacheableDependency($this->getCacheability());
    }
    return $build;
  }

  protected function getCacheability(): CacheableDependencyInterface {
    return EntityTypeTool::getListCacheability('webform')
      ->addCacheContexts(['url.query_args:webform']);
  }

}
