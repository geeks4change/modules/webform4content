<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\route_override\Interfaces\RouteOverrideControllerInterface;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform4content\Mapping\Condition\EntityCondition\EntityConditionInterface;
use Drupal\webform4content\Mapping\DataFetching\FetchDataStage;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Symfony\Component\HttpFoundation\Request;

final class RouteOverrideUpdateController implements RouteOverrideControllerInterface {

  use RouteOverrideControllerTrait;
  use DependencySerializationTrait;

  protected EntityConditionInterface $condition;

  protected string $routeMatchPattern;

  protected string $entityParameterName;

  public function __construct(
    string $webformId,
    string $handlerId,
    EntityConditionInterface $condition,
    string $routeMatchPattern,
    string $entityParameterName,
    string $entityName,
    string $bundle,
    CacheableDependencyInterface $cacheability,
    EntityTypeManagerInterface $entityTypeManager,
    FormBuilderInterface $formBuilder
  ) {
    $this->webformId = $webformId;
    $this->handlerId = $handlerId;
    $this->condition = $condition;
    $this->routeMatchPattern = $routeMatchPattern;
    $this->entityParameterName = $entityParameterName;
    $this->entityName = $entityName;
    $this->bundleName = $bundle;
    $this->cacheability = $cacheability;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  public static function create(
    string $webformId,
    string $handlerId,
    EntityConditionInterface $condition,
    string $routeMatchPattern,
    string $entityParameterName,
    string $entityName,
    string $bundle,
    CacheableDependencyInterface $cacheability,
  ): self {
    return new self(
      $webformId,
      $handlerId,
      $condition,
      $routeMatchPattern,
      $entityParameterName,
      $entityName,
      $bundle,
      $cacheability,
      \Drupal::service('entity_type.manager'),
      \Drupal::service('form_builder')
    );
  }

  public function doesConditionApply(RouteMatchInterface $routeMatch): CacheableBool {
    $entity = $this->getEntityToMap($routeMatch);
    if ($entity) {
      $result = $this->condition->appliesToEntity($entity);
    }
    else {
      $result = CacheableBool::create(FALSE, $this->cacheability);
    }
    return $result;
  }

  protected function getEntityToMap(RouteMatchInterface $routeMatch): ?ContentEntityInterface {
    // May be empty if entity with id does not exist.
    $entity = $routeMatch->getParameter($this->getEntityTypeId());
    // @todo Soon, This should not be, fix RouteOverride module route match generation.
    if ($entity && !is_object($entity)) {
      $entity = \Drupal::entityTypeManager()->getStorage($this->getEntityTypeId())->load($entity);
    }
    return $entity;
  }

  protected function hasApplicableBundleAndMaybeWebformId(RouteMatchInterface $routeMatch, Request $request): CacheableBool {
    $entity = $this->getEntityToMap($routeMatch);
    if (!$entity) {
      return CacheableBool::create(FALSE, $this->cacheability);
    }
    $webformSubmission = WebformSubmission::create([
      'webform_id' => $this->getWebform()->id(),
    ]);
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);

    // Check that tracked bundle matches entity bundle.
    $cacheability = (new CacheableMetadata())->addCacheableDependency($this->getWebform());
    if ($entity->bundle() !== $this->bundleName) {
      return CacheableBool::create(FALSE, $cacheability);
    }

    $entity = $this->getEntityToMap($routeMatch);
    $this->getEntityMapperController()
      ->setPreparedEntity($this->getEntityName(), $entity, $webformSubmissionWrapper);

    // Check that webformId in entity matches ours.
    try {
      $this->getEntityMapperController()
        ->fetchData($webformSubmissionWrapper, FetchDataStage::FormController);
    }
    catch (\InvalidArgumentException $e) {
      // Wrong bundles throw as of missing fields.
    }
    $maybeWebformIdFromEntity = $webformSubmissionWrapper->getMaybeFetchedWebformId();
    return CacheableBool::create(
      // Entity is not multi-profile-enabled,
      empty($maybeWebformIdFromEntity)
      // or has the correct type.
      || $maybeWebformIdFromEntity === $this->getWebform()->id(),
      $cacheability
    );
  }

}
