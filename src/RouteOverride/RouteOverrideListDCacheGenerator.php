<?php

declare(strict_types=1);
namespace Drupal\webform4content\RouteOverride;

use Drupal\dcache\CacheItemGeneratorInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform4content\Plugin\WebformHandler\W4cHandler;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

final class RouteOverrideListDCacheGenerator implements CacheItemGeneratorInterface {

  public function getCacheId(): string {
    return 'webform4content__handler_map';
  }

  public function getCacheTags(): array {
    return \Drupal::entityTypeManager()->getDefinition('webform')->getListCacheTags();
  }

  /**
   * @return list<\Drupal\route_override\Interfaces\RouteOverrideControllerInterface>
   */
  public function getData() {
    $routeOverridesLists = [];
    foreach (Webform::loadMultiple() as $webform) {
      foreach (W4cHandler::all($webform->id()) as $handler) {
        $handlerRouteOverrides = $handler->getEntityMapperController()->getRouteOverrideControllers();
        $routeOverridesLists[] = $handlerRouteOverrides;
      }
    }
    $allHandlerRouteOverrides = array_merge(...$routeOverridesLists);
    $fallbackOverrides = $this->generateFallbackCreateOverrides($allHandlerRouteOverrides);
    // Order matters here: Fallbacks last.
    return array_merge($allHandlerRouteOverrides, $fallbackOverrides);
  }

  protected function generateFallbackCreateOverrides(array $routeOverrides): array {
    $createControllersByRouteMatchPattern = $this->createControllersByRouteMatchPattern($routeOverrides);
    return array_map(
      fn(string $routeMatchPattern, array $controllers) => new RouteOverrideCreateFallbackController($routeMatchPattern, $controllers),
      array_keys($createControllersByRouteMatchPattern), $createControllersByRouteMatchPattern
    );
  }

  protected function createControllersByRouteMatchPattern(array $routeOverrides): array {
    $byRoute = [];
    foreach ($routeOverrides as $routeOverride) {
      if ($routeOverride instanceof RouteOverrideCreateController) {
        $byRoute[$routeOverride->getRouteMatchPattern()][] = $routeOverride;
      }
    }
    return $byRoute;
  }

}
