<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\DataStoring;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;

final class FieldStorer {

  public function __construct(
    protected EntityStorer $storerForEntity,
    protected string $fieldName
  ) {}

  protected function getEntity(): ContentEntityInterface {
    return $this->storerForEntity->getMaybeEntity()
      ?? throw new \LogicException();
  }

  protected function getFieldItemList(): FieldItemListInterface {
    return $this->getEntity()->get($this->fieldName);
  }

  public function getFieldDefinition(): FieldDefinitionInterface {
    return $this->getFieldItemList()->getFieldDefinition();
  }

  public function setValue(array $itemValues): void {
    // Once needed, log entity and field access.
    $fieldItemList = $this->getFieldItemList();
    $fieldItemList->setValue($itemValues);
  }

}
