<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\DataStoring;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\Utility\DebugService;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class EntityStorer {

  protected ?ContentEntityInterface $originalMaybeEntity;

  protected bool $hasAlreadyBeenSet = FALSE;

  protected bool $isStored = FALSE;

  protected bool $isSaved = FALSE;

  public function __construct(
    // We need storer anyway for ::forEntity, so delegate other calls too.
    // We do not expose it though.
    protected Storer $storer,
    protected WebformSubmissionWrapper $webformSubmissionWrapper,
    protected EntityMapperInterface $entityMapper,
    protected ?ContentEntityInterface $maybeEntity,
  ) {
    $this->originalMaybeEntity = $this->maybeEntity;
  }

  public function getWebformSubmissionWrapper(): WebformSubmissionWrapper {
    return $this->webformSubmissionWrapper;
  }

  public function resetMaybeEntity(): void {
    $this->maybeEntity = $this->originalMaybeEntity;
    $this->hasAlreadyBeenSet = FALSE;
  }

  public function setMaybeEntity(?ContentEntityInterface $maybeEntity): void {
    // Spot mapper bugs by preventing to store twice, unless explicitly wanted.
    if ($this->hasAlreadyBeenSet) {
      throw new \UnexpectedValueException(sprintf('Try to set already set %s entity to a different value: ID %s => %s.', $this->getEntityName(), $this->maybeEntity?->id(), $maybeEntity?->id()));
    }

    $this->maybeEntity = $maybeEntity;
    $this->hasAlreadyBeenSet = TRUE;

    $this->getDebugService()
      ->debugDatum(DebugMode::mapping, "Set {$this->getEntityName()}", $maybeEntity);
  }

  protected function getEntityName(): string {
    return $this->entityMapper->getEntityName();
  }


  public function getMaybeEntity(): ?ContentEntityInterface {
    return $this->maybeEntity;
  }

  public function forEntity(string $name): EntityStorer {
    return $this->storer->forEntity($name);
  }


  public function forField(string $fieldName): FieldStorer {
    return new FieldStorer($this, $fieldName);
  }

  /**
   * Trigger the storage of a named entity, but don't do it twice.
   */
  public function ensureIsStored(): ?ContentEntityInterface {
    if (!$this->isStored) {
      $handler = $this->entityMapper->getController()->getHandler();
      $mustStoreEntity = $handler->hookMustStoreEntity($this->getEntityName(), $this->webformSubmissionWrapper, $this->maybeEntity);
      $this->getDebugService()
        ->debugHeading(DebugMode::mapping, "Must store entity {$this->getEntityName()}: " . $mustStoreEntity ? 'Yes' : 'No');
      if ($mustStoreEntity) {
        $this->entityMapper->storeData($this);
      }
      $this->isStored = TRUE;
    }
    return $this->getMaybeEntity();
  }

  /**
   * Trigger EntityMapper(!) saving of a named entity, but don't do it twice.
   */
  public function ensureIsSaved(): ?ContentEntityInterface {
    if (!$this->isSaved) {
      $this->entityMapper->ensureRecursiveSave($this);
      $this->doSave();
      $this->isSaved = TRUE;
    }
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $maybeEntity = $this->getMaybeEntity();
    return $maybeEntity;
  }

  /**
   * Callback for ItemReferenceMapper.
   */
  public function doSave(): void {
    $maybeEntity = $this->getMaybeEntity();
    $maybeEntity?->save();
  }

  protected function getDebugService(): DebugService {
    /** @var \Drupal\webform4content\Mapping\Utility\DebugService $debug */
    $debug = \Drupal::service('webform4content.debug');
    return $debug;
  }

}
