<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\DataStoring;

use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * Storer.
 *
 * On setting an entity, acts according to $willSave.
 */
final class Storer {

  /**
   * @var array<string, \Drupal\webform4content\Mapping\DataStoring\EntityStorer>
   */
  protected array $entityStorers;

  /**
   * @param array $rootMapperNames
   *   The root mapper names.
   * @param array<string, \Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface> $entityMappers
   *   EntityMappers by name.
   * @param array<string, \Drupal\Core\Entity\ContentEntityInterface> $maybeEntities
   *   Prepared Entities or NULL.
   */
  public function __construct(
    WebformSubmissionWrapper $webformSubmissionWrapper,
    protected array $rootMapperNames,
    array $entityMappers,
    array $maybeEntities,
  ) {
    if (array_diff_key($entityMappers, $maybeEntities) || array_diff_key($maybeEntities, $entityMappers)) {
      throw new \UnexpectedValueException();
    }
    if (array_diff($this->rootMapperNames, array_keys($entityMappers))) {
      throw new \UnexpectedValueException();
    }
    foreach ($entityMappers as $name => $entityMapper) {
      $this->entityStorers[$name] = new EntityStorer(
        $this,
        $webformSubmissionWrapper,
        $entityMapper,
        $maybeEntities[$name],
      );
    }
  }

  public function forEntity(string $name): EntityStorer {
    return $this->entityStorers[$name] ??
      throw new \UnexpectedValueException("Got no $name");
  }

  public function toDebugData() {
    $data = [];
    foreach ($this->entityStorers as $name => $entityStorer) {
      $maybeEntity = $entityStorer->getMaybeEntity();
      if ($maybeEntity) {
        $type = $maybeEntity->getEntityTypeId();
        $bundle = $maybeEntity->bundle();
        $data["$name($type:$bundle)"] = $maybeEntity->toArray();
      }
      else {
        $data["$name"] = NULL;
      }
    }
    return $data;
  }


}
