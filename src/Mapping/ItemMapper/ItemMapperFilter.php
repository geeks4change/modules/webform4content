<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper;

final class ItemMapperFilter {

  /**
   * @var ItemMapperInterface
   */
  protected array $itemMappers;

  /**
   * @param \Drupal\webform4content\Mapping\ItemMapper\ItemMapperInterface[] $itemMappers
   */
  public function __construct(array $itemMappers) {
    $this->itemMappers = $itemMappers;
  }

  /**
   * @return ItemMapperInterface
   */
  public function forField(string $fieldName): array {
    return array_filter(
      $this->itemMappers,
      fn(ItemMapperInterface $itemMapper) => $itemMapper->getFieldName() === $fieldName
    );
  }

}
