<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Wrapper;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\EntityAdapterInterface;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Symfony\Component\Validator\ConstraintViolationInterface;

trait EntityAdapterWrapperTrait {

  protected EntityAdapterInterface $wrapped;

  public function __construct(EntityAdapterInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function getFieldName(): string {
    return $this->wrapped->getFieldName();
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $this->wrapped->setValueList($storerForEntity, $valueList);
  }

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    return $this->wrapped->getValueList($entityReader, $propertyName);
  }

  public function ignoreViolation(ConstraintViolationInterface $violation): bool {
    return FALSE;
  }

}
