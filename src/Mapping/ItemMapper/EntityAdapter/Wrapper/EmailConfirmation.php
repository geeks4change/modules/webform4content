<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Wrapper;

use Drupal\user\Plugin\Validation\Constraint\UserMailUnique;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\EntityAdapterInterface;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Symfony\Component\Validator\ConstraintViolationInterface;

final class EmailConfirmation implements EntityAdapterInterface {

  use EntityAdapterWrapperTrait;

  /**
   * Ignore when user email is taken.
   *
   * This is not configurable, because GDPR.
   */
  protected bool $ignoreUserEmailIsTaken = TRUE;

  public function ignoreViolation(ConstraintViolationInterface $violation): bool {
    if ($this->ignoreUserEmailIsTaken) {
      $ignore = $violation->getMessageTemplate() === (new UserMailUnique())->message;
    }
    else {
      return FALSE;
    }
    return $ignore;

  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $this->wrapped->setValueList($storerForEntity, $valueList);
    foreach ($valueList->getValues() as $item) {
      // No need to confirm current user's email.
      if ($item && $item !== \Drupal::currentUser()->getEmail()) {
        $storerForEntity->getWebformSubmissionWrapper()->addEmailToConfirm($item);
      }
    }
  }

}
