<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Symfony\Component\Validator\ConstraintViolationInterface;

interface EntityAdapterInterface {

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList;

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void;

  public function getFieldName(): string;

  public function ignoreViolation(ConstraintViolationInterface $violation): bool;

}
