<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Utility\DebugService;

final class DateField implements EntityAdapterInterface {

  use FieldBaseTrait;
  use DateTrait;

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    $fieldReader = $entityReader->get($this->fieldName);
    $fieldArray = $fieldReader->getValue();
    $values = [];
    foreach ($fieldArray as $itemValue) {
      $value = $itemValue['value']
        ?? throw new \UnexpectedValueException();
      $values[] = $this->fromFieldItemValue($value, $fieldReader->getFieldDefinition());
    }

    $debugData[$this->fieldName] = [
      'fieldArray' => $fieldArray,
      'values' => $values
    ];
    DebugService::fromContainer()->debugData(DebugMode::date, $debugData, __METHOD__);
    return ValueList::fromArray($values);
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $fieldWriter = $storerForEntity->forField($this->fieldName);
    $fieldArray = [];
    foreach ($valueList->getValues() as $value) {
      if (!$value instanceof DrupalDateTime) {
        throw new \OutOfBoundsException("Need Date type in field {$this->fieldName}.");
      }
      $fieldArray[] = ['value' => $this->toFieldItemValue($value, $fieldWriter->getFieldDefinition())];
    }

    $debugData[$this->fieldName] = [
      'values' => $valueList->getValues(),
      'fieldArray' => $fieldArray
    ];
    DebugService::fromContainer()->debugData(DebugMode::date, $debugData, __METHOD__);
    $fieldWriter->setValue($fieldArray);
  }

}
