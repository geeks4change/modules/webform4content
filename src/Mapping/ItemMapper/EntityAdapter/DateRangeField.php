<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\DateRangeValue;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;

final class DateRangeField implements EntityAdapterInterface {

  use FieldBaseTrait;
  use DateTrait;

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    $fieldReader = $entityReader->get($this->fieldName);
    $fieldArray = $fieldReader->getValue();
    $values = [];
    foreach ($fieldArray as $itemValue) {
      $startItemValue = $itemValue['value']
        ?? throw new \UnexpectedValueException();
      $startValue = $this->fromFieldItemValue($startItemValue, $fieldReader->getFieldDefinition());
      $endItemValue = $itemValue['end_value'] ?? NULL;
      $endValue = $this->fromFieldItemValue($endItemValue, $fieldReader->getFieldDefinition());
      $values[] = new DateRangeValue($startValue, $endValue);
    }
    return ValueList::fromArray($values);
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $fieldWriter = $storerForEntity->forField($this->fieldName);
    $fieldArray = [];
    foreach ($valueList->getValues() as $value) {
      if (!$value instanceof DateRangeValue) {
        throw new \OutOfBoundsException("Need DateRange type in field {$this->fieldName}.");
      }
      $fieldArray[] = [
        'value' => $this->toFieldItemValue($value->getStart(), $fieldWriter->getFieldDefinition()),
        'end_value' => $this->toFieldItemValue($value->getEnd(), $fieldWriter->getFieldDefinition()),
      ];
    }
    $fieldWriter->setValue($fieldArray);
  }

}
