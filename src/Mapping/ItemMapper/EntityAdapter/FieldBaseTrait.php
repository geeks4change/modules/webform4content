<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Symfony\Component\Validator\ConstraintViolationInterface;

trait FieldBaseTrait {

  protected string $fieldName;

  public function __construct(string $fieldName) {
    $this->fieldName = $fieldName;
  }

  public function getFieldName(): string {
    return $this->fieldName;
  }

  public function ignoreViolation(ConstraintViolationInterface $violation): bool {
    return FALSE;
  }

}
