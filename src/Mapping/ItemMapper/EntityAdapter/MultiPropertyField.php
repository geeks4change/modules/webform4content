<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;

final class MultiPropertyField implements EntityAdapterInterface {

  use FieldBaseTrait;

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    $fieldWrapper = $entityReader->get($this->fieldName);
    return ValueList::fromArray($fieldWrapper->getValue());
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $fieldWrapper = $storerForEntity->forField($this->fieldName);
    $fieldWrapper->setValue($valueList->getValues());
  }

}
