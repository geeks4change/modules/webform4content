<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;

final class Field implements EntityAdapterInterface {

  use FieldBaseTrait;

  protected ?string $propertyName;

  /**
   * @todo Drop this ugly API.
   *
   * return $this
   */
  public function setPropertyName(?string $propertyName): self {
    $this->propertyName = $propertyName;
    return $this;
  }

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    $fieldWrapper = $entityReader->get($this->fieldName);
    $propertyName = $propertyName
      ?? $this->propertyName
      ?? EntityTypeTool::mainPropertyName($fieldWrapper->getFieldDefinition());
    $values = [];
    foreach ($fieldWrapper->getValue() as $fieldItemValue) {
      $values[] = $fieldItemValue[$propertyName] ?? NULL;
    }
    return ValueList::fromArray($values);
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $fieldWrapper = $storerForEntity->forField($this->fieldName);
    $fieldItemListValue = [];
    foreach ($valueList->getValues() as $value) {
      $fieldItemListValue[] = $value;
    }
    $fieldWrapper->setValue($fieldItemListValue);
  }

}
