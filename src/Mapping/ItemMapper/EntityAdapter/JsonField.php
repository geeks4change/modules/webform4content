<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;

final class JsonField implements EntityAdapterInterface {

  use FieldBaseTrait;

  public function getValueList(EntityReader $entityReader, string $propertyName = NULL): ValueList {
    $fieldReader = $entityReader->get($this->fieldName);
    $json = $fieldReader->getValue()[0]['value'] ?? '[]';
    $data = json_decode($json, TRUE) ?? [];
    // One item with all data.
    return ValueList::fromArray([$data]);
  }

  public function setValueList(EntityStorer $storerForEntity, ValueList $valueList): void {
    $fieldWriter = $storerForEntity->forField($this->fieldName);
    // Only use first value as elements data.
    [$elementsData] = $valueList->getValues();
    $json = json_encode($elementsData);
    $fieldWriter->setValue([0 => ['value' => $json]]);
  }

}
