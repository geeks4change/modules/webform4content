<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\EntityAdapter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

trait DateTrait {

  /**
   * @param string|int|bool|null $value
   */
  protected function fromFieldItemValue($value, FieldDefinitionInterface $fieldDefinition): ?DrupalDateTime {
    // @see \Drupal\datetime\DateTimeComputed::getValue
    if (!isset($value)) {
      return NULL;
    }
    else {
      $format = $this->getFieldStorageFormat($fieldDefinition);
      try {
        $dateTime = DrupalDateTime::createFromFormat($format, $value, DateTimeItemInterface::STORAGE_TIMEZONE);
        if ($this->getDateTimeType($fieldDefinition) === DateTimeItem::DATETIME_TYPE_DATE) {
          $dateTime->setDefaultDateTime();
        }
        // Change to site default timezone.
        $dateTime->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        return $dateTime;
      }
      catch (\InvalidArgumentException $e) {
        throw new \InvalidArgumentException(sprintf("Invalid data format (%s): %s", $format, var_export($value, TRUE)), 0, $e);
      }
    }
  }

  /**
   * @return string|int|bool|null
   */
  protected function toFieldItemValue(?DrupalDateTime $dateTime, FieldDefinitionInterface $fieldDefinition) {
    if (!$dateTime || $dateTime->hasErrors()) {
      return NULL;
    }
    // Change to storage timezone.
    $dateTime->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    return $dateTime->format($this->getFieldStorageFormat($fieldDefinition));
  }

  protected function getDateTimeType(FieldDefinitionInterface $fieldDefinition) {
    return $fieldDefinition->getSetting('datetime_type');
  }

  public function getFieldStorageFormat(FieldDefinitionInterface $fieldDefinition): string {
    if ($fieldDefinition->getType() === 'timestamp') {
      return 'U'; // Timestamp.
    }
    elseif (in_array($fieldDefinition->getType(), ['datetime', 'daterange'])) {
      $storageType = $this->getDateTimeType($fieldDefinition);
      /** @noinspection PhpUnnecessaryLocalVariableInspection */
      $storageFormat = $storageType === DateTimeItem::DATETIME_TYPE_DATE ?
        DateTimeItemInterface::DATE_STORAGE_FORMAT : DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
      return $storageFormat;
    }
    throw new \UnexpectedValueException();
  }

}
