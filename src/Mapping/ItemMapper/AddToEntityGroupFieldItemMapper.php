<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper;

/**
 * A ReferenceItemMapper with special behavior for EntityGroupField.
 *
 * Cares for the special nature of EntityGroupField, it sets a needs_save magic
 * property.
 *
 * Also, the reference is **always** added to the field. This is necessary for
 * user memberships,
 */
final class AddToEntityGroupFieldItemMapper implements ItemMapperInterface {

  use ReferenceItemMapperTrait;

  protected function shallDoRecursiveSave(): bool {
    return FALSE;
  }

  protected function shallAddNeedsSaveMarkerToReferenceItem(): bool {
    return TRUE;
  }

  protected function shallAddToExistingValues(): bool {
    return TRUE;
  }

}
