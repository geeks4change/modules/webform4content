<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\ItemMapper;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

interface ItemMapperInterface {

  public function getFieldName(): ?string;

  public function fetch(
    EntityReader $entityReader,
    WebformSubmissionWrapper $webformSubmissionWrapper
  ): void;

  public function store(EntityStorer $entityStorer): void;

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void;

  /**
   * Map entity violations to elements keys.
   *
   * @return array<string, \Symfony\Component\Validator\ConstraintViolationListInterface>
   *
   * @ingroup internal_api
   */
  public function mapViolationsToElementKeys(
    EntityConstraintViolationListInterface $violationList,
    WebformSubmissionWrapper $webformSubmissionWrapper
  ): array;

  /**
   * @return list<string>
   */
  public function getElementKeys(): array;

}
