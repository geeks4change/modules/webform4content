<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper;

final class ReferenceItemMapper implements ItemMapperInterface {

  use ReferenceItemMapperTrait;

  protected function shallDoRecursiveSave(): bool {
    return TRUE;
  }

  protected function shallAddNeedsSaveMarkerToReferenceItem(): bool {
    return FALSE;
  }

  protected function shallAddToExistingValues(): bool {
    return FALSE;
  }


}
