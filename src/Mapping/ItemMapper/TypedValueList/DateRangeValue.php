<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\TypedValueList;

use Drupal\Core\Datetime\DrupalDateTime;

final class DateRangeValue {

  protected ?DrupalDateTime $start;
  protected ?DrupalDateTime $end;

  public function __construct(?DrupalDateTime $start, ?DrupalDateTime $end) {
    $this->start = $start;
    $this->end = $end;
  }

  public function getStart(): ?DrupalDateTime {
    return $this->start;
  }

  public function getEnd(): ?DrupalDateTime {
    return $this->end;
  }

}
