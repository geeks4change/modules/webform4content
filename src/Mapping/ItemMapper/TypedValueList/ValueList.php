<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\TypedValueList;

/**
 * @todo Consider having different value list for fields and elements.
 */
final class ValueList {

  protected ?array $values;

  protected function __construct(?array $values) {
    $this->values = $values;
  }

  public static function fromArray(array $values): self {
    return new static($values);
  }

  public static function fromMaybeArray(?array $values): ?self {
    return $values ? self::fromArray($values) : NULL;
  }

  public function getValues(): ?array {
    return $this->values;
  }

}
