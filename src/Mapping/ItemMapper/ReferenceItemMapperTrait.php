<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\EntityAdapterInterface;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Utility\DebugService;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

trait ReferenceItemMapperTrait {

  protected string $baseEntityName;

  protected function __construct(
    protected EntityAdapterInterface $entityAdapter,
    protected EntityMapperInterface $referencedEntityMapper,
  ) {}

  public static function create(
    EntityMapper $baseEntityMapper,
    EntityAdapterInterface $entityAdapter,
    EntityMapperInterface $referencedEntityMapper,
  ) {
    $instance = new static($entityAdapter, $referencedEntityMapper);
    /** @noinspection PhpParamsInspection */
    $baseEntityMapper->addItemMapper($instance);

    $controller = $baseEntityMapper->getController();
    $controller->addEntityMapper($referencedEntityMapper, FALSE);

    $instance->baseEntityName = $baseEntityMapper->getEntityName();
    return $instance;
  }

  public function getFieldName(): ?string {
    return $this->entityAdapter->getFieldName();
  }

  public function fetch(EntityReader $entityReader, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    $valueList = $this->entityAdapter->getValueList($entityReader, 'entity');
    DebugService::fromContainer()->debugDatum(DebugMode::item_mapping, "ReferenceItemFetch {$this->baseEntityName}.{$this->getFieldName()} => {$this->referencedEntityMapper->getEntityName()}", $valueList->getValues());
    $values = $valueList->getValues();
    if ($this->shallAddToExistingValues()) {
      $maybeEntity = NULL;
    }
    else {
      $maybeEntity = $values ? $values[0] : NULL;
    }
    $this->referencedEntityMapper->fetchData($maybeEntity, $webformSubmissionWrapper);
  }

  public function store(EntityStorer $entityStorer): void {
    $referencedEntityName = $this->referencedEntityMapper->getEntityName();
    $referencedEntityStorer = $entityStorer->forEntity($referencedEntityName);
    // Store and, if required, save entity.
    $maybeEntity = $referencedEntityStorer->ensureIsStored();
    if ($this->shallAddToExistingValues()) {
      if ($maybeBaseEntity = $entityStorer->getMaybeEntity()) {
        $values = $this->entityAdapter
          ->getValueList(new EntityReader($maybeBaseEntity))
          ->getValues();
      }
      else {
        throw new \LogicException("ReferenceItemMapper for unset entity should not happen in $this->baseEntityName.{$this->getFieldName()}.");
      }
    }
    else {
      $values = [];
    }
    if ($maybeEntity) {
      $value = [
        // Ensure to set maybe unsaved entity, .
        'entity' => $maybeEntity,
      ];
      if ($this->shallAddNeedsSaveMarkerToReferenceItem()) {
        // @see \Drupal\entitygroupfield\Field\EntityGroupFieldItemList::postSave
        $value['needs_save'] = TRUE;
      }
      $values[] = $value;
    }
    $valueList = ValueList::fromArray($values);
    DebugService::fromContainer()->debugDatum(DebugMode::item_mapping, "ReferenceItemStore {$this->referencedEntityMapper->getEntityName()} => {$this->baseEntityName}.{$this->getFieldName()}", $valueList->getValues());
    $this->entityAdapter->setValueList($entityStorer, $valueList);
  }

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return $this->referencedEntityMapper->mapReferenceViolationsToElementKeys($violationList, $webformSubmissionWrapper);
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {
    if ($this->shallDoRecursiveSave()) {
      // This is why this method exists and the only nontrivial implementation.
      $referencedEntityName = $this->referencedEntityMapper->getEntityName();
      $referenceEntityStorer = $storerForEntity->forEntity($referencedEntityName);
      $referenceEntityStorer->doSave();
    }
  }

  public function getElementKeys(): array {
    return [];
  }

  /**
   * Whether the mapper shall save recursively.
   *
   * Mandatory for ordinary reference mappers, but EntityGroupField must not.
   */
  abstract protected function shallDoRecursiveSave(): bool;

  /**
   * Whether to add needs_save marker.
   *
   * EntityGroupField need this.
   */
  abstract protected function shallAddNeedsSaveMarkerToReferenceItem(): bool;

  /**
   * Whether to add to existing values.
   *
   * For multivalued reference fields: Existing values are never touched, any
   * new item is appended. Useful for multivalued user group membership.
   */
  abstract protected function shallAddToExistingValues(): bool;

}
