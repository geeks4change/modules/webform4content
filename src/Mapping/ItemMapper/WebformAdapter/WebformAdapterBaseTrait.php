<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

trait WebformAdapterBaseTrait {

  protected string $elementKey;

  public function __construct(string $elementKey) {
    $this->elementKey = $elementKey;
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $maybeValues = $webformSubmissionWrapper->getElementItems($this->elementKey);
    return $maybeValues ? ValueList::fromArray($maybeValues) : NULL;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    $webformSubmissionWrapper->setElementItems($this->elementKey, $valueList->getValues());
  }

  public function getElementKeys(): array {
    return [$this->elementKey];
  }

}
