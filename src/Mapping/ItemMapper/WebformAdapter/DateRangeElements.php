<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\DateRangeValue;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class DateRangeElements implements WebformAdapterInterface {

  protected DateElement $start;

  protected DateElement $end;

  public function __construct(DateElement $start, DateElement $end) {
    $this->start = $start;
    $this->end = $end;
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $maybeStartValues = $this->start->getMaybeValueList($webformSubmissionWrapper);
    $maybeEndValues = $this->end->getMaybeValueList($webformSubmissionWrapper);
    if (isset($maybeStartValues) && isset($maybeEndValues)) {
      // Ignore indexes that are not common.
      $commonIndexes = array_keys($maybeStartValues->getValues()) + array_keys($maybeEndValues->getValues());
      $rangeValues = [];
      foreach ($commonIndexes as $key) {
        $rangeValues[] = new DateRangeValue(
          $maybeStartValues->getValues()[$key] ?? NULL,
          $maybeEndValues->getValues()[$key] ?? NULL,
        );
      }
      $maybeValueList = ValueList::fromArray($rangeValues);
    }
    else {
      $maybeValueList = NULL;
    }
    return $maybeValueList;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    $startValues = $endValues = [];
    foreach ($valueList->getValues() as $value) {
      assert($value instanceof DateRangeValue);
      if (!$value instanceof DateRangeValue) {
        throw new \OutOfBoundsException(sprintf("Need DateRange type for elements %s.", implode('+', $this->getElementKeys())));
      }
      $startValues[] = $value->getStart();
      $endValues[] = $value->getEnd();
    }
    $this->start->setValueList($webformSubmissionWrapper, ValueList::fromArray($startValues));
    $this->end->setValueList($webformSubmissionWrapper, ValueList::fromArray($endValues));
  }

  public function getElementKeys(): array {
    return array_merge($this->start->getElementKeys(), $this->end->getElementKeys());
  }

}
