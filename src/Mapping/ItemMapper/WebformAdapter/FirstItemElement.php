<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * Some elements have it all in their first item.
 *
 * Ex.: EntityReferenceAutocomplete. Not an ID list like others:
 *  - 17
 *  - 35
 *  - 42
 * But all in the first item:
 *  -
 *    - 17
 *    - 35
 *    - 42
 */
final class FirstItemElement implements WebformAdapterInterface {

  use WebformAdapterBaseTrait;

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $maybeValues = $webformSubmissionWrapper->getElementItems($this->elementKey);
    $maybeValues = $maybeValues[0] ?? [];
    return $maybeValues ? ValueList::fromArray($maybeValues) : NULL;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    $maybeValues = $valueList->getValues();
    $maybeValues = [$maybeValues];
    $webformSubmissionWrapper->setElementItems($this->elementKey, $maybeValues);
  }

}
