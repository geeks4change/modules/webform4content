<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class MultiElement implements WebformAdapterInterface {

  /**
   * @var list<string>
   */
  protected array $elementKeys;

  public function __construct(string ...$elementKeys) {
    $this->elementKeys = $elementKeys;
  }

  public function getElementKeys(): array {
    return $this->elementKeys;
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $maybeValues = [];
    foreach ($this->elementKeys as $elementKey) {
      $maybeValues[$elementKey] = $webformSubmissionWrapper->getElementItems($elementKey);
    }
    // One item with the name-keyed array.
    // Note: May contain null items for invisible elements!
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $valueList = ValueList::fromArray([$maybeValues]);
    return $valueList;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    // There should be only one value.
    [$elementValues] = $valueList->getValues();
    foreach ($this->elementKeys as $elementKey) {
      $elementValue = $elementValues[$elementKey] ?? [];
      $webformSubmissionWrapper->setElementItems($elementKey, $elementValue);
    }
  }

}
