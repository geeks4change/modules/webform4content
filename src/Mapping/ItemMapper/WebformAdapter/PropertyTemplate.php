<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class PropertyTemplate implements WebformAdapterInterface {

  protected array $template;

  public function __construct(array $template) {
    $this->template = $template;
  }

  public function getElementKeys(): array {
    return static::doGetElementKeys($this->template);
  }

  /**
   * @param mixed $template
   */
  protected static function doGetElementKeys($template): array {
    if (is_array($template)) {
      $subResults = array_map(
        fn($item) => static::doGetElementKeys($item),
        $template
      );
      return array_unique(array_merge(...array_values($subResults)));
    }
    elseif ($template instanceof WebformAdapterInterface) {
      return $template->getElementKeys();
    }
    return [];
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $itemValues = static::interpolateTemplate($this->template, $webformSubmissionWrapper);
    // Template is an array, so is return.
    assert(is_array($itemValues));
    return ValueList::fromArray($itemValues);
  }

  /**
   * @param mixed $template
   * @return mixed
   */
  protected static function interpolateTemplate($template, WebformSubmissionWrapper $webformSubmissionWrapper) {
    if (is_array($template)) {
      return array_map(
        fn($item) => static::interpolateTemplate($item, $webformSubmissionWrapper),
        $template
      );
    }
    elseif ($template instanceof WebformAdapterInterface) {
      return $template->getMaybeValueList($webformSubmissionWrapper)->getValues();
    }
    else {
      return $template;
    }
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    // Nothing to do.
  }

}
