<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

interface WebformAdapterInterface {

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList;

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void;

  /**
   * @return list<string>
   */
  public function getElementKeys(): array;

}
