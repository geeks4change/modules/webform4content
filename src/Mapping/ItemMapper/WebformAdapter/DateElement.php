<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\Utility\DebugService;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class DateElement implements WebformAdapterInterface {

  protected string $elementKey;

  public function __construct(string $elementKey) {
    $this->elementKey = $elementKey;
  }

  public function getElementKeys(): array {
    return [$this->elementKey];
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $values = [];
    $maybeElementValues = $webformSubmissionWrapper->getElementItems($this->elementKey);
    if (isset($maybeElementValues)) {
      foreach ($maybeElementValues as $elementItemValue) {
        $maybeDateTime = $this->fromWebformElementValue($elementItemValue);
        if ($maybeDateTime) {
          // Only add non-null values.
          $values[] = $maybeDateTime;
        }
      }
      $maybeValueList = ValueList::fromArray($values);
    }
    else {
      $maybeValueList = NULL;
    }

    $debugData[$this->elementKey] = [
      'elementItems' => $maybeElementValues,
      'values' => $maybeValueList?->getValues(),
    ];
    DebugService::fromContainer()->debugData(DebugMode::date, $debugData, __METHOD__);
    return $maybeValueList;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    $elementItems = [];
    foreach ($valueList->getValues() as $value) {
      if (!$value instanceof DrupalDateTime) {
        throw new \OutOfBoundsException("Need Date type in element {$this->elementKey}.");
      }
      $elementItems[] = $this->toWebformElementValue($value);
    }

    $debugData[$this->elementKey] = [
      'values' => $valueList->getValues(),
      'elementItems' => $elementItems
    ];
    DebugService::fromContainer()->debugData(DebugMode::date, $debugData, __METHOD__);
    $webformSubmissionWrapper->setElementItems($this->elementKey, $elementItems);
  }

  protected function fromWebformElementValue(?string $value): ?DrupalDateTime {
    // 2023-06-23T23:59:59+0200
    if ($value) {
      try {
        $format = $this->getWebformDateStorageFormat();
        $dateTime = DrupalDateTime::createFromFormat($format, $value, NULL, ['validate_format' => FALSE]);
      }
      catch (\InvalidArgumentException|\UnexpectedValueException $e) {
        throw new \InvalidArgumentException(sprintf("Invalid data format (%s): %s", $format, var_export($value, TRUE)), 0, $e);
      }
      if ($dateTime) {
        return $dateTime;
      }
    }
    return NULL;
  }

  protected function toWebformElementValue(?DrupalDateTime $dateTime): ?string {
    if (!$dateTime || $dateTime->hasErrors()) {
      return NULL;
    }
    else {
      return $dateTime->format($this->getWebformDateStorageFormat());
    }
  }

  public function getWebformDateStorageFormat(): string {
    // Webform does NOT use ISO8601, which is 'Y-m-d\TH:i:sP' (P is Timezone
    // WITH colon), but uses '...O' (O is Timezone without colon).
    return 'Y-m-d\TH:i:sO'; // 'Y-m-d\TH:i:sP';
  }

}
