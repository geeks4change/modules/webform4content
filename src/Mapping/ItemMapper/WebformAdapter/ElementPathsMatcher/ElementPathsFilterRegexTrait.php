<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher;

/**
 * @internal
 */
trait ElementPathsFilterRegexTrait {

  private function makeRegex($pattern): string {
    return "/^{$this->makeInnerRegex($pattern)}$/u";
  }

  private function makeInnerRegex($pattern): string {
    // '**' matches dot separator, '*' does not.
    $regex = strtr(preg_quote($pattern, '/'), [
      '\*\*' => '.*',
      '\*' => '[^.]*',
    ]);
    // If no dot-pattern, match any level
    if (strpos($pattern, '.') === FALSE) {
      $regex = "(.*\.)?$regex";
    }
    return $regex;
  }

  public static function validatePatterns(array $patterns): void {
    $invalidPatterns = [];
    foreach ($patterns as $i => $pattern) {
      if (!preg_match('/[a-zA-Z0-9.*_]+/u', $pattern)) {
        $invalidPatterns[$i] = "'$pattern'";
      }
    }
    if ($invalidPatterns) {
      throw new \UnexpectedValueException("Invalid patterns: " . implode(', ', $invalidPatterns));
    }
  }

}
