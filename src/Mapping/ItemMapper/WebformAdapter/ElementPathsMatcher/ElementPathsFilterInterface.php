<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher;

interface ElementPathsFilterInterface {

  public function check(string $path): bool;

}
