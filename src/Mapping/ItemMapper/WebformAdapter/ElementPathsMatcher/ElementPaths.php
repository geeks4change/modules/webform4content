<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher;

use Drupal\webform\WebformInterface;

final class ElementPaths {

  /**
   * @param array<string, string> $keysByPath
   */
  private function __construct(
    protected readonly array $keysByPath,
  ) {}

  public static function fromWebform(WebformInterface $webform): self {
    $elementKeysByPath = [];
    $elements = $webform->getElementsInitializedAndFlattened();
    // @see \Drupal\webform\Entity\Webform::initElementsRecursive
    foreach ($elements as $key => $element) {
      $parents = [$key];
      while ($parentKey = $element['#webform_parent_key']) {
        array_unshift($parents, $parentKey);
        $element = $elements[$parentKey];
      }
      $path = implode('.', $parents);
      $elementKeysByPath[$path] = $key;
    }
    return new self($elementKeysByPath);
  }

  public static function fromPaths(string ...$paths): self {
    $elementKeysByPath = [];
    foreach ($paths as $path) {
      $key = array_reverse(explode('.', $path))[0];
      $elementKeysByPath[$path] = $key;
    }
    return new self($elementKeysByPath);
  }

  /**
   * @return list<string>
   */
  public function keys(): array {
    return array_values($this->keysByPath);
  }

  /**
   * @return list<string>
   */
  public function paths(): array {
    return array_keys($this->keysByPath);
  }

  public function filter(ElementPathsFilterInterface $filter): self {
    $keysByPath = array_filter(
      $this->keysByPath,
      fn(string $path) => $filter->check($path),
      ARRAY_FILTER_USE_KEY
    );
    return new self($keysByPath);
  }

}
