<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher;

use Drupal\webform\WebformInterface;

final class ElementPathsMatcher {

  private function __construct(
    protected readonly array $includePatterns,
    protected readonly array $excludePatterns
  ) {
    $invalidPatterns = [];
    foreach ([
      'include' => $this->includePatterns,
      'exclude' => $this->excludePatterns
    ] as $type => $patterns) {
      foreach ($patterns as $i => $pattern) {
        if (!preg_match('/[a-zA-Z0-9.*_]+/u', $pattern)) {
          $invalidPatterns["$type.$i"] = "'$pattern'";
        }
      }
    }
    if ($invalidPatterns) {
      throw new \UnexpectedValueException("Invalid patterns: " . implode(', ', $invalidPatterns));
    }
  }

  public static function create(): self {
    return new self([], []);
  }

  /**
   * @param string ...$patterns
   *   Patterns match element paths (separated by '.') and may contain '*' for
   *   a name part, and '**' for a path part. Paths are always absolute, while
   *   names without dot may have any prefix.
   *
   */
  public function include(string ... $patterns): self {
    return new self(
      array_merge($this->includePatterns, $patterns),
      $this->excludePatterns,
    );
  }

  /**
   * @param string ...$patterns
   *   Patterns match element paths (separated by '.') and may contain '*' for
   *   a name part, and '**' for a path part. Paths are always absolute, while
   *   names without dot may have any prefix.
   *
   */
  public function exclude(string ...$patterns): self {
    return new self(
      $this->includePatterns,
      array_merge($this->excludePatterns, $patterns),
    );
  }

  /**
   * @return list<string>
   */
  public function match(WebformInterface $webform): array {
    return $this->matchPaths($this->getElementKeysByPath($webform));
  }

  /**
   * @return list<string>
   */
  public function matchPaths(string ...$allElementKeysByPath): array {
    $elementKeysByPath = [];
    foreach ($this->includePatterns as $elementPattern) {
      $regex = $this->makeRegex($elementPattern);
      $elementKeysByPath += array_filter($allElementKeysByPath, fn(string $key) => preg_match($regex, $key));
    }
    foreach ($this->excludePatterns as $elementPattern) {
      $regex = $this->makeRegex($elementPattern);
      $elementKeysByPath = array_diff_key(
        $elementKeysByPath,
        array_filter($elementKeysByPath, fn(string $key) => preg_match($regex, $key))
      );
    }
    return $elementKeysByPath;
  }

  private function getElementKeysByPath(WebformInterface $webform): array {
    $elementKeysByPath = [];
    $elements = $this->trackerInfo->getWebform()
      ->getElementsInitializedAndFlattened();
    // @see \Drupal\webform\Entity\Webform::initElementsRecursive
    foreach ($elements as $key => $element) {
      $path = [$key];
      while ($parentKey = $element['#webform_parent_key']) {
        array_unshift($path, $parentKey);
        $element = $elements[$parentKey];
      }
      $elementKeysByPath[$key] = implode('.', $path);
    }
    return $elementKeysByPath;
  }

  private function makeRegex($pattern): string {
    return "/^{$this->makeInnerRegex($pattern)}$/u";
  }

  private function makeInnerRegex($pattern): string {
    // '**' matches dot separator, '*' does not.
    $regex = strtr(preg_quote($pattern, '/'), [
      '\*\*' => '.*',
      '\*' => '[^.]*',
    ]);
    // If no dot-pattern, match any level
    if (strpos($pattern, '.') === FALSE) {
      $regex = "(.*\.)?$regex";
    }
    return $regex;
  }

}
