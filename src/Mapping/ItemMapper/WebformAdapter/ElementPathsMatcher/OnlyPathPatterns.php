<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\ElementPathsMatcher;

final class OnlyPathPatterns implements ElementPathsFilterInterface {

  use ElementPathsFilterRegexTrait;

  private function __construct(
    protected readonly array $patterns,
  ) {}

  public static function create(string ...$patterns) {
    self::validatePatterns($patterns);
    return new self($patterns);
  }

  public function check(string $path): bool {
    foreach ($this->patterns as $pattern) {
      $regex = $this->makeRegex($pattern);
      $match = preg_match($regex, $path);
      if ($match) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
