<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Wrapper;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface;
use Drupal\webform4content\Mapping\Utility\Assert;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class ReadOnlyTextElementWithFormat implements WebformAdapterInterface {

  protected string $readOnlyTextElementKey;
  protected string $formatSourceElementKey;

  /**
   * Create a ReadOnlyTextElementWithFormat webform adapter.
   *
   * On reading (when the webform is mapped to the entity), the text from the
   * (usually computed) readonly text element is combined with the text format
   * from the format source element (which usually is used to edit / update the
   * final result.
   * Note: The adapter passes all html from the readonly text element through!
   * So usually you will want to pass all user input text through the twig
   * filter `input|e`.
   *
   * @param string $readOnlyTextElementKey
   *   The (usually computed) readonly text element.
   * @param string $formatSourceElementKey
   *   The format source formatted text element.
   */
  public function __construct(string $readOnlyTextElementKey, string $formatSourceElementKey) {
    $this->readOnlyTextElementKey = $readOnlyTextElementKey;
    $this->formatSourceElementKey = $formatSourceElementKey;
  }

  public function getElementKeys(): array {
    return [$this->readOnlyTextElementKey];
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $formatSourceElementDefinition = $webformSubmissionWrapper
      ->getWebformSubmission()->getWebform()
      ->getElementDecoded($this->formatSourceElementKey);
    Assert::hard($formatSourceElementDefinition['#type'] === 'text_format', "Element '$this->formatSourceElementKey' must be formatted text.");
    $format = reset($formatSourceElementDefinition['#allowed_formats']);

    $textAndFormatItems = [];
    $maybeValues = $webformSubmissionWrapper->getElementItems($this->readOnlyTextElementKey);
    if (isset($maybeValues)) {
      foreach ($maybeValues as $elementItem) {
        $textAndFormatItems[] = [
          'value' => $elementItem,
          'format' => $format,
        ];
      }
      $maybeValueList = ValueList::fromArray($textAndFormatItems);
    }
    else {
      $maybeValueList = NULL;
    }
    return $maybeValueList;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    // Nothing to do.
  }

}
