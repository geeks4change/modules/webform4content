<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Wrapper;

use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class CreateAndUpdateElements implements WebformAdapterInterface {

  protected string $updateSwitchElementKey;
  protected WebformAdapterInterface $create;
  protected WebformAdapterInterface $update;

  /**
   * Create a CreateAndUpdateElements webform adapter wrapper.
   *
   * Specially designed to have webforms with multiple non-formatted text fields
   * on create, and a compound formatted text field on update.
   *
   * @param string $updateSwitchElementKey
   *   The element that if non-empty determines that update mode should be used.
   * @param \Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface $create
   *   The webform adapter for create mode.
   * @param \Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface $update
   *   The webform adapter for update mode.
   *
   * @see \Drupal\webform4content_test\Plugin\WebformHandler\Webform4ContentComputedTestHandler::defineEntityMappers
   * @see \Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Wrapper\ReadOnlyTextElementWithFormat::__construct
   */
  public function __construct(string $updateSwitchElementKey, WebformAdapterInterface $create, WebformAdapterInterface $update) {
    $this->updateSwitchElementKey = $updateSwitchElementKey;
    $this->create = $create;
    $this->update = $update;
  }

  public function getElementKeys(): array {
    return array_merge(
      $this->create->getElementKeys(),
      $this->update->getElementKeys(),
    );
  }

  public function getMaybeValueList(WebformSubmissionWrapper $webformSubmissionWrapper): ?ValueList {
    $isUpdate = $this->isUpdate($webformSubmissionWrapper);
    if ($isUpdate) {
      $maybeValueList = $this->update->getMaybeValueList($webformSubmissionWrapper);
    }
    else {
      $maybeValueList = $this->create->getMaybeValueList($webformSubmissionWrapper);
    }
    return $maybeValueList;
  }

  public function setValueList(WebformSubmissionWrapper $webformSubmissionWrapper, ValueList $valueList): void {
    $isUpdate = $this->isUpdate($webformSubmissionWrapper);
    if ($isUpdate) {
      $this->update->setValueList($webformSubmissionWrapper, $valueList);
    }
    else {
      $this->create->setValueList($webformSubmissionWrapper, $valueList);
    }
  }

  protected function isUpdate(WebformSubmissionWrapper $webformSubmissionWrapper) {
    $webformSubmission = $webformSubmissionWrapper->getWebformSubmission();
    return !empty($webformSubmission->getElementData($this->updateSwitchElementKey));
  }

}
