<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\ItemMapper;

use Drupal\Core\Entity\EntityConstraintViolationList;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\EntityAdapterInterface;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface;
use Drupal\webform4content\Mapping\Utility\DebugService;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class ItemMapper implements ItemMapperInterface {

  protected EntityAdapterInterface $entityAdapter;
  protected WebformAdapterInterface $webformAdapter;

  protected function __construct(
    EntityAdapterInterface $entityAdapter,
    WebformAdapterInterface $webformAdapter
  ) {
    $this->entityAdapter = $entityAdapter;
    $this->webformAdapter = $webformAdapter;
  }

  public static function create(
    EntityMapper $entityMapper,
    EntityAdapterInterface $entityAdapter,
    WebformAdapterInterface $webformAdapter
  ) {
    $instance = new static($entityAdapter, $webformAdapter);
    $entityMapper->addItemMapper($instance);
    return $instance;
  }

  public function getFieldName(): ?string {
    return $this->entityAdapter->getFieldName();
  }

  public function fetch(EntityReader $entityReader, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    $valueList = $this->entityAdapter->getValueList($entityReader);
    DebugService::fromContainer()->debugDatum(DebugMode::item_mapping, "ItemFetch {$this->getFieldName()} => " . implode('+', $this->webformAdapter->getElementKeys()), $valueList->getValues());
    $this->webformAdapter->setValueList($webformSubmissionWrapper, $valueList);
  }

  public function store(EntityStorer $entityStorer): void {
    $maybeValueList = $this->webformAdapter->getMaybeValueList($entityStorer->getWebformSubmissionWrapper());
    DebugService::fromContainer()->debugDatum(DebugMode::item_mapping, "ItemStore " . implode('+', $this->webformAdapter->getElementKeys()) . " => {$this->getFieldName()}", $maybeValueList?->getValues());
    // Ignore dynamically hidden elements.
    if ($maybeValueList) {
      $this->entityAdapter->setValueList($entityStorer, $maybeValueList);
    }
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {}

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    $filteredViolationList = $this->filteredViolationList($violationList);
    $elementKeys = $this->webformAdapter->getElementKeys();
    // For multiple elements, choose any, until we know better.
    $elementKey = $elementKeys ? reset($elementKeys) : throw new \UnexpectedValueException();
    return [$elementKey => $filteredViolationList];
  }

  protected function filteredViolationList(EntityConstraintViolationListInterface $violationList): EntityConstraintViolationListInterface {
    $result = new EntityConstraintViolationList($violationList->getEntity());
    foreach ($violationList as $violation) {
      if (!$this->entityAdapter->ignoreViolation($violation)) {
        $result->add($violation);
      }
    }
    return $result;
  }

  public function getElementKeys(): array {
    return $this->webformAdapter->getElementKeys();
  }

}
