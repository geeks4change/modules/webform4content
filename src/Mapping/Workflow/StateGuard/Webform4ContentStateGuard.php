<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Workflow\StateGuard;

use Drupal\Core\Entity\EntityInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowTransition;
use Drupal\webform\WebformSubmissionInterface;

final class Webform4ContentStateGuard implements SystemAwareGuardInterface {

  protected ?\WeakReference $systemTokenReference = NULL;

  public function __construct() {}

  public function acquireSystemToken(): SystemToken {
    if (
      $this->systemTokenReference
      && ($systemToken = $this->systemTokenReference->get())
    ) {
      return $systemToken;
    }
    else {
      $systemToken = new SystemToken();
      $this->systemTokenReference = \WeakReference::create($systemToken);
      return $systemToken;
    }
  }

  protected function hasSystemToken(): bool {
    return $this->systemTokenReference
      && boolval($this->systemTokenReference->get());
  }

  public function allowed(WorkflowTransition $transition, WorkflowInterface $workflow, EntityInterface $entity) {
    assert($entity instanceof WebformSubmissionInterface);
    // @fixme Someday, wonder if state_machine handles cacheability correctly in formatter.
    return $this->hasSystemToken() || $this->isModerationTransition($transition);
  }

  protected function isModerationTransition(WorkflowTransition $transition): bool {
    return in_array($transition->getId(), ['accept', 'reject', 'undo_reject']);
  }

}
