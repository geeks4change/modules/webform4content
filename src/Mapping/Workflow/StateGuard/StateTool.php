<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Workflow\StateGuard;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowTransition;
use Drupal\webform4content\Mapping\Entity\FieldSingleValueWrapper;

final class StateTool {

  public static function getTransitionInPreSave(FieldItemListInterface $fieldItemList): ?WorkflowTransition  {
    $stateItem = self::getStateItem($fieldItemList);
    $workflow = $stateItem->getWorkflow();
    return $workflow->findTransition($stateItem->getOriginalId(), $stateItem->getId());
  }

  private static function getStateItem(FieldItemListInterface $fieldItemList): StateItemInterface {
    $stateItem = FieldSingleValueWrapper::create($fieldItemList)->getItem();
    assert($stateItem instanceof StateItemInterface);
    return $stateItem;
  }

}
