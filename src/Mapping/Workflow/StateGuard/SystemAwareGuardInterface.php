<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Workflow\StateGuard;

use Drupal\state_machine\Guard\GuardInterface;

interface SystemAwareGuardInterface extends GuardInterface {

  public function acquireSystemToken(): SystemToken;

}
