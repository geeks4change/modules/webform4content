<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\webform4content\Mapping\Entity\FieldValueWrapper;

trait FieldToolTrait {

  private function getFieldValues(string $fieldName): array {
    $field = $this->webformSubmission->get($fieldName);
    $valueWrapper = FieldValueWrapper::create($field);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $values = $valueWrapper->get();
    return $values;
  }

  private function resetField(FieldItemListInterface $field): void {
    $valueWrapper = FieldValueWrapper::create($field);
    $valueWrapper->set([]);
  }

  private function addToField(FieldItemListInterface $field, string $fieldPath): array {
    $valueWrapper = FieldValueWrapper::create($field);
    $values = $valueWrapper->get();
    $values[] = $fieldPath;
    $this->normalize($values);
    $valueWrapper->set($values);
    return $values;
  }

  protected function normalize(array &$array): void {
    $array = array_unique($array);
    sort($array, SORT_STRING);
    // Keys are re-assigned by sort, so no need to do that.
  }

}
