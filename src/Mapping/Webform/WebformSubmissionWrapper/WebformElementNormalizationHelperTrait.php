<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\webform\WebformInterface;
use Drupal\webform4content\Mapping\Webform\WebformTool;

/**
 * Provide the method so the formatter can use it too.
 */
trait WebformElementNormalizationHelperTrait {

  /**
   * Convert from our always-array to webform's only-multiple-as-array.
   *
   * @param array $itemValues
   *   Primitive elements: list<string>
   *   Composite elements: list<array<string, string>>
   *
   * @return mixed
   *   Primitive single elements: mixed
   *   Composite single elements: array<string, mixed>
   *   Primitive multiple elements: list<mixed>
   *   Composite multiple elements: list<array<string, mixed>>
   */
  protected static function convertElementItemsToSet(
    string $elementKey,
    array $itemValues,
    WebformInterface $webform,
  ): mixed {
    $elementIsMultiple = WebformTool::elementIsMultiple($webform, $elementKey);
    if ($elementIsMultiple) {
      $elementValue = $itemValues;
    }
    else {
      // Unwrap single elements.
      if ($itemValues) {
        $keys = array_keys($itemValues);
        if ($keys !== [0]) {
          throw new \UnexpectedValueException(sprintf('Got single element %s with multiple keys: %s', $elementKey, print_r($itemValues)));
        }
        $elementValue = $itemValues[0];
      }
      else {
        $elementValue = '';
      }
    }
    return $elementValue;
  }


}
