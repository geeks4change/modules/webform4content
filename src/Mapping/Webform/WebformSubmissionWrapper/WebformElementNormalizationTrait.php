<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\webform4content\Mapping\Webform\WebformTool;

trait WebformElementNormalizationTrait {

  use WebformElementNormalizationHelperTrait;

  public function initTmpWebformId(): void {
    $this->webformSubmission->{self::TMP_WEBFORM_ID_KEY} = NULL;
  }

  public function getMaybeFetchedWebformId(): ?string {
    return $this->webformSubmission->{self::TMP_WEBFORM_ID_KEY} ?? NULL;
  }

  /**
   * Get element data always as array regardless of multiplicity.
   *
   * Also set a '__webform__' pseudoelement.
   */
  public function getElementItems(string $elementKey): ?array {
    $data = $this->webformSubmission->getData();
    // Handle webformId magic.
    if (isset($data[self::WEBFORM_ID_KEY])) {
      throw new \UnexpectedValueException(sprintf('Webform element with name %s is not allowed.', self::WEBFORM_ID_KEY));
    }
    if ($elementKey === self::WEBFORM_ID_KEY) {
      return [$this->webformSubmission->getWebform()->id()];
    }
    // Normalize item values.
    $elementData = $data[$elementKey] ?? NULL;
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $items = $this->convertElementItemsFromGet($elementKey, $elementData);
    return $items;
  }

  /**
   * Set element data always as array regardless of multiplicity.
   *
   * We must separate our magic webformIdKey here, because webform submission
   * ignores unknown keys.
   */
  public function setElementItems(string $elementKey, array $itemValues): void {
    // Handle webformId magic.
    if ($elementKey === self::WEBFORM_ID_KEY) {
      $this->webformSubmission->{self::TMP_WEBFORM_ID_KEY} = $itemValues[0] ?? NULL;
      return;
    }
    // Normalize item values.
    $elementValue = self::convertElementItemsToSet($elementKey, $itemValues, $this->webformSubmission->getWebform());
    $this->webformSubmission->setElementData($elementKey, $elementValue);
  }

  /**
   * Convert from webform's only-multiple-as-array to our always-array.
   *
   * Also handle no-data when hidden elements.
   *
   * @param mixed $elementData
   *   Primitive single elements: mixed
   *   Composite single elements: array<string, mixed>
   *   Primitive multiple elements: list<mixed>
   *   Composite multiple elements: list<array<string, mixed>>
   *
   * @return array|null
   *   Primitive elements: list<mixed>
   *   Composite elements: list<array<string, mixed>>
   *   Null if element is invisible (due to condition).
   */
  public function convertElementItemsFromGet(
    string $elementKey,
    mixed $elementData,
  ): ?array {
    if (!isset($elementData)) {
      return NULL;
    }
    $elementIsMultiple = WebformTool::elementIsMultiple($this->webformSubmission->getWebform(), $elementKey);
    // Handle no-data when element is invisible due to condition.
    // @todo Someday in webform, propose to get a better API for this.
    if (!$this->isElementVisible($elementKey)) {
      return NULL;
    }
    // Wrap single elements.
    if ($elementIsMultiple) {
      $items = $elementData;
    }
    else {
      $items = [$elementData];
    }
    return $items;
  }

  /**
   * Handle no-data when hidden elements.
   *
   * @see \Drupal\webform\WebformSubmissionForm::submitForm
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::processForm
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::processFormRecursive
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::isElementVisible
   */
  public function isElementVisible(string $elementKey): bool {
    /** @var \Drupal\webform\WebformSubmissionConditionsValidatorInterface $conditionsValidator */
    $conditionsValidator = \Drupal::service('webform_submission.conditions_validator');

    $element = $this->webformSubmission->getWebform()->getElementDecoded($elementKey);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $isVisible = $conditionsValidator->isElementVisible($element, $this->webformSubmission);
    return $isVisible;
  }

}
