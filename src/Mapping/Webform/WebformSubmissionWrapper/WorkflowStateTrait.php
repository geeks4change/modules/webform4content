<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowTransition;
use Drupal\webform4content\Mapping\Entity\FieldSingleValueWrapper;
use Drupal\webform4content\Plugin\WebformHandler\W4cHandler;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

trait WorkflowStateTrait {

  public function getState(): StateItemInterface {
    $fieldWrapper = FieldSingleValueWrapper::create($this->webformSubmission->get(self::FIELD_STATE));
    $item = $fieldWrapper->getItem();
    if (!$item) {
      $fieldWrapper->set('draft');
    }
    assert($item instanceof StateItemInterface);
    return $item;
  }

  public function setTmpWorkflowTransitionIdInPreSave(?string $workflowTransitionId): void {
    $this->webformSubmission->{WebformSubmissionWrapper::TMP_STATE_TRANSITION_KEY} = $workflowTransitionId;
  }

  public function getTmpWorkflowTransitionIdFromPreSave(): ?string {
    return $this->webformSubmission->{WebformSubmissionWrapper::TMP_STATE_TRANSITION_KEY};
  }

  protected static function getBaseFieldDefinitionsForWorkflowState() {
    $definitions[self::FIELD_STATE] = BaseFieldDefinition::create('state')
      ->setName(self::FIELD_STATE)
      ->setLabel(t('State'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      // Must be given to display this field at all.
      // @see \Drupal\Core\Entity\EntityDisplayBase::getFieldDefinitions
      ->setDisplayOptions('view', ['weight' => 0])
      ->setSetting('workflow', 'webform4content_default')
    ;
    return $definitions;
  }

  /**
   * Add the state formatter above webform submission view.
   *
   * This seems not to work in hook_entity_bundle_field_info.
   */
  public static function staticHookEntityViewDisplayAlter(EntityViewDisplayInterface $display, array $context): void {
    // @fixme Soon, Add current state to state formatter
    if ($context['entity_type'] === 'webform_submission' && $context['view_mode'] === 'html') {
      $bundle = $context['bundle'];
      if (W4cHandler::all($bundle)) {
        $display->setComponent(WebformSubmissionWrapper::FIELD_STATE, [
          'type' => 'state_transition_form',
          'settings' => [
            // @see \Drupal\state_machine\Plugin\Field\FieldFormatter\StateTransitionFormFormatter::supportsConfirmationForm
            'require_confirmation' => TRUE,
            'use_modal' => TRUE,
          ],
          'label' => 'above',
          'weight' => -99,
          'region' => 'content',
        ]);
      }
    }
  }

}
