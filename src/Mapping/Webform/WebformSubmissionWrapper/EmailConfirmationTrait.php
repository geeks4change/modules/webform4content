<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation;

trait EmailConfirmationTrait {

  use FieldToolTrait;

  public function initEmailConfirmationCollectorField(): void {
    $this->resetField($this->webformSubmission->get(self::FIELD_EMAILS_TO_CONFIRM));
  }

  public function addEmailToConfirm(string $email): void {
    $field = $this->webformSubmission->get(self::FIELD_EMAILS_TO_CONFIRM);
    $this->addToField($field, $email);
  }

  /**
   * @return array<string>
   */
  public function getEmailsToConfirm(): array {
    return $this->getFieldValues(self::FIELD_EMAILS_TO_CONFIRM);
  }

  /**
   * Confirmations callback.
   */
  public function onSetConfirmationState(bool $confirmed): void {
    // @todo Someday move this to a wrapper.
    $systemTransitionEnabler = $this->stateGuard->acquireSystemToken();
    $transitions = $this->getState()->getTransitions();
    if ($confirmed) {
      if ($transitions['confirm_all_emails'] ?? NULL) {
        // Query unconfirmed confirmations for this webform submission.
        $confirmations = WebformSubmissionConfirmation::forWebformSubmission($this->webformSubmission);
        // @todo Someday move this to confirmations query.
        $unconfirmedConfirmations = array_filter(
          $confirmations,
          fn(WebformSubmissionConfirmation $confirmation) => $confirmation->getState() !== TRUE
        );
        $allEmailsConfirmed = empty($unconfirmedConfirmations);
        if ($allEmailsConfirmed) {
          $this->getState()->applyTransitionById('confirm_all_emails');
          $this->webformSubmission->save();
        }
      }
    }
    else {
      if ($transitions['cancel_email'] ?? NULL) {
        $this->getState()->applyTransitionById('cancel_email');
        $this->webformSubmission->save();
      }
    }
    unset($systemTransitionEnabler);
  }

  protected static function getBaseFieldDefinitionsForEmailConfirmation() {
    $definitions[self::FIELD_EMAILS_TO_CONFIRM] = BaseFieldDefinition::create('string')
      ->setName(self::FIELD_EMAILS_TO_CONFIRM)
      ->setLabel(t('Emails to confirm'))
      ->setTranslatable(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ;
    return $definitions;
  }

}
