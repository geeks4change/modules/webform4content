<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Workflow\StateGuard\SystemAwareGuardInterface;

final class WebformSubmissionWrapper {

  public const WEBFORM_ID_KEY = '__webform__';
  const TMP_WEBFORM_ID_KEY = '_webform4content__webform_id';
  use WebformElementNormalizationTrait;


  const TMP_STATE_TRANSITION_KEY = '_webform4content__state_transition';
  const FIELD_STATE = 'webform4content_state';
  use WorkflowStateTrait;


  const FIELD_EMAILS_TO_CONFIRM = 'webform4content_emails_confirm';
  use EmailConfirmationTrait;


  protected WebformSubmissionInterface $webformSubmission;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected SystemAwareGuardInterface $stateGuard;

  /**
   * @param WebformSubmissionInterface $webformSubmission
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param SystemAwareGuardInterface $stateGuard
   */
  public function __construct(WebformSubmissionInterface $webformSubmission, EntityTypeManagerInterface $entityTypeManager, SystemAwareGuardInterface $stateGuard)
  {
    $this->webformSubmission = $webformSubmission;
    $this->entityTypeManager = $entityTypeManager;
    $this->stateGuard = $stateGuard;
  }


  public static function create(WebformSubmissionInterface $webformSubmission) {
    return new self(
      $webformSubmission,
      \Drupal::entityTypeManager(),
      \Drupal::service('webform4content.state_guard'),
    );
  }

  public function getWebformSubmission(): WebformSubmissionInterface {
    return $this->webformSubmission;
  }

  public function getDebugData(): array {
    return $this->webformSubmission->getData();
  }

  public function initFetchData(): void {
    $this->initTmpWebformId();
  }

  public function initStoreData(): void {
    $this->initEmailConfirmationCollectorField();
  }

  public static function baseFieldDefinitionsByEntityTypeId(): ?array {
    return [
      'webform_submission' => self::getBaseFieldDefinitionsForEmailConfirmation()
        + self::getBaseFieldDefinitionsForWorkflowState()
    ];
  }

  /**
   * Change completed time slightly to prevent multiple submitted emails.
   *
   * Updating changed time gets reverted, so does not work.
   *
   * @see \Drupal\webform\Entity\WebformSubmission::getState
   */
  public function adjustCompletedTimestamp(): ?int {
    $timestamp = $this->webformSubmission->get('completed')->first()?->get('value')?->getValue();
    if ($timestamp) {
      $timestampUpdate = intval($timestamp) + 1;
      $this->webformSubmission->set('completed', $timestampUpdate);
      return intval($timestamp);
    }
    return NULL;
  }

  public function setBackCompletedTimestamp(?int $timestamp) {
    $this->webformSubmission->set('completed', $timestamp);
  }

}
