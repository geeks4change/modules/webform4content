<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Webform;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\WebformInterface;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;

final class WebformTool {

  public static function getAllowedEntityBundles(string $trackerElementKey, WebformInterface $webform): ?array {
    return self::extractTrackerElement($trackerElementKey, $webform)['#selection_settings']['target_bundles'] ?? NULL;
  }

  protected static function extractTrackerElement(string $trackerElementKey, WebformInterface $webform): ?array {
    $element = $webform->getElementDecoded($trackerElementKey);
    if (!(self::isEntityReferenceElement($element))) {
      return NULL;
    }
    return $element;
  }

  protected static function isEntityReferenceElement(?array $element): bool {
    return !empty($element['#target_type']);
  }

  public static function accessibleWebformElementKeysOfForm(array $elements): array {
    $webformKeys = [];
    if (self::isAccessible($elements)) {
      if ($webformKey = $elements['#webform_key'] ?? NULL) {
        $webformKeys[$webformKey] = $webformKey;
      }
      foreach (Element::children($elements) as $key) {
        if (is_array($elements[$key])) {
          $webformKeys += self::accessibleWebformElementKeysOfForm($elements[$key]);
        }
      }
    }
    return $webformKeys;
  }

  private static function isAccessible(array $elements): bool {
    $access = $elements['#access'] ?? TRUE;
    if ($access instanceof AccessResultInterface) {
      $access = $access->isAllowed();
    }
    if (!is_bool($access)) {
      $access = boolval($access);
    }
    return $access;
  }

  public static function getElementDefinition(WebformInterface $webform, string $elementKey) {
    $elementDefinition = $webform->getElementDecoded($elementKey);
    if (!$elementDefinition) {
      $elementDefinitions = $webform->getElementsDecodedAndFlattened();
      throw new \OutOfRangeException(sprintf("Unknown element: '%s'. Available: %s", $elementKey, implode('|', array_keys($elementDefinitions))));
    }
    return $elementDefinition;
  }

  public static function elementIsMultiple(WebformInterface $webform, string $elementKey): bool {
    $elementDefinition = self::getElementDefinition($webform, $elementKey);
    return boolval($elementDefinition['#multiple'] ?? FALSE);
  }

  public static function extractEntityFromElementValues($entityTypeId, ?array $maybeValues): ?ContentEntityInterface {
    $entityId = $maybeValues[0] ?? NULL;
    if (!in_array(gettype($entityId), ['integer', 'string', 'NULL'])) {
      throw new \UnexpectedValueException(sprintf('Bad entity ID: %s', var_export($entityId, TRUE)));
    }
    if (isset($entityId)) {
      $entity = EntityTypeTool::getEntityStorage($entityTypeId)->load($entityId);
      if ($entity instanceof ContentEntityInterface) {
        return $entity;
      }
    }
    return NULL;
  }

}
