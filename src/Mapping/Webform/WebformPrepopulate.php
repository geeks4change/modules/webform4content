<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Webform;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\webform\Plugin\WebformElement\OptionsBase;
use Drupal\webform\Plugin\WebformElementEntityReferenceInterface;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformElementOtherInterface;
use Drupal\webform\Utility\WebformOptionsHelper;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;

/**
 * Copy of webform's prepopulate logic.
 *
 * Webform does prepopulate on form level, but we need data in postload.
 *
 * @see \Drupal\webform\WebformSubmissionForm::prepopulateData
 */
class WebformPrepopulate {

  /**
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  protected $webformSubmission;

  /**
   * WebformPrepopulate constructor.
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   */
  private function __construct(WebformSubmissionInterface $webformSubmission) {
    $this->webformSubmission = $webformSubmission;
  }

  public static function create(WebformSubmissionInterface $webformSubmission) {
    return new static($webformSubmission);
  }

  /**
   * Prepopulate element data.
   *
   * In prepareForm, we do not have access to prepopulate data, it is added by
   * form.
   * So we copied webform prepopulate code and use it to get tracked content.
   *
   * @todo Someday suggest changing prepopulation in webform upstream.
   * @see \Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase::prepareForm
   */
  public function getPrepopulateData() {
    // Get prepopulate data.
    if ($this->getWebformSetting('form_prepopulate')) {
      $prepopulate_data = $this->getRequest()->query->all();
    }
    else {
      $prepopulate_data = [];
      $elements = $this->getWebform()->getElementsPrepopulate();
      foreach ($elements as $element_key) {
        if ($this->getRequest()->query->has($element_key)) {
          $prepopulate_data[$element_key] = $this->getRequest()->query->get($element_key);
        }
      }
    }

    // Validate prepopulate data.
    foreach ($prepopulate_data as $element_key => &$value) {
      if ($this->checkPrepopulateDataValid($element_key, $value) === FALSE) {
        unset($prepopulate_data[$element_key]);
      }
    }

    return $prepopulate_data;
  }

  /**
   * Determine if element prepopulate data is valid.
   *
   * @param string $element_key
   *   An element key.
   * @param string|array &$value
   *   A value.
   *
   * @return bool
   *   TRUE if element prepopulate data is valid.
   */
  protected function checkPrepopulateDataValid($element_key, &$value) {
    // Make sure the element exists.
    $element = $this->getWebform()->getElement($element_key);
    if (!$element) {
      return FALSE;
    }

    // Make sure the element is an input.
    $element_plugin = $this->getElementManager()->getElementInstance($element);
    if (!$element_plugin->isInput($element)) {
      return FALSE;
    }

    // Validate entity references.
    // @see \Drupal\Core\Entity\Element\EntityAutocomplete::validateEntityAutocomplete
    // @see \Drupal\webform\Plugin\WebformElement\WebformTermReferenceTrait
    if ($element_plugin instanceof WebformElementEntityReferenceInterface) {
      if (isset($element['#vocabulary'])) {
        $vocabulary_id = $element['#vocabulary'];
        $options = [
          'target_type' => 'taxonomy_term',
          'handler' => 'default:taxonomy_term',
          'target_bundles' => [$vocabulary_id => $vocabulary_id],
        ];
      }
      elseif (isset($element['#selection_settings'])) {
        $options = $element['#selection_settings'] + [
            'target_type' => $element['#target_type'],
            'handler' => $element['#selection_handler'],
          ];
      }
      else {
        return TRUE;
      }

      /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler */
      $handler = $this->getSelectionManager()->getInstance($options);
      $valid_ids = $handler->validateReferenceableEntities((array) $value);
      if (empty($valid_ids)) {
        return FALSE;
      }
      else {
        $value = $element_plugin->hasMultipleValues($element) ? $valid_ids : reset($valid_ids);
        return TRUE;
      }
    }

    // Validate options.
    $is_options_element = isset($element['#options'])
      && $element_plugin instanceof OptionsBase
      && !$element_plugin instanceof WebformElementOtherInterface;
    if ($is_options_element) {
      $option_values = WebformOptionsHelper::validateOptionValues($element['#options'], (array) $value);
      if (empty($option_values)) {
        return FALSE;
      }
      else {
        $value = $element_plugin->hasMultipleValues($element) ? $option_values : reset($option_values);
        return TRUE;
      }
    }

    return TRUE;
  }

  /**
   * Get a webform submission's webform setting.
   *
   * @param string $name
   *   Setting name.
   * @param null|mixed $default_value
   *   Default value.
   *
   * @return mixed
   *   A webform setting.
   */
  protected function getWebformSetting($name, $default_value = NULL) {
    $value = $this->getWebform()->getSetting($name)
      ?: $this->config('webform.settings')->get('settings.default_' . $name)
        ?: NULL;

    if ($value !== NULL) {
      return $this->tokenManager()->replace($value, $this->getEntity());
    }
    else {
      return $default_value;
    }
  }

  /**
   * Gets the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  protected function getRequest() {
    return \Drupal::request();
  }

  protected function config($name): ImmutableConfig {
    return \Drupal::config($name);
  }

  protected function tokenManager(): WebformTokenManagerInterface {
    return \Drupal::service('webform.token_manager');
  }

  protected function getEntity(): WebformSubmissionInterface {
    return $this->webformSubmission;
  }

  /**
   * @return \Drupal\webform\WebformInterface
   */
  protected function getWebform(): WebformInterface {
    return $this->webformSubmission->getWebform();
  }

  /**
   * @return \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected function getElementManager(): WebformElementManagerInterface {
    return \Drupal::service('plugin.manager.webform.element');
  }

  protected function getSelectionManager() {
    return \Drupal::service('plugin.manager.entity_reference_selection');
  }

}
