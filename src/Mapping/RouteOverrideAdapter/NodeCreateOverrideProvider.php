<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\RouteOverrideAdapter;

use Drupal\webform4content\RouteOverride\RouteOverrideCreateController;

final class NodeCreateOverrideProvider implements RouteOverrideAdapterInterface {

  use EntityNameAndControllerInjectionTrait;
  use BundleNameAndConditionConstructorTrait;

  protected function getRouteMatchPatternForCreate(): string {
    return "node.add?node_type=$this->bundleName";
  }

  public function getRouteOverrides(): array {
    return [
      RouteOverrideCreateController::create(
        $this->controller->getWebform()->id(),
        // @fixme handlerId or pluginId?
        $this->controller->getHandler()->getHandlerId(),
        $this->condition,
        $this->getRouteMatchPatternForCreate(),
        $this->entityName,
        $this->bundleName,
        $this->controller->getListCacheability(),
      ),
    ];
  }

}
