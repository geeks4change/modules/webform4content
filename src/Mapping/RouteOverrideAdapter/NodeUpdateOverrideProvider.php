<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\RouteOverrideAdapter;

use Drupal\webform4content\RouteOverride\RouteOverrideUpdateController;

final class NodeUpdateOverrideProvider implements RouteOverrideAdapterInterface {

  use EntityNameAndControllerInjectionTrait;
  use BundleNameAndConditionConstructorTrait;

  protected function getRouteMatchPatternForUpdate(): string {
    return 'entity.node.edit_form';
  }

  protected function getRouteParameterNameProvidingEntity(): string {
    return 'node';
  }

  public function getRouteOverrides(): array {
    return [
      RouteOverrideUpdateController::create(
        $this->controller->getWebform()->id(),
        $this->controller->getHandler()->getHandlerId(),
        $this->condition,
        $this->getRouteMatchPatternForUpdate(),
        $this->getRouteParameterNameProvidingEntity(),
        $this->entityName,
        $this->bundleName,
        $this->controller->getListCacheability(),
      )
    ];
  }

}
