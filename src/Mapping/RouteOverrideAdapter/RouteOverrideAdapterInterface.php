<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\RouteOverrideAdapter;

interface RouteOverrideAdapterInterface {

  /**
   * SetterInjection of Name.
   */
  public function setEntityName(string $entityName): void;
  /**
   * If provider handles RouteOverrides, return them.
   *
   * @return list<\Drupal\route_override\Interfaces\RouteOverrideControllerInterface>
   */
  public function getRouteOverrides(): array;

}
