<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\RouteOverrideAdapter;

use Drupal\webform4content\Mapping\EntityMapperController;

/**
 * @internal
 */
trait EntityNameAndControllerInjectionTrait {

  protected EntityMapperController $controller;

  public function setController(EntityMapperController $controller): void {
    $this->controller = $controller;
  }

  protected string $entityName;

  public function setEntityName(string $entityName): void {
    $this->entityName = $entityName;
  }

}
