<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\RouteOverrideAdapter;

use Drupal\webform4content\Mapping\Condition\EntityCondition\EntityConditionInterface;

trait BundleNameAndConditionConstructorTrait {

  protected string $bundleName;

  protected EntityConditionInterface $condition;

  public function __construct(string $bundleName, EntityConditionInterface $condition) {
    $this->bundleName = $bundleName;
    $this->condition = $condition;
  }

}
