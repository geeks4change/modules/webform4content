<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * EntityNoChangeMapper.
 *
 * Maybe useful in conjunction with SwitchMapper.
 * Element conditions may often be a simpler way to achieve this though.
 */
final class EntityNoChangeMapper implements EntityMapperInterface {

  use EntityMapperTrait;

  public function __construct(string $entityTypeId) {
    $this->entityTypeId = $entityTypeId;
    // Set entityName provisionally.
    $this->entityName = $entityTypeId;
  }

  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    // Nothing to do.
  }

  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void {
    // Nothing to do.
  }

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return [];
  }

  public function mapReferenceViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return [];
  }

  public function getElementKeyFieldPropertyMap(): array {
    return [];
  }

}
