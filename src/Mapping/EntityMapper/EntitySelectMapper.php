<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;
use Drupal\webform4content\Mapping\ItemMapper\TypedValueList\ValueList;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\WebformAdapterInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Mapping\Webform\WebformTool;

final class EntitySelectMapper implements EntityMapperInterface {

  use EntityMapperTrait;

  protected WebformAdapterInterface $selectElement;

  public function __construct(
    string $entityTypeId,
    WebformAdapterInterface $selectElement,
  ) {
    if (!EntityTypeTool::getEntityType($entityTypeId) instanceof ContentEntityTypeInterface) {
      throw new \InvalidArgumentException("$entityTypeId is not a content entity type.");
    }
    $this->entityTypeId = $entityTypeId;
    $this->selectElement = $selectElement;
    // Set entityName provisionally.
    $this->entityName = $entityTypeId;
  }

  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    $entityId = $maybeEntity?->id();
    // Use isset(), as $anonUser->id() => '0' === false :-/
    $values = isset($entityId) ? [$entityId] : [];
    $this->selectElement->setValueList($webformSubmissionWrapper, ValueList::fromArray($values));
  }

  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void {
    $webformSubmissionWrapper = $entityStorer->getWebformSubmissionWrapper();
    $maybeValueList = $this->selectElement->getMaybeValueList($webformSubmissionWrapper);
    // Ignore dynamically hidden elements.
    if ($maybeValueList) {
      $values = $maybeValueList->getValues();
      $maybeEntity = WebformTool::extractEntityFromElementValues($this->entityTypeId, $values);
      $entityStorer->setMaybeEntity($maybeEntity);
    }
    else {
      $entityStorer->setMaybeEntity(NULL);
    }
  }

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    // Ignore all violations of the entity, it is only referenced.
    return [];
  }

  public function mapReferenceViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    $elementKeys = $this->selectElement->getElementKeys();
    $elementKey = $elementKeys ? reset($elementKeys) : throw new \LogicException();
    return [$elementKey => $violationList];
  }

  public function getElementKeyFieldPropertyMap(): array {
    $entityIdKey = EntityTypeTool::getEntityIdKey($this->entityTypeId);
    $map = [];
    foreach ($this->selectElement->getElementKeys() as $elementKey) {
      $map[$elementKey] = $entityIdKey;
    }
    return $map;
  }

}
