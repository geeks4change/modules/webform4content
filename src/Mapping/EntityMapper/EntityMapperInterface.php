<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * @internal
 * @ingroup internal_api
 */
interface EntityMapperInterface {

  /**
   * @ingroup internal_api
   */
  public function setController(EntityMapperController $controller): void;

  /**
   * @ingroup internal_api
   */
  public function getController(): EntityMapperController;

  /**
   * @ingroup internal_api
   */
  public function setEntityName(string $entityName): void;

  /**
   * @ingroup internal_api
   */
  public function getEntityName(): string;

  /**
   * @ingroup internal_api
   */
  public function getEntityTypeId(): string;

  /**
   * Fetch data by iterating over item mappers OR select element.
   *
   * @ingroup internal_api
   */
  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void;

  /**
   * Fetch maybe-entity, and store-map to it.
   *
   * In the responsibility of EntityMapper, it knows its ItemMappers, and if it
   * even has some.
   * ALso, EntityMapper knows itself if it needs to create a fresh entity to
   * populate with field data, or only loads existing entities, or not.
   *
   * @param bool $inhibitSaving *
   *
   * @ingroup internal_api
   */
  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void;

  /**
   * Delegate saving to itemMappers if needed.
   *
   * Must **not** save the entity itself.
   * Reference Item mappers have to call StorerForEntity::ensureSave if needed.
   */
  public function ensureRecursiveSave(EntityStorer $storerForEntity): void;

  /**
   * Map entity violations to elements keys.
   *
   * This is for all ordinary non-reference elements and fields.
   *
   * @return array<string, \Symfony\Component\Validator\ConstraintViolationListInterface>
   *
   * @ingroup internal_api
   */
  public function mapViolationsToElementKeys(
    EntityConstraintViolationListInterface $violationList,
    WebformSubmissionWrapper $webformSubmissionWrapper
  ): array;

  /**
   * Map reference violations to elements keys.
   *
   * Use case: ReferenceItemMapper delegates violations via this method, where
   * SelectMapper picks it up.
   *
   * @return array<string, \Symfony\Component\Validator\ConstraintViolationListInterface>
   *
   * @ingroup internal_api
   */
  public function mapReferenceViolationsToElementKeys(
    EntityConstraintViolationListInterface $violationList,
    WebformSubmissionWrapper $webformSubmissionWrapper
  ): array;

  /**
   * @return array<string, string>
   *   An array keyed by elementKey, value is field name, with optional property
   *   path separated by '.'.
   */
  public function getElementKeyFieldPropertyMap(): array;

}
