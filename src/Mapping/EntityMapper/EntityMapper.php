<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\Entity\EntityReader;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapperFilter;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapperInterface;
use Drupal\webform4content\Mapping\Utility\DumpTool;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class EntityMapper implements EntityMapperInterface {

  use EntityMapperTrait;

  protected string $bundleName;

  /**
   * The configured item mapper plugins.
   *
   * @var \Drupal\webform4content\Mapping\ItemMapper\ItemMapperInterface[]
   */
  protected array $itemMappers = [];

  public function __construct(string $entityTypeId, string $bundleName) {
    if (!EntityTypeTool::getEntityType($entityTypeId) instanceof ContentEntityTypeInterface) {
      throw new \InvalidArgumentException("$entityTypeId is not a content entity type.");
    }
    $this->entityTypeId = $entityTypeId;
    $this->bundleName = $bundleName;

    // Set entityName provisionally.
    $this->entityName = $entityTypeId;
  }

  /**
   * @ingroup handler_api
   */
  public function addItemMapper(ItemMapperInterface $itemMapper): void {
    $this->itemMappers[] = $itemMapper;
  }

  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    $maybeEntity = $maybeEntity ?? $this->createEntity();
    $entityReader = new EntityReader($maybeEntity);
    foreach ($this->itemMappers as $itemMapper) {
      $itemMapper->fetch($entityReader, $webformSubmissionWrapper);
    }
  }

  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void {
    $maybeEntity = $entityStorer->getMaybeEntity();
    $entity = $maybeEntity ?? $this->createEntity();
    $entityStorer->setMaybeEntity($entity);
    $this->letItemMappersStore($entityStorer);

    // Load existing user by email.
    if (
      $entity instanceof UserInterface
      && !$entity->id()
      && ($email = $entity->get('mail')->getString())
    ) {
      /** @var \Drupal\user\UserInterface[] $existingUsers */
      $existingUsers = EntityTypeTool::getEntityStorage('user')
        ->loadByProperties(['mail' => $email]);
      if ($existingUsers) {
        $existingUser = reset($existingUsers);
        $entityStorer->resetMaybeEntity();
        $entityStorer->setMaybeEntity($existingUser);
        $this->letItemMappersStore($entityStorer);
      }
    }
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {
    // This is why this method exists and its only nontrivial implementation.
    foreach ($this->itemMappers as $itemMapper) {
      $itemMapper->ensureRecursiveSave($storerForEntity);
    }
  }

  protected function createEntity(): ContentEntityInterface {
    $bundleKey = EntityTypeTool::getEntityBundleKey($this->entityTypeId);
    $bundleName = $this->getBundleName();
    $entity = EntityTypeTool::getEntityStorage($this->entityTypeId)
      ->create([$bundleKey => $bundleName]);
    assert($entity instanceof ContentEntityInterface);
    $this->controller->hookPrepareCreatedEntity($this->getEntityName(), $entity);
    return $entity;
  }

  public function getBundleName(): string {
    return $this->bundleName;
  }

  public function mapViolationsToElementKeys(
    EntityConstraintViolationListInterface $violationList,
    WebformSubmissionWrapper $webformSubmissionWrapper
  ): array {
    $violationListsByElementKey = [];
    // Pass entity violations.
    // @see \Drupal\Core\Entity\Entity\EntityFormDisplay::validateFormValues
    // @see \Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase::displayFormValidationErrors
    $violationListsByElementKey[''] = $violationList->getEntityViolations();
    foreach ($violationList->getFieldNames() as $fieldName) {
      // @todo Someday Re-implement ::ignoreValidationForFields
      // Usually, this is one item mapper.
      $itemMappers = (new ItemMapperFilter($this->itemMappers))->forField($fieldName);
      foreach ($itemMappers as $itemMapper) {
        $fieldViolationList = $violationList->getByField($fieldName);
        assert($fieldViolationList instanceof EntityConstraintViolationListInterface);
        $newByElementKey = $itemMapper
          ->mapViolationsToElementKeys($fieldViolationList, $webformSubmissionWrapper);
        foreach ($newByElementKey as $elementKey => $newViolationList) {
          if (isset($violationListsByElementKey[$elementKey])) {
            $violationListsByElementKey[$elementKey]->addAll($newViolationList);
          }
          else {
            $violationListsByElementKey[$elementKey] = $newViolationList;
          }
        }
      }
    }
    return $violationListsByElementKey;
  }

  public function mapReferenceViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return [];
  }

  /**
   * @param \Drupal\webform4content\Mapping\DataStoring\EntityStorer $entityStorer
   *
   * @return void
   */
  protected function letItemMappersStore(EntityStorer $entityStorer): void {
    foreach ($this->itemMappers as $itemMapper) {
      $itemMapper->store($entityStorer);
    }
  }

  public function getElementKeyFieldPropertyMap(): array {
    $map = [];
    foreach ($this->itemMappers as $itemMapper) {
      foreach ($itemMapper->getElementKeys() as $elementKey) {
        $map[$elementKey] = $itemMapper->getFieldName();
      }
    }
    return $map;
  }

}
