<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper\Wrapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Utility\ViolationsTool;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class EntityMapperFallback implements EntityMapperInterface{

  protected array $entityMappers;

  public function __construct(EntityMapperInterface ...$entityMappers) {
    if (!$entityMappers) {
      throw new \LogicException('EntityMappers must not be empty.');
    }
    foreach ($entityMappers as $entityMapper) {
      if (!isset($entityTypeId)) {
        $entityTypeId = $entityMapper->getEntityTypeId();
      }
      else {
        if ($entityTypeId !== $entityMapper->getEntityTypeId()) {
          throw new \LogicException(sprintf('Entity type mismatch'));
        }
      }
    }
    $this->entityMappers = $entityMappers;
  }

  public function setController(EntityMapperController $controller): void {
    foreach ($this->entityMappers as $entityMapper) {
      $entityMapper->setController($controller);
    }
  }

  public function getController(): EntityMapperController {
    // All values are guaranteed to be equal, so any is good.
    return $this->entityMappers[0]->getController();
  }

  public function setEntityName(string $entityName): void {
    foreach ($this->entityMappers as $entityMapper) {
      $entityMapper->setEntityName($entityName);
    }
  }

  public function getEntityName(): string {
    // All values are guaranteed to be equal, so any is good.
    return $this->entityMappers[0]->getEntityName();
  }

  public function getEntityTypeId(): string {
    // All values are guaranteed to be equal, so any is good.
    return $this->entityMappers[0]->getEntityTypeId();
  }

  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    // All mappers must fetch, so switch can select on store.
    foreach ($this->entityMappers as $entityMapper) {
      $entityMapper->fetchData($maybeEntity, $webformSubmissionWrapper);
    }
  }

  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void {
    foreach ($this->entityMappers as $entityMapper) {
      $entityMapper->storeData($entityStorer, $inhibitSaving);
      $hasData = !empty($entityStorer->getMaybeEntity());
      if ($hasData) {
        break;
      }
      else {
        $entityStorer->resetMaybeEntity();
      }
    }
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {
    foreach ($this->entityMappers as $entityMapper) {
      $entityMapper->ensureRecursiveSave($storerForEntity);
    }
  }

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    $result = [];
    foreach ($this->entityMappers as $entityMapper) {
      $keyedViolationsLists = $entityMapper
        ->mapViolationsToElementKeys($violationList, $webformSubmissionWrapper);
      $result = ViolationsTool::mergeKeyedLists($result, $keyedViolationsLists);
    }
    return $result;
  }

  public function mapReferenceViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    $result = [];
    foreach ($this->entityMappers as $entityMapper) {
      $keyedViolationsLists = $entityMapper
        ->mapReferenceViolationsToElementKeys($violationList, $webformSubmissionWrapper);
      $result = ViolationsTool::mergeKeyedLists($result, $keyedViolationsLists);
    }
    return $result;
  }

  public function getElementKeyFieldPropertyMap(): array {
    $map = [];
    foreach ($this->entityMappers as $entityMapper) {
      $map = $map + $entityMapper->getElementKeyFieldPropertyMap();
    }
    return $map;
  }

}
