<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper\Wrapper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\WebformSubmissionConditionInterface;
use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class EntityMapperSwitch implements EntityMapperInterface{

  protected WebformSubmissionConditionInterface $condition;
  protected EntityMapperInterface $yesMapper;
  protected EntityMapperInterface $noMapper;

  public function __construct(WebformSubmissionConditionInterface $condition, EntityMapperInterface $yesMapper, EntityMapperInterface $noMapper) {
    $this->condition = $condition;
    $this->yesMapper = $yesMapper;
    $this->noMapper = $noMapper;

    if ($yesMapper->getEntityTypeId() !== $noMapper->getEntityTypeId()) {
      throw new \LogicException(sprintf('Entity type mismatch'));
    }
  }

  public function setController(EntityMapperController $controller): void {
    $this->yesMapper->setController($controller);
    $this->noMapper->setController($controller);
  }

  public function getController(): EntityMapperController {
    // Both values are guaranteed to be equal, so any is good.
    return $this->yesMapper->getController();
  }

  public function setEntityName(string $entityName): void {
    $this->yesMapper->setEntityName($entityName);
    $this->noMapper->setEntityName($entityName);
  }

  public function getEntityName(): string {
    // Both values are guaranteed to be equal, so any is good.
    return $this->yesMapper->getEntityName();
  }

  public function getEntityTypeId(): string {
    // Both values are guaranteed to be equal, so any is good.
    return $this->yesMapper->getEntityTypeId();
  }

  protected function getMapper(WebformSubmissionWrapper $webformSubmissionWrapper): EntityMapperInterface {
    $conditionResult = $this->condition->applies($webformSubmissionWrapper);
    return $conditionResult ? $this->yesMapper : $this->noMapper;
  }

  public function fetchData(?ContentEntityInterface $maybeEntity, WebformSubmissionWrapper $webformSubmissionWrapper): void {
    // Here both mappers must fetch, so switch can select on store.
    $this->yesMapper->fetchData($maybeEntity, $webformSubmissionWrapper);
    $this->noMapper->fetchData($maybeEntity, $webformSubmissionWrapper);
  }

  public function storeData(EntityStorer $entityStorer, bool $inhibitSaving = FALSE): void {
    $this->getMapper($entityStorer->getWebformSubmissionWrapper())
      ->storeData($entityStorer, $inhibitSaving);
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {
    $this->getMapper($storerForEntity->getWebformSubmissionWrapper())
      ->ensureRecursiveSave($storerForEntity);
  }

  public function mapViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return $this->getMapper($webformSubmissionWrapper)
      ->mapViolationsToElementKeys($violationList, $webformSubmissionWrapper);
  }

  public function mapReferenceViolationsToElementKeys(EntityConstraintViolationListInterface $violationList, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    return $this->getMapper($webformSubmissionWrapper)
      ->mapReferenceViolationsToElementKeys($violationList, $webformSubmissionWrapper);
  }

  public function getElementKeyFieldPropertyMap(): array {
    $map = [];
    foreach ([$this->yesMapper, $this->noMapper] as $entityMapper) {
      $map = $map + $entityMapper->getElementKeyFieldPropertyMap();
    }
    return $map;
  }

}
