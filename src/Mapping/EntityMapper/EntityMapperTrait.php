<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\EntityMapper;

use Drupal\webform4content\Mapping\DataStoring\EntityStorer;
use Drupal\webform4content\Mapping\EntityMapperController;

trait EntityMapperTrait {

  protected EntityMapperController $controller;

  protected string $entityName;

  protected bool $entityNameFrozen = FALSE;

  protected string $entityTypeId;

  public function setController(EntityMapperController $controller): void {
    if (isset($this->controller) && $controller !== $this->controller) {
      throw new \LogicException('Can not set controller differently.');
    }
    $this->controller = $controller;
  }

  public function getController(): EntityMapperController {
    if (!isset($this->controller)) {
      throw new \LogicException("You must ->addEntityMapper(,X) or ReferenceItemMapper::create(,,X) before adding ItemMappers to the $this->entityTypeId entity mapper.");
    }
    return $this->controller;
  }

  public function setEntityName(string $entityName): void {
    if (isset($this->controller)) {
      throw new \LogicException("Can not setEntityName after \$controller->addEntityMapper().");
    }
    if ($this->entityNameFrozen) {
      throw new \LogicException("Can not setEntityName after it was used, e.g. by setTracker.");
    }
    $this->entityName = $entityName;
  }

  public function getEntityName(): string {
    $this->entityNameFrozen = TRUE;
    return $this->entityName;
  }

  public function getEntityTypeId(): string {
    return $this->entityTypeId;
  }

  public function ensureRecursiveSave(EntityStorer $storerForEntity): void {}

}
