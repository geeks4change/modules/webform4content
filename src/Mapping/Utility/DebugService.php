<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Utility;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\DestructableInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\Entity\EntityBag;
use Psr\Log\LoggerInterface;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\Yaml\Yaml;

final class DebugService implements DestructableInterface {

  use DependencySerializationTrait;

  protected LoggerInterface $logger;

  protected ConfigFactoryInterface $configFactory;
  protected array $debugModes;

  protected array $logParts = [];

  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $configFactory) {
    $this->logger = $logger;
    $this->configFactory = $configFactory;

    register_shutdown_function([$this, 'destruct']);
  }

  public static function fromContainer(): static {
    return \Drupal::service('webform4content.debug');
  }

  private function getDebugModes(): array {
    if (!isset($this->debugModes)) {
      $modes = $this->configFactory->get('webform4content.settings')->get('debug_modes');
      $this->debugModes = is_array($modes) ? $modes : [];
    }
    return $this->debugModes;
  }

  /**
   * @deprecated Use ::debugHeading, ::debugDatum, for better crash reporting.
   */
  public function debugData(DebugMode $debugMode, array $data, string $className) {
    if (in_array($debugMode->key(), $this->getDebugModes())) {
      $this->debugHeading($debugMode, $className);
      foreach ($data as $section => $sectionData) {
        $this->debugDatum($debugMode, $section, $sectionData);
      }
    }
  }

  public function debugHeading(DebugMode $debugMode, string $title): void {
    if (in_array($debugMode->key(), $this->getDebugModes())) {
      $this->logParts[] = strval(new FormattableMarkup(
        '<h2>@title</h2>', ['@title' => $title,]));
    }
  }

  public function debugDatum(DebugMode $debugMode, string $name, mixed $rawData): void {
    $data = $this->prepareData($rawData);
    if (in_array($debugMode->key(), $this->getDebugModes())) {
      $this->logParts[] = strval(new FormattableMarkup('<details><summary>@name</summary>', ['@name' => $name,]));
      $markup = $this->getYamlMarkup($data);
      $this->logParts[] = $markup;
      $this->logParts[] = '</details>';
    }
  }

  private function dumpToLog(): void {
    if ($this->logParts) {
      $this->logger->log('debug', implode('', $this->logParts));
      $this->logParts = [];
    }
  }

  /**
   * Public destructor method.
   *
   * May be called by the service container, the self-registered shutdown
   * handler, or maybe both, so must be idempotent.
   */
  public function destruct(): void {
    $this->dumpToLog();
  }

  protected function prepareData(mixed $data): mixed {
    if ($data instanceof ContentEntityInterface) {
      return $data->toArray();
    }
    if ($data instanceof EntityBag) {
      return $data->toDebugData();
    }
    return $data;
  }

  protected function getDumperMarkup(mixed $data): string {
    // This does NOT work, as logger escapes the output to broken.
    $cloner = new VarCloner();
    $cloner->addCasters([
      ContentEntityInterface::class => fn(ContentEntityInterface $entity) => $entity->toArray(),
      EntityBag::class => fn(EntityBag $entityBag) => $entityBag->toDebugData(),
    ]);
    $dumper = new HtmlDumper();
    $markup = $dumper->dump($cloner->cloneVar($data), TRUE);
    return $markup;
  }

  protected function getYamlMarkup(mixed $data): string {
    $yaml = Yaml::dump($data, 999, 2);
    $markup = "<pre>$yaml</pre>";
    return $markup;
  }

}
