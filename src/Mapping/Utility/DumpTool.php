<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Utility;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;

final class DumpTool {

  public static function setDumpToTextFile(string $fileName = 'dump.txt') {
    // Prepare var-dumper so SUT can dump to file.
    VarDumper::setHandler(function ($v) use ($fileName) {
      $cloner = new VarCloner();
      $dumper = new CliDumper();
      $output = fopen($fileName, 'a');
      $dumper->dump($cloner->cloneVar($v), $output);
    });
  }

  public static function setDumpToHtmlFile(string $fileName = 'dump.html') {
    // Prepare var-dumper so SUT can dump to file.
    VarDumper::setHandler(function ($v) use ($fileName) {
      $cloner = new VarCloner();
      $dumper = new HtmlDumper();
      $output = fopen($fileName, 'a');
      $dumper->dump($cloner->cloneVar($v), $output);
    });
  }

}
