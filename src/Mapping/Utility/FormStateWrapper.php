<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Utility;

use Drupal\Core\Form\FormStateInterface;

final class FormStateWrapper {

  const SUBMIT_HANDLERS = ['webform4content', 'submit_handlers'];

  protected FormStateInterface $formState;

  private function __construct(FormStateInterface $formState) {
    $this->formState = $formState;
  }

  public static function create(FormStateInterface $formState): self {
    return new self($formState);
  }

  public function addSubmitHandlerToApplyInFormAlter(callable $handler): void {
    $handlers = $this->formState->get(self::SUBMIT_HANDLERS);
    $handlers[] = $handler;
    $this->formState->set(self::SUBMIT_HANDLERS, $handlers);
  }

  public function applySubmitHandlers(array &$form): void {
    $additional = $this->formState->get(self::SUBMIT_HANDLERS);
    if (is_array($additional) && $additional) {
      $current =& $form['actions']['submit']['#submit'];
      $current = array_merge($current, $additional);
      $this->formState->set(self::SUBMIT_HANDLERS, []);
    }
  }

}
