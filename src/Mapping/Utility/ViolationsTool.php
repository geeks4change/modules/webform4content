<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Utility;

final class ViolationsTool {

  /**
   * @param array<string, \Symfony\Component\Validator\ConstraintViolationListInterface> ...$violationListsList
   */
  public static function mergeKeyedLists(array ...$violationListsList): array {
    $result = [];
    foreach ($violationListsList as $violationLists) {
      foreach ($violationLists as $key => $violationList) {
        if (isset($result[$key])) {
          $violationList = $violationList->addAll($result[$key]);
        }
        $result[$key] = $violationList;
      }
    }
    return $result;
  }

}
