<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Utility;

final class Assert {

  public static function hard(bool $assert, string $message= '') {
    if (!$assert) {
      throw new \UnexpectedValueException($message);
    }
  }

}
