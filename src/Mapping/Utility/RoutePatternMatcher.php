<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Utility;

final class RoutePatternMatcher {

  private static function parse(string $routeNameWithParameters, string &$routeName = NULL, array &$parameters = NULL) {
    $parts = parse_url($routeNameWithParameters);
    $routeName = $parts['path'] ?? '';
    $parametersString = $parts['query'] ?? '';
    if ($parametersString) {
      parse_str($parametersString, $parameters);
    }
    else {
      $parameters = [];
    }
  }

  public static function matchRoute(string $routeMatchPattern, string $routeName) {
    self::parse($routeMatchPattern, $routePattern, $parametersPattern);
    return $routeName === $routePattern;
  }

  public static function matchRouteMatch(string $routeMatchPattern, string $routeName, array $rawParameters = NULL) {
    self::parse($routeMatchPattern, $routePattern, $parametersPattern);
    $routeMatches = $routeName === $routePattern;
    $rawParametersThatNeedMatch = array_intersect_assoc($rawParameters, $parametersPattern);
    $parametersMatch = $parametersPattern == $rawParametersThatNeedMatch;
    return $routeMatches && $parametersMatch;
  }

}
