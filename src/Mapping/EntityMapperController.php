<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform4content\Form\DebugMode;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\WebformSubmissionConditionInterface;
use Drupal\webform4content\Mapping\DataFetching\FetchDataStage;
use Drupal\webform4content\Mapping\DataStoring\StorerStage;
use Drupal\webform4content\Mapping\DataStoring\Storer;
use Drupal\webform4content\Mapping\Entity\EntityBag;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\RouteOverrideAdapterInterface;
use Drupal\webform4content\Mapping\Utility\DebugService;
use Drupal\webform4content\Mapping\Utility\ViolationsTool;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Mapping\Webform\WebformTool;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

final class EntityMapperController {

  protected Webform4ContentHandlerBase $handler;

  protected WebformInterface $webform;

  protected DebugService $debugService;

  /**
   * @var array<string, \Drupal\webform4content\Mapping\EntityMapper\EntityMapperInterface>
   */
  protected array $entityMappers = [];

  /**
   * Ordinary mappers are root mappers, reference mappers not.
   *
   * @var list<string>
   */
  private array $rootMapperNames;

  /**
   * @var list<\Drupal\webform4content\Mapping\RouteOverrideAdapter\RouteOverrideAdapterInterface>
   */
  protected array $routeOverrides = [];

  /**
   * @var array<string, ?string>
   */
  protected array $trackerElementKeys = [];

  protected ?WebformSubmissionConditionInterface $autoAcceptCondition = NULL;

  public function __construct(Webform4ContentHandlerBase $handler, WebformInterface $webform, DebugService $debugService) {
    $this->handler = $handler;
    $this->webform = $webform;
    $this->debugService = $debugService;
  }

  public static function create(Webform4ContentHandlerBase $handler, WebformInterface $webform): self {
    return new self(
      $handler,
      $webform,
      \Drupal::service('webform4content.debug')
    );
  }

  public function getWebform(): WebformInterface {
    return $this->webform;
  }

  public function getHandler(): Webform4ContentHandlerBase {
    return $this->handler;
  }

  /**
   * @return list<string>
   */
  public function getAllNames(): array {
    return array_keys($this->entityMappers);
  }

  /**
   * @return list<string>
   */
  public function getRootMapperNames(): array {
    return $this->rootMapperNames;
  }

  public function getEntityMapper(string $name): EntityMapperInterface {
    return $this->entityMappers[$name]
      ?? throw new \UnexpectedValueException(sprintf('Can not find EntityMapper name %s. Having: %s', $name, implode('|', $this->getAllNames())));
  }

  public function addEntityMapper(
    EntityMapperInterface $entityMapper,
    bool $isRootMapper = TRUE,
  ): void {
    $entityName = $entityMapper->getEntityName();
    if (isset($this->entityMappers[$entityName]) && $entityMapper !== $this->entityMappers[$entityName]) {
      throw new \LogicException("EntityMapper '$entityName' already set to a different one.");
    }
    $this->entityMappers[$entityName] = $entityMapper;
    if ($isRootMapper) {
      $this->rootMapperNames[] = $entityName;
    }

    $entityMapper->setController($this);
  }

  /**
   * @ingroup handler_api
   */
  public function addRouteOverride(EntityMapperInterface $entityMapper, RouteOverrideAdapterInterface $routeOverrideAdapter): void {
    try {
      $name = $entityMapper->getEntityName();
    }
    catch (\Error $e) {
      // Make the error more understandable.
      throw new \LogicException("EntityMapper->addRouteOverride(...) must be called after it got a name via Controller->addEntityMapper or ReferenceItemMapper::create()", 0, $e);
    }
    // Clone so nothing breaks when handler re-uses provider.
    $routeOverrideAdapter = clone $routeOverrideAdapter;
    $routeOverrideAdapter->setEntityName($name);
    $routeOverrideAdapter->setController($this);
    $this->routeOverrides[] = $routeOverrideAdapter;
  }

  /**
   * @return list<\Drupal\route_override\Interfaces\RouteOverrideControllerInterface>
   */
  public function getRouteOverrideControllers(): array {
    $routeOverrideControllers = [];
    foreach ($this->routeOverrides as $entityProviderAdapter) {
      $routeOverrideControllers = array_merge(
        $routeOverrideControllers,
        $entityProviderAdapter->getRouteOverrides()
      );
    }
    return $routeOverrideControllers;
  }

  /**
   * Set auto-accept condition.
   *
   * Note that sth like auto-decline has no known reasonable use case.
   *
   * @param \Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\WebformSubmissionConditionInterface $autoAcceptCondition
   *   The condition on the webform to trigger auto-accept. If the webform
   *   fulfills the condition, then it is auto-accepted immediately after all
   *   emails are confirmed. Note that this is trivially the case, when there
   *   are zero emails to confirm, or if an email to confirm is submitted by
   *   an authenticated user with that email.
   *   Currently, the only implemented webform condition is
   *   MappedEntityCondition, which checks if the entity created or updated by
   *   $entityMapper fulfills the $entityCondition (e.g. HasEntityAccess).
   *
   * @see \Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\MappedEntityCondition::__construct
   * @see \Drupal\webform4content\Mapping\Condition\EntityCondition\HasEntityAccess
   */
  public function setAutoAcceptCondition(WebformSubmissionConditionInterface $autoAcceptCondition) {
    $this->autoAcceptCondition = $autoAcceptCondition;
  }

  public function checkAutoAccept(WebformSubmissionWrapper $webformSubmissionWrapper) {
    $result = FALSE;
    if ($this->autoAcceptCondition) {
      $result = $this->autoAcceptCondition->applies($webformSubmissionWrapper);
    }
    return $result;
  }

  /**
   * Get element violation lists keyed by element key.
   *
   * @return array<string, \Symfony\Component\Validator\ConstraintViolationListInterface>
   */
  public function getElementViolations(Storer $storer, WebformSubmissionWrapper $webformSubmissionWrapper): array {
    $violationLists = [];
    foreach ($this->getAllNames() as $name) {
      $entityMapper = $this->getEntityMapper($name);
      $entity = $storer->forEntity($name)->getMaybeEntity();
      if ($entity) {
        $entityViolationList = $entity->validate();
        $entityViolationLists = $entityMapper
          ->mapViolationsToElementKeys($entityViolationList, $webformSubmissionWrapper);
        $violationLists = ViolationsTool::mergeKeyedLists(
          $violationLists,
          $entityViolationLists
        );
      }
    }
    return $violationLists;
  }

  /**
   * Add tracker.
   *
   * Using EntityMapper parameter, as getting name from reference mapper is not
   * intuitive.
   *
   * @ingroup handler_api
   *
   * Tracker currently has 2 usages:
   * - **TrackedEntity**: After accept, store finally mapped entity IDs in a
   *   designated element for posterity. This was the original usage, and as
   *   such it optional.
   * - **PreparedEntity**: Store IDs of entities to updata,  prepared from the
   *   form for later, for...
   *   a) the w4c fetch logic in the form.
   *   b) the w4c fetch logic in later stages of the workflow.
   *      Note that after the initial creation of the WebformSubmission, in
   *      general there are also the confirmation and accept stages.
   *   c) the logic in the webform itself, which may be different in entity
   *      create and entity update (which is different from WebformSubmission
   *      create and update).
   *   This is
   *   - mandatory for root entities and
   *   - NOT supported for referenced entities
   *
   * Separate APIs for this are prepared here, currently they share the same
   * storage.
   *
   * If we need a pre-populated prepared entity, it can not be stored in tracker.
   * If we ever need that, add a WebformSubmission field. Property magic is not
   * enough, because the prepopulated entity must be available not only on first
   * form usage, but also on confirm and accept.
   * If we only need some adjustments to entity, consider adding a concept of
   * adjusters to the controller.
   * But for now, storing only ID in tracker is fine.
   */
  public function addTracker(EntityMapperInterface $entityMapper, string $trackerElementKey) {
    try {
      $name = $entityMapper->getEntityName();
    }
    catch (\Error $e) {
      // Make the error more understandable.
      throw new \LogicException("EntityMapper->addTracker('$trackerElementKey') must be called after it got a name via Controller->addEntityMapper or ReferenceItemMapper::create()", 0, $e);
    }
    $this->trackerElementKeys[$name] = $trackerElementKey;
  }

  protected function setTrackedEntity(
    string $name,
    ?ContentEntityInterface $entity,
    WebformSubmissionWrapper $webformSubmissionWrapper,
  ): void {
    // Store IDs of saved entities to tracker element.
    $entityId = $entity?->id();
    $trackerElementKey = $this->trackerElementKeys[$name] ?? NULL;
    // Use isset($entityId) as '0' === false.
    if ($trackerElementKey) {
      // Don't care if $entityId is int or string, webform stores string anyway.
      $itemValues = isset($entityId) ? [strval($entityId)] : [];
      $webformSubmissionWrapper->setElementItems($trackerElementKey, $itemValues);
    }
  }

  public function setPreparedEntity(
    string $name,
    ?ContentEntityInterface $entity,
    WebformSubmissionWrapper $webformSubmissionWrapper,
  ): void {
    if (!in_array($name, $this->rootMapperNames)) {
      throw new \UnexpectedValueException("Only root mappers can be prepared. Got: $name");
    }
    $trackerElementKey = $this->trackerElementKeys[$name] ?? NULL;
    if (!$trackerElementKey) {
      throw new \LogicException("Mandatory tracker for root entity $name missing.");
    }
    // Currently, store IDs of loaded (not created) entities to tracker element.
    $this->setTrackedEntity($name, $entity, $webformSubmissionWrapper);
  }

  /**
   * Get prepared root entity.
   *
   * If we ever need access to prepared referenced entities, do a
   *   FetchData, and read EntityBag.
   */
  public function getPreparedEntity(string $name, WebformSubmissionWrapper $webformSubmissionWrapper): ?ContentEntityInterface {
    $trackerElementKey = $this->trackerElementKeys[$name] ?? NULL;
    $entityMapper = $this->entityMappers[$name] ?? NULL;
    if ($trackerElementKey && $entityMapper) {
      $entityTypeId = $entityMapper->getEntityTypeId();
      $maybeValues = $webformSubmissionWrapper->getElementItems($trackerElementKey);
      return WebformTool::extractEntityFromElementValues($entityTypeId, $maybeValues);
    }
    return NULL;
  }

  public function getListCacheability(): CacheableDependencyInterface {
    return (new CacheableMetadata())
      ->addCacheTags(\Drupal::entityTypeManager()
        ->getDefinition('webform')
        ->getListCacheTags())
      ->addCacheContexts(\Drupal::entityTypeManager()
        ->getDefinition('webform')
        ->getListCacheContexts())
      ;
  }

  public function fetchData(WebformSubmissionWrapper $webformSubmissionWrapper, FetchDataStage $fetchDataStage): EntityBag {
    $webformSubmissionWrapper->initFetchData();
    $debugData = [];

    // Fetch EntityBag.
    $entityBag = new EntityBag();
    foreach ($this->getRootMapperNames() as $name) {
      $maybeEntity = $this->getPreparedEntity($name, $webformSubmissionWrapper);
      $entityBag->set($name, $maybeEntity);
    }
    $debugData['Entities'] = $entityBag->toDebugData();

    // Map to WebformSubmission.
    // Iterate over root entities, others are mapped via recursion in
    // ReferenceItemMapper.
    $debugData['Webform before'] = $webformSubmissionWrapper->getDebugData();
    foreach ($this->getRootMapperNames() as $name) {
      $entityMapper = $this->getEntityMapper($name);
      $entity = $entityBag->get($name);
      $entityMapper->fetchData($entity, $webformSubmissionWrapper);
    }
    $debugData['Webform after'] = $webformSubmissionWrapper->getDebugData();
    $this->debugService->debugData(DebugMode::mapping, $debugData, $fetchDataStage->label());
    return $entityBag;
  }

  public function storeData(
    WebformSubmissionWrapper $webformSubmissionWrapper,
    StorerStage $storeDataStage,
    bool $willSave = FALSE,
  ): Storer {
    $this->debugService->debugHeading(DebugMode::mapping, $storeDataStage->label());
    $this->debugService->debugDatum(DebugMode::mapping,
      $storeDataStage->label() . ': Initial Webform', $webformSubmissionWrapper->getDebugData());

    // @todo Drop once cleaned up.
    $this->handler->hookWebformSubmissionPrepare($webformSubmissionWrapper);
    $this->handler->hookWebformSubmissionPrepareStoring($webformSubmissionWrapper);

    $this->debugService->debugDatum(DebugMode::mapping,
      $storeDataStage->label() . ': Prepared Webform', $webformSubmissionWrapper->getDebugData());

    // Get prepare entities from tracker elements.
    $preparedEntities = [];
    foreach ($this->getAllNames() as $name) {
      $preparedEntities[$name] = $this->getPreparedEntity($name, $webformSubmissionWrapper);
    }

    // Init Storer.
    $webformSubmissionWrapper->initStoreData();
    $storer = new Storer(
      $webformSubmissionWrapper,
      $this->rootMapperNames,
      $this->entityMappers,
      $preparedEntities,
    );

    $this->debugService->debugDatum(
      DebugMode::mapping,
      $storeDataStage->label() . ": Entities after init storer",
      $storer->toDebugData()
    );

    $this->handler->hookStorerPrepare($storer);

    $this->debugService->debugDatum(
      DebugMode::mapping,
      $storeDataStage->label() . ": Entities after hookStorerPrepare",
      $storer->toDebugData()
    );

    // Store.
    foreach ($this->getRootMapperNames() as $name) {
      // If needed, consider moving entity preparation out to here.
      $maybeEntity = $storer->forEntity($name)->ensureIsStored();
      $entityId = $maybeEntity?->id();

      $this->debugService->debugDatum(
        DebugMode::mapping,
        $storeDataStage->label() . ": Entities after store $name" . ($entityId ? "($entityId)" : ''),
        $storer->toDebugData()
      );
    }
    if ($willSave) {
      $this->handler->hookStorePreSave($storer);

      $this->debugService->debugDatum(
        DebugMode::mapping,
        $storeDataStage->label() . ": Entities after hookStorePreSave",
        $storer->toDebugData()
      );

      // Save.
      foreach ($this->getRootMapperNames() as $name) {
        $storer->forEntity($name)->ensureIsSaved();
      }
      // Set trackers.
      foreach ($this->getAllNames() as $name) {
        $maybeEntity = $storer->forEntity($name)->getMaybeEntity();
        $this->setTrackedEntity(
          $name,
          $maybeEntity,
          $webformSubmissionWrapper,
        );
      }

      $this->debugService->debugDatum(
        DebugMode::mapping,
        $storeDataStage->label() . ": Entities after save",
        $storer->toDebugData()
      );

      $this->handler->hookStorerPostSave($storer);

      $this->debugService->debugDatum(
        DebugMode::mapping,
        $storeDataStage->label() . ": Entities after hookStorerPostSave",
        $storer->toDebugData()
      );
    }
    return $storer;
  }

  public function hookPrepareCreatedEntity(string $entityName, ContentEntityInterface $entity) {
    $this->handler->hookPrepareCreatedEntity($entityName, $entity);
  }


}
