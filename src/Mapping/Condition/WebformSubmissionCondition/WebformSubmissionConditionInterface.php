<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition;

use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

interface WebformSubmissionConditionInterface {

  public function applies(WebformSubmissionWrapper $webformSubmissionWrapper): bool;

}
