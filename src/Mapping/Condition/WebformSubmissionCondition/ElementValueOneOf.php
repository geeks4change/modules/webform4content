<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition;

use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Element;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class ElementValueOneOf implements WebformSubmissionConditionInterface {

  protected Element $element;
  protected array $values;

  public function __construct(Element $element, array $values) {
    $this->element = $element;
    $this->values = $values;
  }

  public function applies(WebformSubmissionWrapper $webformSubmissionWrapper): bool {
    $valueList = $this->element->getMaybeValueList($webformSubmissionWrapper);
    return !empty(array_intersect($valueList?->getValues() ?? [], $this->values));
  }

}
