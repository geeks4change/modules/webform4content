<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition;

use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Element;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class ElementValueNotEmpty implements WebformSubmissionConditionInterface {

  protected Element $element;

  public function __construct(Element $element) {
    $this->element = $element;
  }

  public function applies(WebformSubmissionWrapper $webformSubmissionWrapper): bool {
    $maybeValueList = $this->element->getMaybeValueList($webformSubmissionWrapper);
    return !empty($maybeValueList?->getValues());
  }

}
