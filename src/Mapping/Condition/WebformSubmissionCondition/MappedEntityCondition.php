<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\webform4content\Mapping\Condition\EntityCondition\EntityConditionInterface;
use Drupal\webform4content\Mapping\DataStoring\StorerStage;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

final class MappedEntityCondition implements WebformSubmissionConditionInterface {

  protected EntityMapperController $controller;

  protected string $entityName;

  protected EntityConditionInterface $entityCondition;

  /**
   * Create a MappedEntityCondition
   *
   *
   * The condition is fulfilled on a $webformSubmission, if the entity created
   * or updated by $entityMapper fulfills the $entityCondition.
   *
   * This is currently limited to root entities.
   *
   * @todo Consider leveraging $controller::storeData to get full control.
   *   We can then make ::getPreparedEntity private.
   *
   * @param \Drupal\webform4content\Mapping\EntityMapper\EntityMapper $entityMapper
   * @param \Drupal\webform4content\Mapping\Condition\EntityCondition\EntityConditionInterface $entityCondition
   *
   * @see \Drupal\webform4content\Mapping\EntityMapperController::getPreparedEntity
   *
   */
  public function __construct(EntityMapper $entityMapper, EntityConditionInterface $entityCondition) {
    $this->controller = $entityMapper->getController();
    $this->entityName = $entityMapper->getEntityName();
    $this->entityCondition = $entityCondition;
  }

  public function applies(WebformSubmissionWrapper $webformSubmissionWrapper): bool {
    $storer = $this->controller->storeData($webformSubmissionWrapper, StorerStage::EntityCondition);
    $maybeEntity = $storer->forEntity($this->entityName)->getMaybeEntity();
    if (!$maybeEntity) {
      $cacheableBool = CacheableBool::create(FALSE, new CacheableMetadata());
    }
    elseif ($maybeEntity->id()) {
      $cacheableBool = $this->entityCondition->appliesToEntity($maybeEntity);
    }
    else {
      $cacheableBool = $this->entityCondition->appliesToEntityBundle($maybeEntity->getEntityTypeId(), $maybeEntity->bundle());
    }
    return $cacheableBool->value();
  }

}
