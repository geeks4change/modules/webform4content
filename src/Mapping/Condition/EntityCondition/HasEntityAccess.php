<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\EntityCondition;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\webform4content\Mapping\Entity\EntityTypeTool;

final class HasEntityAccess implements EntityConditionInterface {

  public function appliesToEntity(ContentEntityInterface $entity): CacheableBool {
    $accessResult = $entity->access('update', NULL, TRUE);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $cacheableBool = CacheableBool::ifAccessResultAllowed($accessResult);
    return $cacheableBool;
  }

  public function appliesToEntityBundle(string $entityTypeId, string $bundle): CacheableBool {
    // @see \Drupal\webform4content_test\Plugin\WebformHandler\Webform4ContentTestHandler::doAutoAccept
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $cacheableBool = EntityTypeTool::getEntityCreateBoolAccess($entityTypeId, $bundle);
    return $cacheableBool;
  }

}
