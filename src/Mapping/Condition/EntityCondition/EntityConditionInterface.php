<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\EntityCondition;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Entity\ContentEntityInterface;

interface EntityConditionInterface {

  public function appliesToEntity(ContentEntityInterface $entity): CacheableBool;

  public function appliesToEntityBundle(string $entityTypeId, string $bundle): CacheableBool;

}
