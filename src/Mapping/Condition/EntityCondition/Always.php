<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Condition\EntityCondition;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;

final class Always implements EntityConditionInterface {

  public function appliesToEntity(ContentEntityInterface $entity): CacheableBool {
    return CacheableBool::create(TRUE, new CacheableMetadata());
  }

  public function appliesToEntityBundle(string $entityTypeId, string $bundle): CacheableBool {
    return CacheableBool::create(TRUE, new CacheableMetadata());
  }

}
