<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Entity;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;

final class EntityTypeTool {

  public static function getEntityType(string $entityTypeId): EntityTypeInterface {
    return \Drupal::entityTypeManager()->getDefinition($entityTypeId);
  }

  public static function getEntityStorage(string $entityTypeId): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage($entityTypeId);
  }

  public static function getEntityBundleKey(string $entityTypeId): string {
    return \Drupal::entityTypeManager()->getDefinition($entityTypeId)->getKey('bundle');
  }

  public static function getEntityIdKey(string $entityTypeId): string {
    return \Drupal::entityTypeManager()->getDefinition($entityTypeId)->getKey('id');
  }

  public static function mainPropertyName(FieldDefinitionInterface $fieldDefinition): ?string {
    /** @var \Drupal\Core\Field\FieldItemInterface $itemClass */
    $itemClass = $fieldDefinition->getItemDefinition()->getClass();
    return $itemClass::mainPropertyName();
  }

  public static function getAccessControlHandler(string $entityTypeId): EntityAccessControlHandlerInterface {
    return \Drupal::entityTypeManager()->getAccessControlHandler($entityTypeId);
  }

  public static function getEntityBoolAccess(EntityInterface $entity, AccountInterface $account = NULL): CacheableBool {
    return CacheableBool::ifAccessResultAllowed(self::getEntityAccessResult($entity, $account));
  }

  /**
   * Wrap $entity->access() with some group specific hacks.
   */
  public static function getEntityAccessResult(EntityInterface $entity, AccountInterface $account = NULL): AccessResultInterface {
    if ($entity->isNew()) {
      $createAccessContext = [];
      if ($entity->getEntityTypeId() === 'group_content') {
        // @see \Drupal\group\Entity\Access\GroupContentAccessControlHandler::checkCreateAccess
        // @see \Drupal\group\Plugin\GroupContentEnablerBase::createAccess
        if (
          ($groupField = $entity->get('gid'))
          && ($groupFieldItem = $groupField->first())
          && ($group = $groupFieldItem->get('entity')->getValue())
        ) {
          $createAccessContext['group'] = $group;
        }
        else {
          return AccessResult::forbidden();
        }
      }
      $entityAccess = self::getAccessControlHandler($entity->getEntityTypeId())
        ->createAccess($entity->bundle(), $account, $createAccessContext, TRUE);
    }
    else {
      $entityAccess = $entity->access('update', $account, TRUE);
    }
    return $entityAccess;
  }

  public static function getEntityCreateBoolAccess(string $entityTypeId, ?string $bundle, AccountInterface $account = NULL): CacheableBool {
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $cacheableBool = CacheableBool::ifAccessResultAllowed(self::getEntityCreateAccessResult($entityTypeId, $bundle, $account));
    return $cacheableBool;
  }

  public static function getEntityCreateAccessResult(string $entityTypeId, ?string $bundle, AccountInterface $account = NULL): AccessResultInterface {
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $accessResult = self::getAccessControlHandler($entityTypeId)
      ->createAccess($bundle, $account, [], TRUE);
    return $accessResult;
  }

  public static function getListCacheability(string $entityId): CacheableDependencyInterface {
    $entityType = self::getEntityType($entityId);
    $cacheTags = $entityType->getListCacheTags();
    $cacheContexts = $entityType->getListCacheContexts();
    return (new CacheableMetadata())
      ->addCacheTags($cacheTags)
      ->addCacheContexts($cacheContexts);

  }


}
