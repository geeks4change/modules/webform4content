<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Entity;

use Drupal\Core\Field\FieldItemListInterface;

class FieldSingleValueWrapper {

  protected FieldItemListInterface $fieldItemList;

  protected ?string $property;

  public function __construct(FieldItemListInterface $fieldItemList, ?string $property) {
    $this->fieldItemList = $fieldItemList;
    $this->property = $property;
  }


  public static function create(FieldItemListInterface $fieldItemList, ?string $property = NULL) {
    return new static($fieldItemList, $property);
  }

  public function get() {
    return !$this->fieldItemList->isEmpty() ?
      $this->fieldItemList->first()->getValue():
      NULL;
  }

  public function getItem(): ?\Drupal\Core\TypedData\TypedDataInterface {
    if ($this->fieldItemList->count() > 1) {
      $this->fieldItemList->setValue(array_intersect_key($this->fieldItemList->getValue(), [TRUE]));
    }
    if ($this->fieldItemList->isEmpty()) {
      $this->fieldItemList->appendItem();
    }
    $item = $this->fieldItemList->first();
    return $item;
  }

  public function set($value): void {
    $item = $this->getItem();
    $property = $this->property ?? $item::mainPropertyName();
    $item->get($property)->setValue($value);
  }

}
