<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Entity;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;

final class FieldValueWrapper {

  protected FieldItemListInterface $fieldItemList;

  protected ?string $property;

  public function __construct(FieldItemListInterface $fieldItemList, ?string $property) {
    $this->fieldItemList = $fieldItemList;
    if ($property) {
      $this->property = $property;
    }
    else {
      if (!$fieldItemList->isEmpty()) {
        $this->property = $fieldItemList->first()::mainPropertyName();
      }
      else {
        $this->property = $fieldItemList->appendItem()::mainPropertyName();
        $fieldItemList->setValue([]);
      }
    }
  }


  public static function create(FieldItemListInterface $fieldItemList, ?string $property = NULL) {
    return new static($fieldItemList, $property);
  }

  public function get(): array {
    $values = [];
    foreach ($this->fieldItemList as $i => $item) {
      assert($item instanceof FieldItemInterface);
      $values[$i] = $item->get($this->property)->getValue();
    }
    return $values;
  }

  public function set(array $values): void {
    $values = array_values($values);
    $fieldValues = array_intersect_key(
      $this->fieldItemList->getValue(),
      $values
    );
    foreach ($values as $index => $value) {
      $property = $this->property;
      $fieldValues[$index][$property] = $value;
    }
    $this->fieldItemList->setValue($fieldValues);
  }

}
