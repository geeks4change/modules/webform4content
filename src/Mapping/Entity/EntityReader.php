<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Entity mapping wrapper.
 *
 * Ensure view access, and help with cardinality.
 */
final class EntityReader {

  protected ContentEntityInterface $entity;

  public function __construct(ContentEntityInterface $entity) {
    $this->entity = $entity;
  }

  public function get(string $fieldName): FieldReader {
    try {
      $fieldItemList = $this->entity->get($fieldName);
    }
    catch (\InvalidArgumentException $e) {
      throw new \InvalidArgumentException(sprintf('In %s of bundle %s: %s', $this->entity->getEntityTypeId(), $this->entity->bundle(), $e->getMessage()), 0, $e);
    }
    return new FieldReader($fieldItemList);
  }

  public function getEntity(): ContentEntityInterface {
    return $this->entity;
  }

}
