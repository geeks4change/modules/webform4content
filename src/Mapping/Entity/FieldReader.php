<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\Entity;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;

final class FieldReader {

  protected FieldItemListInterface $fieldItemList;

  public function __construct(FieldItemListInterface $fieldItemList) {
    $this->fieldItemList = $fieldItemList;
  }

  public function getValue(): array {
    return $this->fieldItemList->getValue();
  }

  public function getFieldDefinition(): FieldDefinitionInterface {
    return $this->fieldItemList->getFieldDefinition();
  }


}
