<?php

declare(strict_types=1);

namespace Drupal\webform4content\Mapping\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

class EntityBag {

  /**
   * @var array<\Drupal\Core\Entity\ContentEntityInterface>
   */
  protected array $entities = [];

  public function __construct() {}

  public function set(string $name, ?ContentEntityInterface $entity): void {
    $this->entities[$name] = $entity;
  }

  public function has(string $name): bool {
    return in_array($name, $this->getNames());
  }

  public function get(string $name): ?ContentEntityInterface {
    if (!array_key_exists($name, $this->entities)) {
      throw new \UnexpectedValueException($name);
    }
    return $this->entities[$name] ?? NULL;
  }

  /**
   * @return list<string>
   */
  public function getNames(): array {
    return array_keys($this->entities);
  }

  public function toDebugData(): array {
    $data = [];
    foreach ($this->entities as $name => $entity) {
      if ($entity) {
        $type = $entity->getEntityTypeId();
        $bundle = $entity->bundle();
        $data["$name($type:$bundle)"] = $entity->toArray();
      }
      else {
        $data["$name"] = NULL;
      }
    }
    return $data;
  }


}
