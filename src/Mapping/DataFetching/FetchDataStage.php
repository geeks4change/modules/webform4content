<?php

declare(strict_types=1);
namespace Drupal\webform4content\Mapping\DataFetching;

enum FetchDataStage {

  case FormController;
  case PrepareForm;

  public function label(): string {
    $class = array_reverse(explode("\\", __CLASS__))[0];
    return "$class::$this->name";
  }

}
