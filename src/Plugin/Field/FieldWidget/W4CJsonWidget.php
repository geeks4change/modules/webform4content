<?php

declare(strict_types=1);
namespace Drupal\webform4content\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Element\InlineEntityForm;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\InlineEntityForm\Helpers\SootheWebformUnsavedWarning;
use Drupal\webform4content\InlineEntityForm\WebformSubmissionInlineForm;
use Drupal\webform4content\Mapping\Webform\WebformTool;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'W4C JSON' widget.
 *
 * @todo Consider moving out to its own project.
 *
 * @FieldWidget(
 *   id = "webform4content_json",
 *   label = @Translation("W4C JSON"),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary",
 *   }
 * )
 */
final class W4CJsonWidget extends WidgetBase {

  protected WebformSubmissionConditionsValidatorInterface $conditionsValidator;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    return $instance;
  }

  public static function defaultSettings() {
    return [
      'webform' => '',
      'current_page' => '',
      ] + parent::defaultSettings();
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['webform'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform'),
      '#default_value' => $this->getSetting('webform'),
      '#required' => TRUE,
    ];
    $form['current_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current page key'),
      '#default_value' => $this->getSetting('current_page')
    ];
    return $form;
  }

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $webformId = $this->getSetting('webform');
    $webform = Webform::load($webformId);

    if (!$webform) {
      return [];
    }

    $webformSubmission = WebformSubmission::create([
      'webform_id' => $webformId,
    ]);

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $items->get($delta);
    $this->setNormalizedData($webformSubmission, $this->getJsonData($item));

    $build = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'webform_submission',
      '#bundle' => $webformId,
      '#default_value' => $webformSubmission,
      '#op' => 'edit',
      '#save_entity' => FALSE,
    ];

    WebformSubmissionInlineForm::setCurrentPage($build, $this->getSetting('current_page'));

    SootheWebformUnsavedWarning::fixInBuild($build);

    return $build;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $inlineFormHandler = InlineEntityForm::getInlineFormHandler('webform_submission');
    $massaged = [];
    foreach ($values as $index => $value) {
      $inlineForm =& $form[$this->fieldDefinition->getName()]['widget'][$index];
      $inlineFormHandler->entityFormSubmit($inlineForm, $form_state);
      /** @var \Drupal\webform\WebformSubmissionInterface $webformSubmission */
      $webformSubmission = $inlineForm['#entity'];
      $data1 = $this->getNormalizedData($webformSubmission);
      $data2 = array_filter(
        $data1,
        fn(string $key) => $this->isElementOnPage($key, $webformSubmission),
        ARRAY_FILTER_USE_KEY
      );
      $data3 = array_filter(
        $data2,
        fn(string $key) => $this->isElementVisible($key, $webformSubmission),
        ARRAY_FILTER_USE_KEY
      );
      $json = json_encode($data3);
      $massaged[$index] = [
        '_original_delta' => $value['_original_delta'] ?? NULL,
        'value' => $json,
      ];
    }
    return $massaged;
  }

  private function getJsonData(FieldItemInterface $item): array {
    $json = $item->get('value')->getValue();
    $data = $json ? json_decode($json, TRUE) : [];
    return is_array($data) ? $data : [];
  }

  /**
   * Get webform data as normalized (single elements are array too).
   *
   * Normalized data does not break when element is switched between single and
   * multiple.
   */
  private function getNormalizedData(WebformSubmission $webformSubmission): array {
    $webform = $webformSubmission->getWebform();
    $data = $webformSubmission->getData();
    // Note that array_map with key does not preserve key. This does.
    return iterator_to_array((function() use ($data, $webform) {
      foreach ($data as $key => $value) {
        $normalizedValue = WebformTool::elementIsMultiple($webform, $key)
          ? $value : [$value];
        yield $key => $normalizedValue;
      }
    })());
  }

  /**
   * Set webform data from normalized (single elements are array too).
   *
   * Normalized data does not break when element is switched between single and
   * multiple.
   */
  private function setNormalizedData(WebformSubmission $webformSubmission, array $data): void {
    $webform = $webformSubmission->getWebform();
    // Note that array_map with key does not preserve key. This does.
    $denormalizedData = iterator_to_array((function() use ($data, $webform) {
      foreach ($data as $key => $value) {
        $denormalizedValue = WebformTool::elementIsMultiple($webform, $key)
          ? $value : reset($value);
        yield $key => $denormalizedValue;
      }
    })());
    $webformSubmission->setData($denormalizedData);
  }

  private function isElementVisible(string $elementKey, WebformSubmissionInterface $webformSubmission): bool {
    $element = $webformSubmission->getWebform()->getElementDecoded($elementKey);
    /** @noinspection PhpTernaryExpressionCanBeReplacedWithConditionInspection */
    $isVisible = $element
      ? $this->conditionsValidator->isElementVisible($element, $webformSubmission)
      : FALSE;
    return $isVisible;
  }

  private function isElementOnPage(string $elementKey, WebformSubmissionInterface $webformSubmission): bool {
    $currentPage = $this->getSetting('current_page');
    if ($currentPage) {
      $element = $webformSubmission->getWebform()->getElement($elementKey);
      $parents = $element['#webform_parents'] ?? [''];
      $isOnPage = $parents[0] === $currentPage;
    }
    else {
      $isOnPage = TRUE;
    }
    return $isOnPage;
  }

}
