<?php

declare(strict_types=1);
namespace Drupal\webform4content\Plugin\ConfirmationBundle;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\confirmation\Entity\Confirmation;
use Drupal\confirmation\Entity\ConfirmationWithEmailInterface;
use Drupal\confirmation\Entity\RequiredEmailFieldTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * Plugin implementation of the confirmation.
 *
 * @ConfirmationBundle(
 *   id = "webform4content",
 *   label = @Translation("Webform4Content"),
 *   description = @Translation("Confirm webforms that get mapped to content.")
 * )
 */
class WebformSubmissionConfirmation extends Confirmation implements ConfirmationWithEmailInterface {

  use StringTranslationTrait;
  use RequiredEmailFieldTrait;

  public const BUNDLE = 'webform4content';

  public static function createInstance(WebformSubmissionInterface $webformSubmission, string $email) {
    return static::create(['bundle' => self::BUNDLE])
      ->setWebformSubmission($webformSubmission)
      ->setEmail($email);
  }

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields['webform_submission'] = BaseFieldDefinition::create('entity_reference')
      ->setRequired(TRUE)
      ->setSetting('target_type', 'webform_submission')
      ->setLabel(t('Webform submission'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    self::makeEmailFieldRequired($fields, $base_field_definitions);

    return $fields;
  }

  public function getWebformSubmission(): WebformSubmissionInterface {
    return $this->get('webform_submission')->first()->get('entity')->getValue();
  }

  /**
   * @return $this
   */
  public function setWebformSubmission(WebformSubmissionInterface $webformSubmission) {
    $this->set('webform_submission', $webformSubmission);
    return $this;
  }

  /**
   * @return array<\Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation>
   */
  public static function forWebformSubmission(WebformSubmissionInterface $webformSubmission, $onlyUnconfirmed = FALSE): array {
    $query = \Drupal::entityTypeManager()->getStorage('confirmation')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('bundle', self::BUNDLE)
      ->condition('webform_submission', $webformSubmission->id());
    $ids = $query->execute();
    return self::loadMultiple($ids);
  }

  protected function hookConfirmationStateSettled(bool $state): void {
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($this->getWebformSubmission());
    $webformSubmissionWrapper->onSetConfirmationState($state);
  }

  public function getConfirmationQuestion() {
    $timestamp = $this->getWebformSubmission()->getCreatedTime();
    $formattedTime = DateTimePlus::createFromTimestamp($timestamp)
      ->format('Y-m-d H:i:s');
    return $this->t('Confirm submission made on %time', ['%time' => $formattedTime]);
  }

  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    /** @var self $entity */
    foreach ($entities as $entity) {
      // Delete referenced webform submissions when purged after expiration.
      // Either this is unconfirmed, then it can not be confirmed anymore.
      // Or it is confirmed, then it is already mapped.
      $entity->getWebformSubmission()->delete();
    }
  }

}
