<?php

declare(strict_types=1);

namespace Drupal\webform4content\Plugin\WebformHandler;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\CacheableTypes\CacheableBoolInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rat\v1\RenderArray;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformFormHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\DataFetching\FetchDataStage;
use Drupal\webform4content\Mapping\DataStoring\StorerStage;
use Drupal\webform4content\Mapping\DataStoring\Storer;
use Drupal\webform4content\Mapping\Entity\EntityBag;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\Utility\FormStateWrapper;
use Drupal\webform4content\Mapping\Webform\WebformPrepopulate;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;
use Drupal\webform4content\Mapping\Workflow\StateGuard\StateTool;
use Drupal\webform4content\Mapping\Workflow\StateGuard\SystemAwareGuardInterface;
use Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform4content handler base class.
 */
abstract class Webform4ContentHandlerBase extends WebformHandlerBase {

  use DependencySerializationTrait;

  protected SystemAwareGuardInterface $stateGuard;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->stateGuard = $container->get('webform4content.state_guard');
    return $instance;
  }

  /**
   * Get entity mapper controller
   *
   * Must be public so the action can access it.
   */
  public function getEntityMapperController(): EntityMapperController {
    $controller = EntityMapperController::create($this, $this->webform);
    $this->defineEntityMappers($controller);
    return $controller;
  }

  abstract protected function defineEntityMappers(EntityMapperController $controller): void;

  /**
   * @inheritDoc
   * @noinspection PhpParameterNameChangedDuringInheritanceInspection
   *
   * - get prepopulated Webform tracker values
   * - load existing content entities from tracker values (if allowed)
   * - populate Webform data from those entities
   *
   * We do all this in prepareForm, rather than postLoad, to not interfere with
   * viewing submissions.
   */
  public function prepareForm(WebformSubmissionInterface $webformSubmission, $operation, FormStateInterface $form_state) {
    // Prepopulate possible tracker element data (others are discarded below).
    // Even after routeOverride voided the need to prepopulate tracker elements,
    // prepopulating other elements, and having them backmapped, may make sense,
    // even for routeOverride use cases.
    $webformSubmission->setData(
      WebformPrepopulate::create($webformSubmission)->getPrepopulateData()
      + $webformSubmission->getData()
    );
    // Only load tracked content, but do not map any other elements, so we have
    // correct values to map back.
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    $this->getEntityMapperController()
      ->fetchData($webformSubmissionWrapper, FetchDataStage::PrepareForm);
    parent::prepareForm($webformSubmission, $operation, $form_state);
  }

  /**
   * Warn user if webform is reused.
   *
   * @noinspection PhpParameterNameChangedDuringInheritanceInspection*/
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webformSubmission) {
    parent::alterForm($form, $form_state, $webformSubmission);

    // Prevent webform reuse.
    if (
      !$webformSubmission->isNew()
      && !$webformSubmission->isDraft()
      && !$form_state->isSubmitted()
    ) {
      $form['elements']['#disabled'] = TRUE;
      $form['actions']['#disabled'] = TRUE;
      // @see \Drupal\Core\Render\Element\StatusMessages::renderMessages
      $message = $this->t('Webform4Content form must not be reused.');
      $form['webform4content_no_reuse'] = [
        '#weight' => -99,
        '#theme' => 'status_messages',
        '#message_list' => ['error' => [$message]],
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ];
    }

    // Add route override controller submit handler.
    FormStateWrapper::create($form_state)
      ->applySubmitHandlers($form);
  }

  /** @noinspection PhpParameterNameChangedDuringInheritanceInspection */
  public function validateForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission) {
    parent::validateForm($form, $formState, $webformSubmission);
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    $storer = $this->getEntityMapperController()
      ->storeData($webformSubmissionWrapper, StorerStage::Validate);
    $this->displayFormValidationErrors($this->getEntityMapperController(), $storer, $webformSubmissionWrapper, $form, $formState);
  }

  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::submitForm($form, $form_state, $webform_submission);
    $workflowTransition = StateTool::getTransitionInPreSave($this->webformSubmission->get(WebformSubmissionWrapper::FIELD_STATE));
    $transitionId = $workflowTransition ? $workflowTransition->getId() : NULL;
    $isAcceptTransition = $transitionId === 'accept';
    if ($isAcceptTransition) {
      \Drupal::messenger()->addMessage($this->t('Successfully mapped webform.'));
    }
  }

  /** @noinspection PhpParameterNameChangedDuringInheritanceInspection */
  public function preSave(WebformSubmissionInterface $webformSubmission) {
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    // On submission, set state submitted.
    $state = $webformSubmissionWrapper->getState();
    // Set to submitted if nededed.
    if ($state->getId() === 'draft' && !$this->webformSubmission->isDraft()) {
      // May not be necessary, but be strict.
      /** @noinspection PhpUnusedLocalVariableInspection */
      $systemEnabler = $this->stateGuard->acquireSystemToken();
      $state->setValue('submitted');
    }

    // React to state changes.
    $workflowTransition = StateTool::getTransitionInPreSave($this->webformSubmission->get(WebformSubmissionWrapper::FIELD_STATE));
    if ($workflowTransition?->getId() == 'accept') {
      // Map and save.
      $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
      $this->getEntityMapperController()
        ->storeData($webformSubmissionWrapper, StorerStage::PreSave, TRUE);
    }
    // Save state change in entity for postSave hook.
    // Note that this does not work with get()/set().
    $webformSubmissionWrapper->setTmpWorkflowTransitionIdInPreSave($workflowTransition?->getId());
  }

  /** @noinspection PhpParameterNameChangedDuringInheritanceInspection */
  public function postSave(WebformSubmissionInterface $webformSubmission, $update = TRUE) {
    $systemTransitionEnabler = $this->stateGuard->acquireSystemToken();
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webformSubmission);
    if ($webformSubmissionWrapper->getState()->isTransitionAllowed('confirm_all_emails')) {
      // Request confirmation for all emails to confirm.
      $emailsToConfirm = $webformSubmissionWrapper->getEmailsToConfirm();
      if ($emailsToConfirm) {
        foreach ($emailsToConfirm as $email) {
          $confirmation = WebformSubmissionConfirmation::createInstance($webformSubmission, $email);
          $confirmation->save();
          Webform4ContentEmailHandlerForEmailConfirmation::sendConfirmationEmail($confirmation);
          $confirmation->setLastSentTime();
          $confirmation->save();
        }
      }
      else {
        // No confirmations needed, so update workflow state.
        // Recursion is guaranteed to terminate here.
        $webformSubmissionWrapper->getState()->setValue('emails_confirmed');
        $this->reSaveWebformSubmissionAndFixTransition($webformSubmissionWrapper);
        unset($systemTransitionEnabler);
      }
    }

    // Auto-accept if needed.
    if ($webformSubmissionWrapper->getState()->isTransitionAllowed('accept')) {
      $doAutoAccept = $this->getEntityMapperController()
        ->checkAutoAccept($webformSubmissionWrapper);
      if ($doAutoAccept) {
        $webformSubmissionWrapper->getState()->applyTransitionById('accept');
        // Recursion is guaranteed to terminate here.
        $this->reSaveWebformSubmissionAndFixTransition($webformSubmissionWrapper);
        unset($systemTransitionEnabler);
      }
    }
  }

  /**
   * On re-save in post-save and same request, adjustments need to be made.
   */
  public function reSaveWebformSubmissionAndFixTransition(WebformSubmissionWrapper $webformSubmissionWrapper): void {
    // Also increment the webform changed timestamp, so that handlers do not
    // send multiple completion emails.
    /** @see \Drupal\webform\Entity\WebformSubmission::getState */
    $originalTimestamp = $webformSubmissionWrapper->adjustCompletedTimestamp();

    $maybeTransitionId = $webformSubmissionWrapper->getTmpWorkflowTransitionIdFromPreSave();
    $webformSubmissionWrapper->getWebformSubmission()->save();
    $webformSubmissionWrapper->setTmpWorkflowTransitionIdInPreSave($maybeTransitionId);

    $webformSubmissionWrapper->setBackCompletedTimestamp($originalTimestamp);
  }

  private static function displayFormValidationErrors(
    EntityMapperController $controller,
    Storer $storer,
    WebformSubmissionWrapper $webformSubmissionWrapper,
    array $form,
    FormStateInterface $formState,
  ): void {
    $violationListsByElementKey = $controller
      ->getElementViolations($storer, $webformSubmissionWrapper);
    $elements = WebformFormHelper::flattenElements($form);
    uasort($elements, SortArray::sortByWeightProperty(...));
    foreach ($elements as $elementKey => $element) {
      $elementAccessResult = RenderArray::alter($element)->getAccessResult();
      if ($elementAccessResult->isAllowed()) {
        $violationList = $violationListsByElementKey[$elementKey] ?? [];
        foreach ($violationList as $violation) {
          $elementLabel = $element['#title'] ?? t('Element with key %key', ['%key' => $elementKey]);
          $uiMessage = new FormattableMarkup('@element: @message', [
            '@element' => $elementLabel,
            '@message' => $violation->getMessage()
          ]);
          if ($elementKey = '') {
            // @see \Drupal\Core\Entity\Entity\EntityFormDisplay::validateFormValues
            // @see \Drupal\webform4content\Mapping\EntityMapper\EntityMapper::mapViolationsToElementKeys
            $formState->setError($form, $uiMessage);
          }
          else {
            $formState->setError($element, $uiMessage);
          }
        }
      }
    }
  }

  public function postLoad(WebformSubmissionInterface $webform_submission) {
    parent::postLoad($webform_submission);
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webform_submission);
    // Trigger auto-healing code for legacy submissions.
    $state = $webformSubmissionWrapper->getState();
  }

  /**
   * @see \Drupal\webform\Plugin\WebformElementBase::checkAccessRules
   * @see \Drupal\webform\Entity\Webform::invokeHandlers
   */
  public function accessElement(array &$element, $operation, AccountInterface $account = NULL) {
    if (
      in_array($operation, ['create', 'edit'])
      && ($elementKey = $element['#webform_key'] ?? NULL)
    ) {
      $elementHookAccess = $this->hookElementAccess($elementKey, $account ?? \Drupal::currentUser());
      if (!$elementHookAccess->value()) {
        return CacheableBool::create(FALSE, $elementHookAccess)
          ->toNeutralOrForbidden();
      }

      $entityNames = [];
      foreach ($this->getEntityMapperController()->getAllNames() as $name) {
        $mapper = $this->getEntityMapperController()->getEntityMapper($name);
        $elementKeyFieldMap = $mapper->getElementKeyFieldPropertyMap();
        if (isset($elementKeyFieldMap[$elementKey])) {
          $entityNames[] = $name;
        }
      }

      // Hide the element, if it belongs to one or more entities, AND all
      // must not be shown.
      if ($entityNames) {
        $webformSubmissionWrapper = WebformSubmissionWrapper::create($this->webformSubmission);
        $mustShowMap = array_map(
          fn($entityName) => $this->hookMustShowEntity($entityName, $webformSubmissionWrapper),
          $entityNames
        );
        $mustShow = CacheableBool::or(...array_values($mustShowMap));
        return $mustShow->toNeutralOrForbidden();
      }
    }
    return parent::accessElement($element, $operation, $account);
  }

  public function hookElementAccess(string $elementName, AccountInterface $account): CacheableBoolInterface {
    return CacheableBool::create(TRUE, new CacheableMetadata());
  }

  /**
   * @deprecated Because name too easy to confuse with hookPrepareForm.
   */
  public function hookWebformSubmissionPrepare(WebformSubmissionWrapper $webformSubmissionWrapper): void {}

  /**
   * Handlers can do additional stuff to the webformSubmission after form
   *   submission and before storing data to entities.
   */
  public function hookWebformSubmissionPrepareStoring(WebformSubmissionWrapper $webformSubmissionWrapper): void {}

  /**
   * Handlers can do additional stuff before storing data to entities.
   */
  public function hookStorerPrepare(Storer $storer): void {}

  /**
   * Handlers can do additional stuff before saving.
   */
  public function hookStorePreSave(Storer $storer): void {}

  /**
   * Handlers can do additional stuff after saving.
   */
  public function hookStorerPostSave(Storer $storer): void {}

  public function hookPrepareCreatedEntity(string $entityName, ContentEntityInterface $entity): void {}

  public function hookMustStoreEntity(string $entityName, WebformSubmissionWrapper $webformSubmissionWrapper, ?ContentEntityInterface $maybeEntity): bool {
    return TRUE;
  }

  public function hookMustShowEntity(string $entityName, WebformSubmissionWrapper $webformSubmissionWrapper): CacheableBoolInterface {
    return CacheableBool::create(TRUE, new CacheableMetadata());
  }

}
