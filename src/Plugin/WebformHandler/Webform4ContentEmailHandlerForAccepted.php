<?php

declare(strict_types=1);

namespace Drupal\webform4content\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user_secure_tokens\SecureTokens;
use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper;

/**
 * Emails a webform submission.
 *
 * @WebformHandler(
 *   id = "webform4content_email_accepted",
 *   label = @Translation("Webform4Content: Email for accepted"),
 *   category = @Translation("Webform4Content"),
 *   description = @Translation("Sends an email when webform is accepted."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class Webform4ContentEmailHandlerForAccepted extends EmailWebformHandler {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $secureUserTokensEnabler = SecureTokens::getService()->acquireEnabler();
    $form = parent::buildConfigurationForm($form, $form_state);
    unset($secureUserTokensEnabler);
    $form['additional']['states']['#access'] = FALSE;
    return $form;
  }

  public function postDelete(WebformSubmissionInterface $webform_submission) {
    // Do nothing.
  }

  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $webformSubmissionWrapper = WebformSubmissionWrapper::create($webform_submission);
    $transitionId = $webformSubmissionWrapper->getTmpWorkflowTransitionIdFromPreSave();
    if ($transitionId  === 'accept') {
      $secureUserTokensEnabler = SecureTokens::getService()->acquireEnabler();
      $message = $this->getMessage($webform_submission);
      $this->sendMessage($webform_submission, $message);
      unset($secureUserTokensEnabler);
    }
  }

}
