<?php

declare(strict_types=1);

namespace Drupal\webform4content\Plugin\WebformHandler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\Plugin\WebformHandlerPluginCollection;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Plugin\ConfirmationBundle\WebformSubmissionConfirmation;

/**
 * Emails a webform submission.
 *
 * @todo Our custom token works with token mode, but breaks twig.
 *
 * @WebformHandler(
 *   id = "webform4content_email_email_confirmation",
 *   label = @Translation("Webform4Content: Email for email confirmation"),
 *   category = @Translation("Webform4Content"),
 *   description = @Translation("Alters the confirmation email."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class Webform4ContentEmailHandlerForEmailConfirmation extends EmailWebformHandler {

  protected ?WebformSubmissionConfirmation $currentConfirmation = NULL;

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['additional']['states']['#access'] = FALSE;
    $form['to']['to_mail']['#access'] = FALSE;
    return $form;
  }

  protected function buildTokenTreeElement(array $token_types = ['webform', 'webform_submission',], $description = NULL) {
    $token_types = array_merge($token_types, $this->additionalTokenTypes());
    return parent::buildTokenTreeElement($token_types, $description);
  }

  protected function elementTokenValidate(array &$form, array $token_types = ['webform', 'webform_submission', 'webform_handler',]) {
    $token_types = array_merge($token_types, $this->additionalTokenTypes());
    return parent::elementTokenValidate($form, $token_types);
  }

  protected function additionalTokenTypes(): array {
    return ['confirmation'];
  }

  public function postDelete(WebformSubmissionInterface $webform_submission) {
    // Do nothing.
  }

  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Do nothing.
  }

  protected function getMessageEmails(WebformSubmissionInterface $webform_submission, $configuration_name, $configuration_value) {
    if ($configuration_name === 'to') {
      return [$this->currentConfirmation->getEmail()];
    }
    return parent::getMessageEmails($webform_submission, $configuration_name, $configuration_value);
  }

  protected function replaceTokens($text, EntityInterface $entity = NULL, array $data = [], array $options = []) {
    $data += ['confirmation' => $this->currentConfirmation];
    return parent::replaceTokens($text, $entity, $data, $options);
  }

  protected function getBodyDefaultValues($format = NULL) {
    $bodyDefault = <<<EOD
To confirm, visit: {{ webform_token('[confirmation:response-url]', webform_submission, {confirmation: confirmation}, options) }}
Submitted on {{ webform_token('[webform_submission:created]', webform_submission, [], options) }}
Submitted by: {{ webform_token('[webform_submission:user]', webform_submission, [], options) }}
EOD;
    $formats = [
      'text' => $bodyDefault,
      'html' => $bodyDefault,
    ];
    return ($format === NULL) ? $formats : $formats[$format];
  }

  protected function doSendConfirmationEmail(WebformSubmissionConfirmation $confirmation) {
    $this->webformSubmission = $confirmation->getWebformSubmission();
    $this->currentConfirmation = $confirmation;
    $webformSubmission = $confirmation->getWebformSubmission();
    $message = $this->getMessage($webformSubmission);
    $this->sendMessage($webformSubmission, $message);
    $this->currentConfirmation = NULL;
  }

  public static function sendConfirmationEmail(WebformSubmissionConfirmation $confirmation): void {
    $webformSubmission = $confirmation->getWebformSubmission();
    $webform = $webformSubmission->getWebform();
    foreach (static::getInstances($webform) as $instance) {
      $instance->doSendConfirmationEmail($confirmation);
    }
  }

  /**
   * @return \Iterable<static>
   */
  protected static function getInstances(WebformInterface $webform): WebformHandlerPluginCollection {
    return $webform->getHandlers('webform4content_email_email_confirmation', TRUE);
  }

}
