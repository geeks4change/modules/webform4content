<?php

declare(strict_types=1);
namespace Drupal\webform4content\Plugin\WebformHandler;

use Drupal\webform\Entity\Webform;

final class W4cHandler {

  /**
   * @template T
   * @param string $webformId
   * @param class-string<T> $class
   * @return array<T>
   */
  protected static function allOfClass(string $webformId, string $class): array {
    $webform = Webform::load($webformId);
    $handlerPluginCollection = $webform->getHandlers(status: TRUE);
    $handlers = iterator_to_array($handlerPluginCollection->getIterator());
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $handlersOfClass = array_filter($handlers, fn($handler) => is_subclass_of($handler, $class));
    return $handlersOfClass;
  }

  /**
   * @return array<\Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase>
   */
  public static function all(string $webformId): array {
    return self::allOfClass($webformId, Webform4ContentHandlerBase::class);
  }

  /**
   * @return array<\Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase>
   */
  public static function allForPluginId(string $webformId, string $handlerId): array {
    $webform = Webform::load($webformId);
    $handlerPluginCollection = $webform->getHandlers(plugin_id: $handlerId, status: TRUE);
    $handlers = iterator_to_array($handlerPluginCollection->getIterator());
    return $handlers;
  }

  /**
   * PluginID is the ID of the handler definition.
   */
  public static function uniqueForPluginId(string $webformId, string $pluginId): ?Webform4ContentHandlerBase {
    $handlers = self::allForPluginId($webformId, $pluginId);
    if (count($handlers) > 1) {
      throw new \UnexpectedValueException("Duplicate handler with pluginID: $pluginId");
    }
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $handlers ? reset($handlers) : NULL;
  }

  /**
   * HandlerId is a string identifying the handler in the scope of the webform.
   */
  public static function forHandlerId(string $webformId, string $handlerId): ?Webform4ContentHandlerBase {
    $webform = Webform::load($webformId);
    $handlerPluginCollection = $webform->getHandlers(status: TRUE);
    return $handlerPluginCollection->get($handlerId);
  }

}
