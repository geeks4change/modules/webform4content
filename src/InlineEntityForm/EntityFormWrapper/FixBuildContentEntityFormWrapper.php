<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\EntityFormWrapper;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * An entity form wrapper that always build the inline form.
 *
 * Some validation handlers do an entity build. We give them a form object that
 * does what they expect.
 *
 * @see \Drupal\webform\WebformSubmissionConditionsValidator::elementValidate
 */
final class FixBuildContentEntityFormWrapper implements ContentEntityFormInterface {

  use EntityFormDecoratorTrait;

  public function __construct(
    protected ContentEntityFormInterface $decorated,
    protected array &$inlineForm,
  ) {}

  public function buildEntity(array $form, FormStateInterface $form_state) {
    return $this->decorated->buildEntity($this->inlineForm, $form_state);
  }

}
