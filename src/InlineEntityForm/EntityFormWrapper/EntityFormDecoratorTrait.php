<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\EntityFormWrapper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpFoundation\RequestStack;

trait EntityFormDecoratorTrait {

  protected ContentEntityFormInterface $decorated;

  public function updateFormLangcode($entity_type_id, EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $this->decorated->updateFormLangcode($entity_type_id, $entity, $form, $form_state);
  }

  public function updateChangedTime(EntityInterface $entity) {
    $this->decorated->updateChangedTime($entity);
  }

  public function getFormDisplay(FormStateInterface $form_state) {
    return $this->decorated->getFormDisplay($form_state);
  }

  public function setFormDisplay(EntityFormDisplayInterface $form_display, FormStateInterface $form_state) {
    $this->decorated->setFormDisplay($form_display, $form_state);
  }

  public function getFormLangcode(FormStateInterface $form_state) {
    return $this->decorated->getFormLangcode($form_state);
  }

  public function isDefaultFormLangcode(FormStateInterface $form_state) {
    return $this->decorated->isDefaultFormLangcode($form_state);
  }


  public function setOperation($operation) {
    return $this->decorated->setOperation($operation);
  }

  public function getBaseFormId() {
    return $this->decorated->getBaseFormId();
  }

  public function getFormId() {
    return $this->decorated->getFormId();
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    return $this->decorated->buildForm($form, $form_state);
  }

  public function form(array $form, FormStateInterface $form_state) {
    return $this->decorated->form($form, $form_state);
  }

  public function processForm($element, FormStateInterface $form_state, $form) {
    return $this->decorated->processForm($element, $form_state, $form);
  }

  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $this->decorated->afterBuild($element, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->decorated->submitForm($form, $form_state);
  }

  public function save(array $form, FormStateInterface $form_state) {
    return $this->decorated->save($form, $form_state);
  }

  public function buildEntity(array $form, FormStateInterface $form_state) {
    return $this->decorated->buildEntity($form, $form_state);
  }

  public function getEntity() {
    return $this->decorated->getEntity();
  }

  public function setEntity(EntityInterface $entity) {
    return $this->decorated->setEntity($entity);
  }

  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    return $this->decorated->getEntityFromRouteMatch($route_match, $entity_type_id);
  }

  public function getOperation() {
    return $this->decorated->getOperation();
  }

  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    return $this->decorated->setModuleHandler($module_handler);
  }

  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    return $this->decorated->setEntityTypeManager($entity_type_manager);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->decorated->validateForm($form, $form_state);
  }

  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    return $this->decorated->setConfigFactory($config_factory);
  }

  public function resetConfigFactory() {
    $this->decorated->resetConfigFactory();
  }

  public function setRequestStack(RequestStack $request_stack) {
    return $this->decorated->setRequestStack($request_stack);
  }

  public function setMessenger(MessengerInterface $messenger) {
    $this->decorated->setMessenger($messenger);
  }

  public function messenger() {
    return $this->decorated->messenger();
  }

  public function setRedirectDestination(RedirectDestinationInterface $redirect_destination) {
    $this->decorated->setRedirectDestination($redirect_destination);
  }

  public function setStringTranslation(TranslationInterface $translation) {
    $this->decorated->setStringTranslation($translation);
  }

}
