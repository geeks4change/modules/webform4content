<?php

declare(strict_types=1);

namespace Drupal\webform4content\InlineEntityForm;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\InlineEntityForm\EntityFormWrapper\FixBuildContentEntityFormWrapper;
use Drupal\webform4content\InlineEntityForm\FormState\InlineFormStateInjector;
use Drupal\webform4content\InlineEntityForm\Helpers\AttributesFixer;
use Drupal\webform4content\InlineEntityForm\Helpers\CallbacksFixer;
use Drupal\webform4content\InlineEntityForm\Helpers\CurrentPageFixer;
use Drupal\webform4content\InlineEntityForm\Helpers\FormStateTmp;
use Drupal\webform4content\InlineEntityForm\Helpers\SootheWebformUnsavedWarning;
use Drupal\webform4content\InlineEntityForm\Helpers\WebformParentsFixer;
use Drupal\webform4content\InlineEntityForm\Helpers\WebformStatesFixer;
use Drupal\webform4content\InlineEntityForm\Helpers\WebformValidationFixer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle a WebformSubmission inline form.
 *
 * Parent uses EntityFormDisplay, which won't work for webform elements.
 * @see \Drupal\inline_entity_form\Form\EntityInlineForm::entityForm
 *
 * Instead leverage
 */
final class WebformSubmissionInlineForm extends EntityInlineForm {

  protected const PROP_CURRENT_PAGE = '#inline_webform_submission_current_page';

  protected WebformSubmissionConditionsValidatorInterface $conditionsValidator;

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    return $instance;
  }

  public static function setCurrentPage(array &$element, string $currentPage) {
    $element[self::PROP_CURRENT_PAGE] = $currentPage;
  }

  public function entityForm(array $entity_form, FormStateInterface $form_state) {
    $entity_form = parent::entityForm($entity_form, $form_state);

    $currentPage = $entity_form[self::PROP_CURRENT_PAGE];

    /** @var \Drupal\webform\Entity\WebformSubmission $webformSubmission */
    $webformSubmission = $entity_form['#entity'];

    /** @var \Drupal\webform\WebformSubmissionForm $subformObject */
    $subformObject = \Drupal::entityTypeManager()->getFormObject('webform_submission', 'edit');
    $subformObject->setEntity($webformSubmission);
    $subformObject = new FixBuildContentEntityFormWrapper($subformObject, $entity_form);

    // Changes form state, will be reverted when it goes out of scope.
    $tmpFormState = new FormStateTmp($form_state);
    $tmpFormState->set('current_page', $currentPage);

    $tmpFormState->set('form_display', $this->getFormDisplay($webformSubmission, $entity_form['#form_mode']));
    $tmpFormState->set('current_page', $entity_form[self::PROP_CURRENT_PAGE] ?? NULL);
    // $webformSubmission->setCurrentPage($currentPage);

    SootheWebformUnsavedWarning::fixInProcess($form_state->getCompleteForm());

    // BUILD the form.
    $entity_form = $subformObject->form($entity_form, $form_state);

    // Change form object from now (#process) to #after_build.
    // @todo Make computed elements work by also passing subformState.
    // @see \Drupal\webform\Element\WebformComputedBase::getWebformSubmission
    // @see \Drupal\webform\WebformSubmissionForm::copyFormValuesToEntity
    // (new FormObjectFixer($formObject))
    //  ->fixInProcess($entity_form, $form_state);

    // Swap out formState, formObject, and add subform.
    // Successor of FormObjectFixer and FormStateFixer.
    InlineFormStateInjector::changeToInlineFormState($form_state, $entity_form, $subformObject);

    // Fix current-page for all pages.
    CurrentPageFixer::fixAllPagesInProcess($entity_form['elements'], $webformSubmission);

    // Add entity builder.
    $entity_form['#entity_builders'][] = [get_class($this), 'buildWebformSubmission'];

    // Disable the progress bar.
    $entity_form['progress']['#access'] = FALSE;

    // Fix #parents.
    // Webform needs flattened element values. But we do not want them totally
    // flattened. So adjust parents.
    // Doing so in #after_build breaks groups, so do it in #process.
    // Do it before all others, to not interfere with dynamic elements like
    // text_format, that are generated in #process.
    // @todo Do this directly, and directly after build.
    $entity_form['#tree'] = TRUE;
    $entity_form['elements'] += ['#process' => []];
    array_unshift($entity_form['elements']['#process'], [WebformParentsFixer::class, 'process']);

    // Make callbacks absolute: ::foo must be WebformSubmissionForm::foo
    CallbacksFixer::makeAbsolute($entity_form, $subformObject);

    // Adjust states. See webform.states.js.
    $elementsParents = array_merge($entity_form['#parents'], ['elements']);
    WebformStatesFixer::prefixStateNames($entity_form['elements'], $elementsParents);
    //  $.fn.isWebform = function () {
    //    return $(this).closest('form.webform-submission-form, form[id^="webform"], form[data-is-webform]').length ? true : false;
    //  };
    //   $.fn.isWebformElement = function () {
    //    return ($(this).isWebform() || $(this).closest('[data-is-webform-element]').length) ? true : false;
    //  };
    $entity_form['#attributes']['data-is-webform-element'] = TRUE;

    // Add fake actions to soothe WebformSubmissionForm::afterBuild
    $entity_form['actions'] = [];
    $entity_form['#action'] = 'form_action_DUMMY';

    // Apply states to make elements un-required.
    $this->conditionsValidator->buildForm($entity_form, $form_state);

    // Fix webform element validation.
    (new WebformValidationFixer($entity_form, $subformObject, $currentPage))
      ->fixElements();

    // Drop autofocus.
    AttributesFixer::removeClasses($entity_form, 'js-webform-autofocus');

    return $entity_form;
  }

  /**
   * Webform entity builder callback.
   */
  public static function buildWebformSubmission(string $entityTypeId, WebformSubmissionInterface $webformSubmission, array $form, FormStateInterface $formState) {
    // $form has #parents, its children do not yet.
    $parents = $form['#parents'];
    $parents[] = 'elements';
    // Set all data items, the consumer widget may filter this.
    $webformSubmission->setData($formState->getValue($parents, []));
  }

}
