<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\FormState;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * InlineFormState, similar to SubformState.
 *
 * - SubformState translates formState::getValues)
 * - InlineFormState additionally fakes ::getCompleteForm.
 * - InlineFormState additionally has a formObject.
 * - does NOT have getCompleteFormState,
 * - but getParentFormState, getRootFormState.
 */
final class InlineFormState extends ForkOfSubformState implements InlineFormStateInterface {

  protected FormInterface $formObject;

  protected function __construct(
    array &$subform,
    array &$parentForm,
    FormStateInterface $parentFormState,
    FormInterface $formObject,
  ) {
    $this->decoratedFormState = $parentFormState;
    $this->parentForm = $parentForm;
    $this->subform = $subform;
    $this->formObject = $formObject;
  }

  public static function createForSubform(
    array &$subform,
    array &$parentForm,
    FormStateInterface $parentFormState,
    FormInterface $formObject,
  ) {
    return new static($subform, $parentForm, $parentFormState, $formObject);
  }

  public function &getCompleteForm() {
    return $this->subform;
  }

  public function getFormObject(): FormInterface {
    return $this->formObject;
  }

  public function getParentFormState(): FormStateInterface {
    return $this->decoratedFormState;
  }

  public function getRootFormState(): FormStateInterface {
    return ($this->decoratedFormState instanceof InlineFormStateInterface)
      ? $this->decoratedFormState->getRootFormState() : $this->decoratedFormState;
  }

}
