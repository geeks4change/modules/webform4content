<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\FormState;

use Drupal\Core\Form\FormStateInterface;

interface InlineFormStateInterface extends FormStateInterface {

  public function getParentFormState(): FormStateInterface;
  public function getRootFormState(): FormStateInterface;

}
