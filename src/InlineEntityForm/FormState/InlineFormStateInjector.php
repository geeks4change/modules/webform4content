<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\FormState;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\h4d_shared\DumperTool;

/**
 * Form state fixer.
 *
 * Webform is not designed as inline form and quite some places assume it is the
 * only form. Fix this by swapping out form state and form object.
 *
 * @see \Drupal\webform\Element\WebformComputedBase::getWebformSubmission
 * @see \Drupal\webform\WebformSubmissionConditionsValidator::elementValidate
 */
final class InlineFormStateInjector {

  public function __construct(
    protected FormStateInterface $formState,
  ) {}

  /**
   * Swap out form staten and form object until #after_build.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The subform.
   * @param array $subform
   *   The current form state.
   * @param \Drupal\Core\Form\FormInterface $formObject
   *   The new form object.
   *
   * @return void
   */
  public static function changeToInlineFormState(FormStateInterface &$formState, array &$subform, FormInterface $formObject): void {
    $inlineFormState = InlineFormState::createForSubform(
      $subform, $formState->getCompleteForm(), $formState, $formObject);
    $formState = $inlineFormState;

    // We can't swap out formState ourselves, because caller has no reference.
    // But we can add a #process callback.
    // Because caller itself runs in #process, it can not be done on the same
    // array level, but as #process works downward, do it in first child.
    // The gap in this is later #process callbacks on this level though.
    $injector = new self($inlineFormState);
    $prepend['webform4content_inline_state_injector']['#process'][] = [$injector, 'inChildProcessChangeToInlineFormState'];
    $subform = $prepend + $subform;

    // Add #after_build callback to restore formState.
    $subform['#after_build'][] = [self::class, 'afterBuildRestoreParentFormState'];
  }

  public function inChildProcessChangeToInlineFormState(array &$element, FormStateInterface &$formState, array &$completeForm): array {
    $formState = $this->formState;
    return $element;
  }

  /**
   * @internal
   */
  public static function afterBuildRestoreParentFormState(array $elements, FormStateInterface &$formState): array {
    if (!$formState instanceof InlineFormStateInterface) {
      throw new \UnexpectedValueException('Expected InlineFormState.');
    }
    $formState = $formState->getParentFormState();
    return $elements;
  }

}
