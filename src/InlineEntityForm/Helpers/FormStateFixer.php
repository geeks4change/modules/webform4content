<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;

/**
 * Form state fixer.
 *
 * Webform is not designed as inline form and quite some places assume it is the
 * only form. Fix this by swapping out form state and form object.
 *
 * @see \Drupal\webform\Element\WebformComputedBase::getWebformSubmission
 * @see \Drupal\webform\WebformSubmissionConditionsValidator::elementValidate
 *
 * @todo Reconsider WebformValidationFixer
 * @todo Reconsider FormObjectFixer
 */
final class FormStateFixer {

  private function __construct(
    private FormStateInterface $formState,
    private FormInterface $formObject,
    private array &$completeForm,
  ) {}

  /**
   * Swap out form staten and form object until #after_build.
   *
   * @param array $subform
   *   The subform.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current form state.
   * @param \Drupal\Core\Form\FormInterface $formObject
   *   The new form object.
   *
   * @return void
   */
  public static function swapOutFormStateInPreprocess(array &$subform, FormStateInterface &$formState, FormInterface $formObject): void {
    $fixer = new self($formState, $formState->getFormObject(), $formState->getCompleteForm());
    $subform['#after_build'][] = [$fixer, 'callbackAfterBuild'];
    $subformState = SubformState::createForSubform($subform, $formState->getCompleteForm(), $formState);
    $subformState->setFormObject($formObject);
    $subformState->setCompleteForm($subform);
    $formState = $subformState;
  }

  /**
   * @internal Callback #after_build.
   */
  public function callbackAfterBuild(array $elements, FormStateInterface &$formState): array {
    $formState = $this->formState;
    $formState->setFormObject($this->formObject);
    $formState->setCompleteForm($this->completeForm);
    return $elements;
  }

}
