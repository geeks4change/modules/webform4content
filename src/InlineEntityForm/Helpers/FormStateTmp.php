<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

final class FormStateTmp {

  protected array $originalProperties = [];

  protected ?FormInterface $originalFormObject = NULL;

  public function __construct(
    protected FormStateInterface $formState,
  ) {}

  public function set(string $property, mixed $value): void {
    $this->originalProperties[$property] = $this->formState->get($property);
    $this->formState->set($property, $value);
  }

  public function setFormObject(FormInterface $formObject): void {
    // Only back this up once.
    if (!$this->originalFormObject) {
      $this->originalFormObject = $this->formState->getFormObject();
    }
    $this->formState->setFormObject($formObject);
  }

  public function __destruct() {
    foreach ($this->originalProperties as $property => $value) {
      $this->formState->set($property, $value);
    }
    if ($this->originalFormObject) {
      $this->formState->setFormObject($this->originalFormObject);
    }
  }

}
