<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Render\Element;

final class WebformStatesFixer {

  /**
   * Prefix parents to all states selector names.
   *
   * Example:
   * {"invisible-slide":{".webform-submission-h4c-fbz-node-5-edit-form :input[name=\\u0022homepage_check\\u0022]":{"unchecked":true}}}
   *
   * where we want name to be eg
   * :input[name="field_h4c_fbz_basics[0][elements][homepage_check]"]
   * :input[name="waermeversorgung[checkboxes][kwk]"]
   *
   * @see \Drupal\webform\WebformSubmissionForm::addStatesPrefix
   *   which prefixes selector with class to make it specific.
   *
   * @param array $elements
   * @param list<string> $parents
   */
  public static function prefixStateNames(array &$elements, array $parents): void {
    if (!empty($elements['#states'])) {
      self::doPrefixStateNames($elements['#states'], $parents);
    }
    foreach (Element::children($elements) as $key) {
      self::prefixStateNames($elements[$key], $parents);
    }
  }

  public static function doPrefixStateNames(array &$elements, array $parents): void {
    if (!$parents) {
      return;
    }
    $parentsPrefix = self::createPrefix($parents);
    $result = [];
    foreach ($elements as $key => &$element) {
      if (
        is_string($key)
        && preg_match('~^(.*:input\[name=")([^\[]*)(\[.*])?("])$~u', $key, $matches)
      ) {
        [, $prefix, $firstName, $trailingNames, $suffix] = $matches;
        $key = "$prefix{$parentsPrefix}[$firstName]$trailingNames$suffix";
      }
      if (is_array($element)) {
        self::doPrefixStateNames($element, $parents);
      }
      $result[$key] = $element;
    }
    $elements = $result;
  }

  private static function createPrefix(array $parents): string {
    $parts = [];
    if ($parents) {
      $parts[] = array_shift($parents);
      $trailing = array_map(fn(string $part) => "[$part]", $parents);
      $parts = array_merge($parts, $trailing);
    }
    return implode('', $parts);
  }


}
