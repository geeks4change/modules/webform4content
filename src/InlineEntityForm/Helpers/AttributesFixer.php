<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

final class AttributesFixer {

  public static function removeClasses(array &$elements, ...$classesToRemove): void {
    if (
      isset($elements['#attributes']['class'])
      && ($classes =& $elements['#attributes']['class'])
      && is_array($classes)
    ) {
      $classes = array_diff($classes, $classesToRemove);
    }
  }

}
