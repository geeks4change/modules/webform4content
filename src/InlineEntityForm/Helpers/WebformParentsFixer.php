<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Fix webform element parents.
 *
 * Webform submission form has root #tree=FALSE, so all elements have only its
 * key as #parents. Some webform elements though set #tree=true, to house some
 * internal structure.
 * For the inline form, this mostly-flattened structure needs to live under
 * some root parents. This is not possible with #tree alone, so handled here.
 *
 * For reference (see FormBuilder):
 * - #tree defaults to false
 * - #tree is propagated downwards
 * - Any element with explicit #parents has these.
 * - Any element with #tree=false gets #parents=[$key]
 * - The child of any element with #tree=false gets #parents=[$key]
 * - All others (element and parent #tree=true) get #parents propagated,
 *   i.e. #parents = parent#parents + key.
 * @see \Drupal\Core\Form\FormBuilder::doBuildForm
 */
final class WebformParentsFixer {

  /**
   * @see \Drupal\Core\Render\Element\RenderElement::processGroup
   */
  public static function process(array $element, FormStateInterface $formState, array &$completeForm): array {
    self::fixWebformElementParents($element, $element['#parents'], TRUE);
    return $element;
  }

  /**
   * Set all element parents to parents of this element, then respect #tree.
   *
   * @see \Drupal\webform\Entity\Webform::initElementsRecursive
   */
  public static function afterBuild(array $element, FormStateInterface $formState): array {
    self::fixWebformElementParents($element, $element['#parents'], TRUE);
    return $element;
  }

  private static function fixWebformElementParents(array &$elements, array $rootParents, bool $propagateParents): void {
    foreach (Element::children($elements) as $key) {
      $element =& $elements[$key];
      if (is_array($element)) {
        // Webform element must have root parents.
        if (!empty($element['#webform_element'])) {
          $element['#parents'] = array_merge($rootParents, [$key]);
        }
        else {
          if ($propagateParents) {
            // Copied from @see \Drupal\Core\Form\FormBuilder::doBuildForm
            $element['#parents'] = ($element['#tree'] && $elements['#tree'])
              ? array_merge($elements['#parents'], [$key]) : [$key];
          }
        }
        // Webform elements may be containers and have other webform elements
        // as children, so recurse in any case.
        self::fixWebformElementParents($element, $rootParents, $propagateParents);
      }
    }
  }

}
