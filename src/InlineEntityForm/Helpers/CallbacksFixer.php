<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormInterface;

final class CallbacksFixer {

  public static function makeAbsolute(array &$elements, FormInterface $formObject): void {
    foreach (['#preprocess', '#process', '#pre_render', '#post_render', '#after_build', '#entity_builders',] as $callbacksKey) {
      if (is_array($elements[$callbacksKey] ?? NULL)) {
        foreach ($elements[$callbacksKey] as &$callback) {
          if (is_string($callback) && str_starts_with($callback, '::')) {
            $callback = [$formObject, substr($callback, 2)];
          }
        }
      }
    }
  }

}
