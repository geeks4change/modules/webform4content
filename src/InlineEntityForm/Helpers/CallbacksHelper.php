<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

final class CallbacksHelper {

  public static function prepend(?array &$callbacks, callable $callback): void {
    if (!is_array($callbacks)) {
      $callbacks = [];
    }
    array_unshift($callbacks, $callback);
  }

}
