<?php

declare(strict_types=1);
namespace Drupal\webform4content\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

final class FormObjectFixer {

  protected FormInterface $originalFormObject;

  public function __construct(
    protected FormInterface $formObject,
  ) {}

  public function fixInProcess(array &$elements, FormStateInterface $formState): void {
    $this->originalFormObject = $formState->getFormObject();
    $formState->setFormObject($this->formObject);
    $elements['#after_build'][] = [$this, 'callbackAfterBuild'];
  }

  public function callbackAfterBuild(array $elements, FormStateInterface $form_state): array {
    $form_state->setFormObject($this->originalFormObject);
    return $elements;
  }

}
