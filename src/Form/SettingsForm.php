<?php

declare(strict_types=1);
namespace Drupal\webform4content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

final class SettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  protected function getEditableConfigNames() {
    return ['webform4content.settings'];
  }

  public function getFormId() {
    return 'webform4content_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform4content.settings');
    $form['debug_modes'] = [
      '#title' => $this->t('Debug modes'),
      '#type' => 'checkboxes',
      '#options' => DebugMode::options(),
      '#default_value' => $config->get('debug_modes'),
    ];
    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do here.
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('webform4content.settings')
      ->set('debug_modes', array_filter(array_values($form_state->getValue('debug_modes'))))
      ->save();
  }

}
