<?php

declare(strict_types=1);
namespace Drupal\webform4content\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;

enum DebugMode {

  case mapping;
  case item_mapping;
  case date;

  public function key(): string {
    return $this->name;
  }

  public function label(): TranslatableMarkup {
    return match ($this) {
      self::mapping => t('Mapping'),
      self::item_mapping => t('Item mapping'),
      self::date => t('Date'),
    };
  }

  /**
   * @return array<string, \Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function options(): array {
    $options = [];
    foreach (self::cases() as $case) {
      $options[$case->key()] = $case->label();
    }
    return $options;
  }

}
