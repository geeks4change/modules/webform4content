<?php

declare(strict_types=1);

namespace Drupal\webform4content_test_group\Plugin\WebformHandler;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4content\Mapping\Condition\EntityCondition\Always;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\MappedEntityCondition;
use Drupal\webform4content\Mapping\DataStoring\Storer;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapper\EntitySelectMapper;
use Drupal\webform4content\Mapping\EntityMapper\Wrapper\EntityMapperFallback;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Field;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Wrapper\EmailConfirmation;
use Drupal\webform4content\Mapping\ItemMapper\EntityGroupFieldItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\ReferenceItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Element;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeCreateOverrideProvider;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

/**
 * @WebformHandler(
 *   id = "webform4content_group_test",
 *   label = @Translation("W4CT Group test"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Use webform to create content."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class Webform4ContentGroupTestHandler extends Webform4ContentHandlerBase {

  protected const GROUP_CONTENT_PLUGIN_ID = 'group_node:w4ct_group';

  protected const GROUP_CREATOR_ROLES = ["w4ct-administrator"];

  protected const SELECT_USER_ELEMENT = 'select_user';

  public const SELECT_USER_PERMISSION = 'edit any webform submission';

  protected const CREATE_USER_CONTAINER = 'create_user_container';

  /**
   * Prepopulate select_user = current user for auth.
   *
   * If admin, this default can be changed, for others not.
   */
  public function prepareForm(WebformSubmissionInterface $webformSubmission, $operation, FormStateInterface $form_state) {
    parent::prepareForm($webformSubmission, $operation, $form_state);

    if (\Drupal::currentUser()->isAuthenticated()) {
      $webformSubmission->setElementData(self::SELECT_USER_ELEMENT, \Drupal::currentUser()->id());
    }
  }

  /**
   * Control user element access (do NOT try to change that in the UI!):
   *
   * - Allow select_user only for admin ('edit any webform submission').
   * - Allow create_user only for anonymous.
   *
   * Do NOT try to implement access both here and in the UI! Never!
   * Trying so rapidly leads to Drupal's 3-state access hell. (access handlers
   * are orif'ed, and the others return allowed / neutral, so those will grant
   * access, but never deny access.)
   *
   * We choose to have the access control here only, as it can be audited
   * better.
   *
   * @see \Drupal\webform\Plugin\WebformElementBase::checkAccessRules
   */
  public function accessElement(array &$element, $operation, AccountInterface $account = NULL) {
    if ($operation === 'create' || $operation === 'edit') {
      $isAuthenticated = \Drupal::currentUser()->isAuthenticated();

      $elementKey = $element['#webform_key'] ?? NULL;
      $elementParents = $element['#webform_parents'] ?? [];

      if ($elementKey === self::SELECT_USER_ELEMENT) {
        return CacheableBool::ifAccessResultAllowed(
          AccessResult::allowedIfHasPermission(
            \Drupal::currentUser(),
            self::SELECT_USER_PERMISSION
          )
        )->toAllowedOrForbidden();
      }
      // Prevent access on a container does not help, so check parents.
      if (in_array(self::CREATE_USER_CONTAINER, $elementParents)) {
        return CacheableBool::create(!$isAuthenticated, $this->webform)
          ->toAllowedOrForbidden();
      }
    }

    return parent::accessElement($element, $operation, $account);
  }

  protected function defineEntityMappers(EntityMapperController $controller): void {
    // -- Entities --
    $nodeMapper = new EntityMapper(entityTypeId: 'node', bundleName: 'w4ct_group');
    $groupMapper = new EntityMapper(entityTypeId: 'group', bundleName: 'w4ct');

    $userSelectMapper = new EntitySelectMapper(
      entityTypeId: 'user', selectElement: new Element('select_user'));
    $userCreateMapper = new EntityMapper(entityTypeId: 'user', bundleName: 'user');
    $userMapper = new EntityMapperFallback(
      $userSelectMapper,
      $userCreateMapper,
    );

    // -- RootMappers and Relations --
    $controller->addEntityMapper($nodeMapper);
    $controller->addEntityMapper($groupMapper);
    ReferenceItemMapper::create($nodeMapper, new Field('uid'), $userMapper);
    // Ensure that correct user is used for membership auto-create.
    ReferenceItemMapper::create($groupMapper, new Field('uid'), $userMapper);

    // -- Tracker(s).
    $controller->addTracker($nodeMapper, 'node_tracker');

    // -- RouteOverrides --
    $controller->addRouteOverride($nodeMapper,
      new NodeCreateOverrideProvider('w4ct_group', new Always())
    );

    // -- AutoAccept --
    $controller->setAutoAcceptCondition(
      new MappedEntityCondition($nodeMapper, new Always())
    );

    // -- Fields --
    ItemMapper::create($nodeMapper, new Field('title'), new Element('node_title'));

    ItemMapper::create($groupMapper, new Field('label'), new Element('group_title'));

    ItemMapper::create($userCreateMapper,
      new EmailConfirmation(new Field('mail')), new Element('email')
    );
    ItemMapper::create($userCreateMapper,
      new Field('name'), new Element('username')
    );

  }

  /**
   * Create groupContent, and if needed groupMembership.
   *
   * According to Group::postSave, membership auto-create for programmatically
   * created groups will be deprecated someday.
   */
  public function hookStorerPostSave(Storer $storer): void {
    $node = $storer->forEntity('node')->getMaybeEntity();
    $group = $storer->forEntity('group')->getMaybeEntity();
    $user = $storer->forEntity('user')->getMaybeEntity();
    if (
      $node instanceof NodeInterface
      && $group instanceof GroupInterface
    ) {
      $group->addContent($node, self::GROUP_CONTENT_PLUGIN_ID);
    }

    $autoCreateMembershipConfigured = $this->configFactory
      ->get('group.type.w4ct')
      ->get('creator_membership');
    if (
      $user instanceof UserInterface
      && $group instanceof GroupInterface
      &&!$autoCreateMembershipConfigured
    ) {
      $group->addMember($user, ['group_roles' => self::GROUP_CREATOR_ROLES]);
    }
  }

}
