<?php

declare(strict_types=1);

namespace Drupal\webform4content_test\Plugin\WebformHandler;

use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform4content\Mapping\Condition\EntityCondition\Always;
use Drupal\webform4content\Mapping\Condition\EntityCondition\HasEntityAccess;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\ElementValueOneOf;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\MappedEntityCondition;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapper\EntitySelectMapper;
use Drupal\webform4content\Mapping\EntityMapper\Wrapper\EntityMapperSwitch;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\DateField;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\DateRangeField;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Field;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\JsonField;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Wrapper\EmailConfirmation;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\ReferenceItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\DateElement;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\DateRangeElements;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Element;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\MultiElement;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeCreateOverrideProvider;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeUpdateOverrideProvider;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

/**
 * @WebformHandler(
 *   id = "webform4content_test",
 *   label = @Translation("Webform4ContentTestHandler"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Use webform to create content."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class Webform4ContentTestHandler extends Webform4ContentHandlerBase {

  protected function defineEntityMappers(EntityMapperController $controller): void {

    // -- Entities --
    // The parameter names here are optional and only for docs.
    $userSelectMapper = new EntitySelectMapper(entityTypeId: 'user', selectElement: new Element('select_user'));
    $userCreateMapper = new EntityMapper(entityTypeId: 'user', bundleName: 'user');
    $switchCondition = new ElementValueOneOf(new Element('create_user'), ['1']);
    $userMapper = new EntityMapperSwitch(
      $switchCondition,
      $userCreateMapper,
      $userSelectMapper,
    );
    $nodeMapper = new EntityMapper(entityTypeId: 'node', bundleName: 'webform4content_test');
    // entityName = entityTypeId unless setEntityName (before addEntityMapper).
    // i.e. this is implicit:
    $nodeMapper->setEntityName('node');

    // -- RootMappers and Relations --
    $controller->addEntityMapper($nodeMapper);
    ReferenceItemMapper::create($nodeMapper, new Field('uid'), $userMapper);

    // -- Tracker(s).
    $controller->addTracker($nodeMapper, 'webform4content_test');
    $controller->addTracker($userMapper, 'account');

    // -- RouteOverrides --
    $controller->addRouteOverride($nodeMapper,
      new NodeCreateOverrideProvider('webform4content_test', new Always()));
    $controller->addRouteOverride($nodeMapper,
      new NodeUpdateOverrideProvider('webform4content_test', new Always()));

    // -- AutoAccept --
    $controller->setAutoAcceptCondition(
      new MappedEntityCondition($nodeMapper, new HasEntityAccess())
    );

    // -- Fields --
    ItemMapper::create($userCreateMapper, new EmailConfirmation(new Field('mail')), new Element('email'));
    ItemMapper::create($userCreateMapper, new Field('name'), new Element('username'));

    ItemMapper::create($nodeMapper, new Field('title'), new Element('title'));
    ItemMapper::create($nodeMapper, new Field('field_webform4content_body'), new Element('body'));
    ItemMapper::create($nodeMapper, new Field('field_webform4content_number'), new Element('number'));
    ItemMapper::create($nodeMapper, new Field('field_webform4content_bool'), new Element('bool'));
    ItemMapper::create($nodeMapper, new JsonField('field_webform4content_json'), new MultiElement('jtext', 'jnumber', 'jbool'));
    ItemMapper::create($nodeMapper, new DateField('field_w4c_timestamp'), new DateElement('timestamp'));
    ItemMapper::create($nodeMapper, new DateField('field_w4c_date'), new DateElement('date'));
    ItemMapper::create($nodeMapper, new DateRangeField('field_w4c_date_range'),
      new DateRangeElements(
        new DateElement('date_range_start'),
        new DateElement('date_range_end'),
      ),
    );

  }

}
