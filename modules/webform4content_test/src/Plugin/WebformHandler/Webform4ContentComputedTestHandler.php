<?php

declare(strict_types=1);
namespace Drupal\webform4content_test\Plugin\WebformHandler;

use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform4content\Mapping\Condition\EntityCondition\Always;
use Drupal\webform4content\Mapping\Condition\EntityCondition\HasEntityAccess;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\MappedEntityCondition;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Field;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\MultiPropertyField;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Element;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\PropertyTemplate;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Wrapper\CreateAndUpdateElements;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\Wrapper\ReadOnlyTextElementWithFormat;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeCreateOverrideProvider;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeUpdateOverrideProvider;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

/**
 * @WebformHandler(
 *   id = "webform4content_computed_test",
 *   label = @Translation("Webform4Content Computed Test Handler"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Use webform to create content."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
final class Webform4ContentComputedTestHandler extends Webform4ContentHandlerBase {

  protected function defineEntityMappers(EntityMapperController $controller): void {
    $nodeMapper = new EntityMapper('node', 'webform4content_computed');
    // This must come first.
    $controller->addEntityMapper($nodeMapper);

    $controller->addRouteOverride($nodeMapper,
      new NodeCreateOverrideProvider('webform4content_computed', new Always()));
    $controller->addRouteOverride($nodeMapper,
      new NodeUpdateOverrideProvider('webform4content_computed', new Always()));

    $controller->addTracker($nodeMapper, 'tracker');

    $controller->setAutoAcceptCondition(
      new MappedEntityCondition($nodeMapper, new HasEntityAccess())
    );

    ItemMapper::create($nodeMapper, new Field('title'), new PropertyTemplate(['ConstantTitle']));
    ItemMapper::create($nodeMapper,
      new MultiPropertyField('field_webform4content_formatted'),
      new CreateAndUpdateElements(
        'tracker', // Switches to update mode.
        // On create.
        new ReadOnlyTextElementWithFormat(
          // Read the text from here.
          'computed',
          // Read the format from this formatted-text element.
          'formatted'
        ),
        // On update.
        new Element('formatted')
      )
    );
  }

}
