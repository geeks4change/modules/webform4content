<?php

declare(strict_types=1);
namespace Drupal\webform4content_test\Plugin\WebformHandler;

use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform4content\Mapping\Condition\EntityCondition\Always;
use Drupal\webform4content\Mapping\Condition\EntityCondition\HasEntityAccess;
use Drupal\webform4content\Mapping\Condition\WebformSubmissionCondition\MappedEntityCondition;
use Drupal\webform4content\Mapping\EntityMapper\EntityMapper;
use Drupal\webform4content\Mapping\EntityMapperController;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\Field;
use Drupal\webform4content\Mapping\ItemMapper\EntityAdapter\JsonField;
use Drupal\webform4content\Mapping\ItemMapper\ItemMapper;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\MultiElement;
use Drupal\webform4content\Mapping\ItemMapper\WebformAdapter\PropertyTemplate;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeCreateOverrideProvider;
use Drupal\webform4content\Mapping\RouteOverrideAdapter\NodeUpdateOverrideProvider;
use Drupal\webform4content\Plugin\WebformHandler\Webform4ContentHandlerBase;

/**
 * @WebformHandler(
 *   id = "webform4content_profile1",
 *   label = @Translation("Webform4Content Profile 1"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Use webform to create content."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
final class Webform4ContentProfile1Handler extends Webform4ContentHandlerBase {

  protected function defineEntityMappers(EntityMapperController $controller): void {
    $nodeMapper = new EntityMapper('node', 'webform4content_profile');
    // This must come first.
    $controller->addEntityMapper($nodeMapper);

    $controller->addRouteOverride($nodeMapper,
      new NodeCreateOverrideProvider('webform4content_profile', new Always()));
    $controller->addRouteOverride($nodeMapper,
      new NodeUpdateOverrideProvider('webform4content_profile', new Always()));

    $controller->addTracker($nodeMapper, 'tracker');

    ItemMapper::create($nodeMapper, new Field('title'), new PropertyTemplate(['Profile1']));

    ItemMapper::create($nodeMapper, new JsonField('field_webform4content_json'), new MultiElement('__webform__', 'email'));

    $controller->setAutoAcceptCondition(
      new MappedEntityCondition($nodeMapper, new HasEntityAccess())
    );
  }

}
