<?php

declare(strict_types=1);

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Add field webform_submission:webform4content_mappers_saved
 */
function webform4content_update_9201(&$sandbox) {
  // @see \Drupal\webform4content\Mapping\Webform\WebformSubmissionWrapper\WebformSubmissionWrapper::baseFieldDefinitionsByEntityTypeId
  $definition = BaseFieldDefinition::create('string')
    ->setLabel(t('Mappers saved'))
    ->setTranslatable(FALSE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
  ;

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('webform4content_mappers_saved', 'webform_submission', 'webform4content', $definition);
}

/**
 * Add field webform_submission:webform4content_()
 */
function webform4content_update_9202(&$sandbox) {
  $definitions['webform4content_fields_no_access'] = BaseFieldDefinition::create('string')
    ->setName('webform4content_fields_no_access')
    ->setLabel(t('Fields with no access'))
    ->setTranslatable(FALSE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
  ;
  $definitions['webform4content_emails_confirm'] = BaseFieldDefinition::create('string')
    ->setName('webform4content_emails_confirm')
    ->setLabel(t('Emails to confirm'))
    ->setTranslatable(FALSE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
  ;
  $definitions['webform4content_state'] = BaseFieldDefinition::create('state')
    ->setName('webform4content_state')
    ->setLabel(t('State'))
    ->setRequired(TRUE)
    ->setTranslatable(FALSE)
    // Must be given to display this field at all.
    // @see \Drupal\Core\Entity\EntityDisplayBase::getFieldDefinitions
    ->setDisplayOptions('view', ['weight' => 0])
    ->setSetting('workflow', 'webform4content_default')
  ;

  foreach ($definitions as $name => $definition) {
    \Drupal::entityDefinitionUpdateManager()
      ->installFieldStorageDefinition($name, 'webform_submission', 'webform4content', $definition);
  }
}

/**
 * Enable confirmation.module. (email_confirmer.module can be disabled.)
 */
function webform4content_update_9203(&$sandbox) {
  \Drupal::service('module_installer')->install(['confirmation']);
}

/**
 * Drop obsolete webform_submission fields.
 */
function webform4content_update_9204(&$sandbox) {
  $definitions['webform4content_fields_no_access'] = BaseFieldDefinition::create('string')
    ->setTargetEntityTypeId('webform_submission')
    ->setName('webform4content_fields_no_access')
    ->setLabel(t('Fields with no access'))
    ->setTranslatable(FALSE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
  ;
  $definitions['webform4content_mappers_saved'] = BaseFieldDefinition::create('string')
    ->setTargetEntityTypeId('webform_submission')
    ->setName('webform4content_mappers_saved')
    ->setLabel(t('Mappers saved'))
    ->setTranslatable(FALSE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
  ;
  foreach ($definitions as $definition) {
    \Drupal::entityDefinitionUpdateManager()->uninstallFieldStorageDefinition($definition);
  }
}

/**
 * Update debug settings to new schema.
 */
function webform4content_update_9205(&$sandbox) {
  $settings = \Drupal::configFactory()->getEditable('webform4content.settings');
  $debugBool = $settings->get('debug');
  $debugModes = $debugBool ? ['mapping', 'date'] : [];
  // Set new key and unset old.
  $settings->setData(['debug_modes' => $debugModes]);
  $settings->save();
}
